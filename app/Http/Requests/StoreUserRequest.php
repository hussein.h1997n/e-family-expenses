<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'name' => 'required|string|regex:/^[a-zA-Z ]{4,25}$/',
            'email' => 'required|email|unique:users,email',
            'password' => 'required|regex:/^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{8,20}$/',
            'privilege' => 'required'
        ];
    }
    public function messages()
    {
        return ([
            'name.regex' => 'invalid name,name must be between 4 and 25, and contains only letters',
            'email.required' => 'email field is required',
            'password.regex' => 'invalid password ,password must be betweeb 8 and 20 and contains at least one digit,one letter, one special character @ $ ! % * # ? &'
        ]
        );
    }
}
