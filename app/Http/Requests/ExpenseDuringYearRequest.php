<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ExpenseDuringYearRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            // 'year' => 'required|date_format:Y|digits:4|after:1980'
        ];
    }
    public function messages()
    {
        return [
            'year.date_format' => 'invalid date , you should insert a year with format YYYY ,eg:2020'
        ];
    }
}
