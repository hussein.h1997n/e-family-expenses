<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StoreOutlayRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'price' => 'required|integer|min:1',
            'description' => 'string|required|min:2|max:30',
            'material_id' => 'required|exists:materials,id',
            'outlaytype_id' => 'required|exists:outlaytypes,id',
            'user_id' => ['exists:users,id', Rule::requiredIf($this->user()->hasRole('super-admin')), Rule::excludeIf($this->user()->hasRole('member'))],
            'date' => 'required|date_format:Y-m-d'
        ];
    }
    public function messages()
    {
        return [
            'date.date_format' => 'invalid date,date must match the format Y-m-d, eg:2022-1-1'
        ];
    }
}
