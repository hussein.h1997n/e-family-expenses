<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class ServiceOutlaysCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $total = array_reduce($this->collection->toArray(), function ($ServiceAcc, $outlay) {
            $outlaysTotal = array_reduce($outlay->outlays->toArray(), function ($acc, $outlay) {
                $acc += $outlay['price'];
                return $acc;
            });
            $ServiceAcc += $outlaysTotal;
            return $ServiceAcc;
        });
        return [
            'services' => ServiceOutlaysResource::collection($this->collection),
            'sum' => count($this->collection),
            'total' => $total

        ];
    }
}
