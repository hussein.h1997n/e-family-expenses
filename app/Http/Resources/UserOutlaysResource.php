<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UserOutlaysResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $total = 0;
        if (count($this->outlays->toArray()))
            $total = array_reduce($this->outlays->toArray(), function ($acc, $outlay) {
                $acc += $outlay['price'];
                return $acc;
            });
        // return parent::toArray($request);
        return [
            "id" => $this->id,
            "name" => $this->name,
            "email" => $this->email,
            "total" => $total,
            "outlays" => $this->outlays->map->only([
                "id",
                "material_id",
                "outlaytype_id",
                "user_id",
                "price",
                "description",
                "date"
            ])
        ];
    }
}
