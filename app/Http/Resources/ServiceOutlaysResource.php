<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ServiceOutlaysResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $total = array_reduce($this->outlays->toArray(), function ($acc, $outlay) {
            $acc += $outlay['price'];
            return $acc;
        });
        return [
            "id" => $this->id,
            "name" => $this->name,
            "isService" => $this->isService,
            "description" => $this->description,
            "total" => $total,
            "outaly_num" => count($this->outlays),
            "outlays" => $this->outlays
        ];
    }
}
