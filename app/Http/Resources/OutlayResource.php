<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class OutlayResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "material" => ['material_id' => $this->material->id, 'material_name' => $this->material->name],
            "outlaytype" => ['outlaytype_id' => $this->outlaytype->id, 'outlaytype_name' => $this->outlaytype->name],
            "user" => ['user_id' => $this->user->id, 'user_name' => $this->user->name],
            "price" => $this->price,
            "description" => $this->description,
            "date" => $this->date,
            "created_at" => $this->created_at,
            "updated_at" => $this->updated_at
        ];
    }
}
