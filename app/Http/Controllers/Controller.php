<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;


/**
 * @OA\Info(
 *     version="1.0",
 *     title="Family Expenses"
 * )
 */



class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
}

/**
 * @OA\Schema(
 *      schema="Material",
 *      title="Material",
 *     @OA\Property(
 *         property="id",
 *         type="int"
 *     ),
 *     @OA\Property(
 *         property="name",
 *         type="string"
 *     ),
 *     @OA\Property(
 *         property="isService",
 *         type="int",
 *     ),
 *     @OA\Property(
 *         property="description",
 *         type="string",
 *     ),
 *     @OA\Property(
 *         property="created_at",
 *         type="string",
 *         format="date-time",
 *     ),
 *     @OA\Property(
 *         property="updated_at",
 *         type="string",
 *         format="date-time",
 *     ),
 *     example={"id": 1,"name": "Electricity Service","isService": 1,"description": "pay money for electricity 24/24", "created_at": "2024-05-14T10:00:14.000000Z", "updated_at": "2024-05-14T10:00:14.000000Z"}
 * )
 */

class Material
{
};

/**
 * @OA\Schema(
 *      schema="OutlayType",
 *      title="Outlay Type",
 *     @OA\Property(
 *         property="id",
 *         type="int",
 *        readOnly=true,
 *     ),
 *     @OA\Property(
 *         property="name",
 *         type="string"
 *     ),
 *     @OA\Property(
 *         property="description",
 *         type="string"
 *     ),
 *     @OA\Property(
 *         property="updated_at",
 *         type="string",
 *         format="date-time",
 *         readOnly=true,
 *     ),
 *     @OA\Property(
 *         property="created_at",
 *         type="string",
 *         format="date-time",
 *         readOnly=true,
 *     ),
 * )
 */

class OutlayType
{
};
/**
 * @OA\Schema(
 *      schema="User",
 *      title="User",
 *     @OA\Property(
 *         property="id",
 *         type="int",
 *        readOnly=true,
 *     ),
 *     @OA\Property(
 *         property="name",
 *         type="string"
 *     ),
 *     @OA\Property(
 *         property="email",
 *         type="string",
 *         format="email",
 *     ),
 *     @OA\Property(
 *         property="password",
 *         type="string",
 *         format="password",
 *     ),
 *     @OA\Property(
 *         property="privilege",
 *         type="string",
 *     ),
 *     @OA\Property(
 *         property="updated_at",
 *         type="string",
 *         format="date-time",
 *         readOnly=true,
 *     ),
 *     @OA\Property(
 *         property="created_at",
 *         type="string",
 *         format="date-time",
 *         readOnly=true,
 *     ),
 * )
 */

class User
{
};
/**
 * @OA\Schema(
 *      schema="Outlay",
 *      title="Outlay",
 *     @OA\Property(
 *         property="id",
 *         type="int",
 *     ),
 *     @OA\Property(
 *         property="material_id",
 *         type="int",
 *     ),
 *     @OA\Property(
 *         property="outlaytype_id",
 *         type="int"
 *     ),
 *     @OA\Property(
 *         property="price",
 *         type="int"
 *     ),
 *     @OA\Property(
 *         property="date",
 *         type="string",
 *         format="date",
 *     ),
 *     @OA\Property(
 *         property="description",
 *         type="string",
 *     ),
 *
 *     @OA\Property(
 *         property="updated_at",
 *         type="string",
 *         format="date-time",
 *         readOnly=true,
 *     ),
 *     @OA\Property(
 *         property="created_at",
 *         type="string",
 *         format="date-time",
 *         readOnly=true,
 *     ),
 * )
 */

class Outlay
{
};
