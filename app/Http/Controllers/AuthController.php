<?php

namespace App\Http\Controllers;

use App\Http\Requests\LoginRequest;
use App\Models\Material;
use App\Models\Outlay;
use App\Models\OutlayType;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth as FacadesAuth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\MessageBag;
use Illuminate\Validation\ValidationException;
use Spatie\Permission\Models\Role;



/**
 * @OA\Post(
 *      path="/api/login",
 *      operationId="login",
 *      tags={"Auth"},
 *      summary="login to your account",
 *      description="login to your account via email and password",
 *      @OA\Parameter(
 *          description="Accept",
 *          in="header",
 *          name="Accept",
 *          required=true,
 *          @OA\Schema(type="string",enum={"application/json"}),
 *      ),
 *      @OA\RequestBody(
 *          required=true,
 *          @OA\JsonContent(
 *                 @OA\Property(
 *                     property="email",
 *                     type="string",
 *                     format="email",
 *                 ),
 *                 @OA\Property(
 *                     property="password",
 *                     type="string",
 *                     type="password",
 *                 ),
 *                 @OA\Examples(example="admin",value={"email":"admin@gmail.com","password":"123123123a@"},summary="login as admin"),
 *                 @OA\Examples(example="user",value={"email":"john@gmail.com","password":"123123123a@"},summary="login as user"),
 *          )
 *      ),
 *      @OA\Response(
 *          response=200,
 *          description="Response Message",
 *          @OA\JsonContent(
 *              @OA\Property(
 *                     property="status",
 *                     type="string",
 *                     example="success",
 *                 ),
 *              @OA\Property(
 *                     property="data",
 *                     type="object",
 *                     @OA\Property(
 *                         property="id",
 *                         type="int",
 *                         example=1,
 *                     ),
 *                     @OA\Property(
 *                         property="name",
 *                         type="string",
 *                         example="user",
 *                     ),
 *                     @OA\Property(
 *                         property="email",
 *                         type="string",
 *                         example="user@gmail.com",
 *                     ),
 *                     @OA\Property(
 *                         property="token_type",
 *                         type="string",
 *                         example="Bearer",
 *                     ),
 *                     @OA\Property(
 *                         property="token",
 *                         type="string",
 *                         example="dd",
 *                     ),
 *              ),
 *          ),
 *      ),
 *      @OA\Response(
 *               response="422",
 *               description="Unprocessable Entity",
 *               @OA\JsonContent(
 *                 @OA\Property(
 *                     property="string",
 *                     type="int",
 *                 ),
 *               ),
 *      ),
 *      @OA\Response(
 *               response="401",
 *               description="Unauthorized",
 *               @OA\JsonContent(
 *                 @OA\Property(
 *                     property="message",
 *                     type="string",
 *                     example="Unauthenticated."
 *                 ),
 *               ),
 *      ),
 *      @OA\Response(
 *               response="400",
 *               description="Bad Request",
 *               @OA\JsonContent(
 *                 @OA\Property(
 *                     property="status",
 *                     type="string",
 *                     example="failure"
 *                 ),
 *                 @OA\Property(
 *                     property="message",
 *                     type="string",
 *                     example="you need to login first"
 *                 ),
 *               ),
 *      ),
 *      @OA\Response(
 *               response="404",
 *               description="not found",
 *               @OA\JsonContent(
 *                 @OA\Property(
 *                     property="status",
 *                     type="string",
 *                     example="failure"
 *                 ),
 *                 @OA\Property(
 *                     property="message",
 *                     type="string",
 *                     example="resource not found"
 *                 ),
 *               ),
 *      ),
 *     )
 */

class AuthController extends Controller
{
    public function Login(LoginRequest $request)
    {
        $user = User::where([
            'email' => $request->email,
            'password' => $request->password
        ])->first();
        if ($user) {
            auth()->login($user, true);
            if ($request->wantsJson()) {
                $token = FacadesAuth::user()->createToken('SanctumApi')->plainTextToken;
                return response()->json([
                    'status' => 'success',
                    'data' => [
                        'id' => auth()->user()->id,
                        'name' => auth()->user()->name,
                        'email' => auth()->user()->email,
                        'token_type' => 'Bearer',
                        'token' => $token
                    ]
                ]);
            } else {
                if ($user->roles[0]->name == "super-admin") {
                    return redirect(URL::to('/dashboard_users'));
                } elseif ($user->roles[0]->name == "member") {
                    return redirect(URL::to('/home'));
                }
            }
        } else {
            if ($request->wantsJson()) {
                return response()->json(['status' => 'fail', 'errors' => ['auth' => "invalid email or password"]]);
            }
            return back()->with('auth', 'email or password is invalid');
        }
    }

    /**
     * @OA\Post(
     *      path="/api/logout",
     *      operationId="logout",
     *      tags={"Auth"},
     *      summary="logout",
     *      security={{ "sanctum":{} }},
     *      description="logout user",
     *      @OA\RequestBody(),
     *      @OA\Response(
     *          response=200,
     *          description="ok",
     *           @OA\JsonContent(
     *                 @OA\Property(
     *                     property="status",
     *                     type="string",
     *                     example="success",
     *                 ),
     *                 @OA\Property(
     *                     property="message",
     *                     type="string",
     *                     example="logged out",
     *                 ),
     *          )
     *       ),
     *      @OA\Response(
     *          response=404,
     *          description="not found",
     *           @OA\JsonContent(
     *                 @OA\Property(
     *                     property="status",
     *                     type="string",
     *                     example="error",
     *                 ),
     *                 @OA\Property(
     *                     property="message",
     *                     type="string",
     *                     example="not found",
     *                 ),
     *          )
     *       ),
     *      @OA\Response(
     *          response=401,
     *          description="not found",
     *           @OA\JsonContent(
     *                 @OA\Property(
     *                     property="status",
     *                     type="string",
     *                     example="error",
     *                 ),
     *                 @OA\Property(
     *                     property="message",
     *                     type="string",
     *                     example="unathorized",
     *                 ),
     *          )
     *       ),
     *      @OA\Response(
     *          response=500,
     *          description="server error",
     *           @OA\JsonContent(
     *                 @OA\Property(
     *                     property="status",
     *                     type="string",
     *                     example="error",
     *                 ),
     *                 @OA\Property(
     *                     property="message",
     *                     type="string",
     *                     example="server error",
     *                 ),
     *          )
     *       ),
     *     )
     */
    public function logout(Request $request)
    {
        if ($request->wantsJson()) {
            auth()->user()->tokens()->delete();
            return response()->json([
                'status' => 'success',
                'message' => 'logged out'
            ]);
        } else {
            FacadesAuth::logout();
            return redirect()->route('login');
        }
    }

    /**
     * @OA\Get(
     *      path="/api/userinfo",
     *      operationId="user info",
     *      tags={"Auth"},
     *      security={{ "sanctum":{} }},
     *      summary="get user information",
     *      description="get detailed information id, name and email about logged in user",
     *      @OA\Response(
     *          response=200,
     *          description="ok",
     *           @OA\JsonContent(
     *                 @OA\Property(
     *                     property="id",
     *                     type="int",
     *
     *                 ),
     *                 @OA\Property(
     *                     property="name",
     *                     type="string",
     *                 ),
     *                 @OA\Property(
     *                     property="email",
     *                     type="string",
     *                 ),
     *           )
     *       ),
     *      @OA\Response(
     *          response=404,
     *          description="not found",
     *           @OA\JsonContent(
     *                 @OA\Property(
     *                     property="status",
     *                     type="string",
     *                     example="error",
     *                 ),
     *                 @OA\Property(
     *                     property="message",
     *                     type="string",
     *                     example="not found",
     *                 ),
     *          )
     *       ),
     *      @OA\Response(
     *          response=401,
     *          description="unathorized",
     *           @OA\JsonContent(
     *                 @OA\Property(
     *                     property="status",
     *                     type="string",
     *                     example="error",
     *                 ),
     *                 @OA\Property(
     *                     property="message",
     *                     type="string",
     *                     example="unathorized",
     *                 ),
     *          )
     *       ),
     *      @OA\Response(
     *          response=500,
     *          description="server error",
     *           @OA\JsonContent(
     *                 @OA\Property(
     *                     property="status",
     *                     type="string",
     *                     example="error",
     *                 ),
     *                 @OA\Property(
     *                     property="message",
     *                     type="string",
     *                     example="server error",
     *                 ),
     *          )
     *       ),
     *     )
     */

    public function userInfo(Request $request)
    {
        return $request->user()->only(['id', 'name', 'email']);
    }


    public function showLogin()
    {
        return view('Auth.login');
    }
    public function AdminIndex()
    {
        $services_json =  app('App\Http\Controllers\OutlayController')->index();
        $services = json_encode($services_json);
        $services = collect(json_decode($services, true));
        $outlays = collect($services['original']['data']);
        $users = User::all();
        $materials = Material::all();
        $outlaytypes = OutlayType::all();
        $outlays = Outlay::all();
        return view('admin.dashboard', compact(
            'materials',
            'users',
            'outlaytypes',
            'outlays',
            // 'services'
        ));
    }
}
