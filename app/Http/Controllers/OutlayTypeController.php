<?php

namespace App\Http\Controllers;

use App\Models\OutlayType;
use App\Http\Requests\StoreOutlayTypeRequest;
use App\Http\Requests\UpdateOutlayTypeRequest;
use Illuminate\Http\Request;

class OutlayTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    /**
     * @OA\Get(
     *      path="/api/outlaytype?page={page}",
     *      operationId="get all Outlay Type",
     *      security={{"sanctum":{}}},
     *      tags={"Outlay Type"},
     *      summary="get All Outlay Type",
     *      description="get all Outlay Type with details id, name, description, etc...",
     *      @OA\Parameter(
     *          description="page number",
     *          in="query",
     *          name="page",
     *          required=true,
     *          @OA\Schema(type="string"),
     *          @OA\Examples(example="string", value="1", summary="first page"),
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="ok",
     *           @OA\JsonContent(
     *                 @OA\Property(
     *                     property="status",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="data",
     *                     type="array",
     *                     @OA\items(ref="#/components/schemas/OutlayType")
     *                 ),
     *                 example={"status":"success","data":{{"id":1,"name": "internet","description": "internet and tv bills","updated_at": "2024-05-14T19:01:30.000000Z","created_at": "2024-05-14T19:01:30.000000Z"}}}
     *           )
     *       ),
     *      @OA\Response(
     *          response=404,
     *          description="not found",
     *           @OA\JsonContent(
     *                 @OA\Property(
     *                     property="status",
     *                     type="string",
     *                     example="error",
     *                 ),
     *                 @OA\Property(
     *                     property="message",
     *                     type="string",
     *                     example="not found",
     *                 ),
     *          )
     *       ),
     *      @OA\Response(
     *          response=401,
     *          description="unathorized",
     *           @OA\JsonContent(
     *                 @OA\Property(
     *                     property="status",
     *                     type="string",
     *                     example="error",
     *                 ),
     *                 @OA\Property(
     *                     property="message",
     *                     type="string",
     *                     example="unathorized",
     *                 ),
     *          )
     *       ),
     *      @OA\Response(
     *          response=500,
     *          description="server error",
     *           @OA\JsonContent(
     *                 @OA\Property(
     *                     property="status",
     *                     type="string",
     *                     example="error",
     *                 ),
     *                 @OA\Property(
     *                     property="message",
     *                     type="string",
     *                     example="server error",
     *                 ),
     *          )
     *       ),
     *     )
     */

    public function index(Request $request)
    {
        try {
            $outlaytypes = OutlayType::all();
            if ($request->wantsJson()) {
                return ['status' => 'success', 'data' => $outlaytypes];
            } else {
                return view('admin.dashboard_outlaytypes', compact('outlaytypes'));
            }
        } catch (\Throwable $th) {
            if ($request->wantsJson()) {
                return response()->json([
                    'status' => 'fail',
                    'errors' => ['error' => 'something wrong happened']
                ]);
            } else {
                return back()->with('error', 'Something Wrong Happened!');
            }
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreOutlayTypeRequest  $request
     * @return \Illuminate\Http\Response
     */

    /**
     * @OA\Post(
     *      path="/api/outlaytype",
     *      operationId="add outlay type",
     *      tags={"Outlay Type"},
     *      security={{"sanctum":{}}},
     *      summary="add new outlay type",
     *      description="add new outlay type with data name,description",
     *      @OA\RequestBody(
     *          @OA\JsonContent(ref="#/components/schemas/OutlayType",example={"name": "internet","description": "internet and tv bills"}),
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="ok",
     *           @OA\JsonContent(
     *                 @OA\Property(
     *                     property="status",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="data",
     *                     ref="#/components/schemas/OutlayType"
     *                 ),
     *                 example={"status":"success","data":{"id":1,"name": "internet","description": "internet and tv bills","updated_at": "2024-05-14T19:01:30.000000Z","created_at": "2024-05-14T19:01:30.000000Z"}}
     *           )
     *       ),
     *      @OA\Response(
     *          response=404,
     *          description="not found",
     *           @OA\JsonContent(
     *                 @OA\Property(
     *                     property="status",
     *                     type="string",
     *                     example="error",
     *                 ),
     *                 @OA\Property(
     *                     property="message",
     *                     type="string",
     *                     example="not found",
     *                 ),
     *          )
     *       ),
     *      @OA\Response(
     *          response=401,
     *          description="unathorized",
     *           @OA\JsonContent(
     *                 @OA\Property(
     *                     property="status",
     *                     type="string",
     *                     example="error",
     *                 ),
     *                 @OA\Property(
     *                     property="message",
     *                     type="string",
     *                     example="unathorized",
     *                 ),
     *          )
     *       ),
     *      @OA\Response(
     *          response=500,
     *          description="server error",
     *           @OA\JsonContent(
     *                 @OA\Property(
     *                     property="status",
     *                     type="string",
     *                     example="error",
     *                 ),
     *                 @OA\Property(
     *                     property="message",
     *                     type="string",
     *                     example="server error",
     *                 ),
     *          )
     *       ),
     *     )
     */

    public function store(StoreOutlayTypeRequest $request)
    {
        try {
            $outlayTypes = OutlayType::create([
                'name' => $request->name,
                'description' => $request->description,
            ]);
            if ($request->wantsJson()) {

                return ['status' => 'success', 'data' => $outlayTypes];
            } else {
                return back()->with('success', 'Outlaytype added successfully');
            }
        } catch (\Throwable $th) {
            if ($request->wantsJson()) {
                return response()->json([
                    'status' => 'fail',
                    'errors' => ['error' => 'something wrong happened']
                ]);
            } else {
                return back()->with('error', 'something wrong happened');
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\OutlayType  $outlayType
     * @return \Illuminate\Http\Response
     */
    /**
     * @OA\Get(
     *      path="/api/outlaytype/{id}",
     *      operationId="get single Outlay Type",
     *      security={{"sanctum":{}}},
     *      tags={"Outlay Type"},
     *      summary="get single Outlay Type",
     *      description="get single Outlay Type with details id, name, description, etc...",
     *      @OA\Parameter(
     *          description="outlay type id",
     *          in="path",
     *          name="id",
     *          required=true,
     *          @OA\Schema(type="string"),
     *          @OA\Examples(example="string", value="1", summary="first material"),
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="ok",
     *           @OA\JsonContent(
     *                 @OA\Property(
     *                     property="status",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="data",
     *                     ref="#/components/schemas/OutlayType"
     *                 ),
     *                 example={"status":"success","data":{"id":1,"name": "internet","description": "internet and tv bills","updated_at": "2024-05-14T19:01:30.000000Z","created_at": "2024-05-14T19:01:30.000000Z"}}
     *           )
     *       ),
     *      @OA\Response(
     *          response=404,
     *          description="not found",
     *           @OA\JsonContent(
     *                 @OA\Property(
     *                     property="status",
     *                     type="string",
     *                     example="error",
     *                 ),
     *                 @OA\Property(
     *                     property="message",
     *                     type="string",
     *                     example="not found",
     *                 ),
     *          )
     *       ),
     *      @OA\Response(
     *          response=401,
     *          description="unathorized",
     *           @OA\JsonContent(
     *                 @OA\Property(
     *                     property="status",
     *                     type="string",
     *                     example="error",
     *                 ),
     *                 @OA\Property(
     *                     property="message",
     *                     type="string",
     *                     example="unathorized",
     *                 ),
     *          )
     *       ),
     *      @OA\Response(
     *          response=500,
     *          description="server error",
     *           @OA\JsonContent(
     *                 @OA\Property(
     *                     property="status",
     *                     type="string",
     *                     example="error",
     *                 ),
     *                 @OA\Property(
     *                     property="message",
     *                     type="string",
     *                     example="server error",
     *                 ),
     *          )
     *       ),
     *     )
     */

    public function show(Request $request, OutlayType $outlaytype)
    {
        return $outlaytype;
        try {
            return response()->json([
                'status' => 'success',
                'data' => $outlaytype
            ]);
        } catch (\Throwable $th) {
            if ($request->wantsJson()) {
                return response()->json([
                    'status' => 'fail',
                    'errors' => ['error' => 'something wrong happened']
                ]);
            } else {
                return back()->with('error', 'something wrong happened');
            }
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\OutlayType  $outlayType
     * @return \Illuminate\Http\Response
     */
    public function edit(OutlayType $outlayType)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateOutlayTypeRequest  $request
     * @param  \App\Models\OutlayType  $outlayType
     * @return \Illuminate\Http\Response
     */
    /**
     * @OA\Put(
     *      path="/api/outlaytype/update/{id}",
     *      operationId="update Outlay Type",
     *      tags={"Outlay Type"},
     *      security={{"sanctum":{}}},
     *      summary="update Outlay Type via id",
     *      description="update Outlay Type data name,description via material id",
     *      @OA\Parameter(
     *          description="id",
     *          in="path",
     *          name="id",
     *          required=true,
     *          @OA\Schema(type="string"),
     *          @OA\Examples(example="string", value="1", summary="first material"),
     *      ),
     *      @OA\RequestBody(
     *          @OA\JsonContent(ref="#/components/schemas/OutlayType",example={"name": "internet","description": "internet and tv bills"}),
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="ok",
     *           @OA\JsonContent(
     *                 @OA\Property(
     *                     property="status",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="data",
     *                     ref="#/components/schemas/OutlayType"
     *                 ),
     *                 example={"status":"success","data":{"id":1,"name": "internet","description": "internet and tv bills","updated_at": "2024-05-14T19:01:30.000000Z","created_at": "2024-05-14T19:01:30.000000Z"}}
     *           )
     *       ),
     *      @OA\Response(
     *          response=404,
     *          description="not found",
     *           @OA\JsonContent(
     *                 @OA\Property(
     *                     property="status",
     *                     type="string",
     *                     example="error",
     *                 ),
     *                 @OA\Property(
     *                     property="message",
     *                     type="string",
     *                     example="not found",
     *                 ),
     *          )
     *       ),
     *      @OA\Response(
     *          response=401,
     *          description="unathorized",
     *           @OA\JsonContent(
     *                 @OA\Property(
     *                     property="status",
     *                     type="string",
     *                     example="error",
     *                 ),
     *                 @OA\Property(
     *                     property="message",
     *                     type="string",
     *                     example="unathorized",
     *                 ),
     *          )
     *       ),
     *      @OA\Response(
     *          response=500,
     *          description="server error",
     *           @OA\JsonContent(
     *                 @OA\Property(
     *                     property="status",
     *                     type="string",
     *                     example="error",
     *                 ),
     *                 @OA\Property(
     *                     property="message",
     *                     type="string",
     *                     example="server error",
     *                 ),
     *          )
     *       ),
     *     )
     */

    public function update(UpdateOutlayTypeRequest $request, OutlayType $outlaytype)
    {
        try {
            $outlaytype->name = $request->name;
            $outlaytype->description = $request->description;
            $outlaytype->save();
            if ($request->wantsJson()) {
                return response()->json([
                    'status' => 'success',
                    'data' => $outlaytype,
                ]);
            } else {
                return back()->with('success', 'Outlaytype updated successfully');
            }
        } catch (\Throwable $th) {
            if ($request->wantsJson()) {
                return response()->json([
                    'status' => 'fail',
                    'errors' => ['error' => 'something wrong happened']
                ]);
            } else {
                return back()->with('error', 'something wrong happened');
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\OutlayType  $outlayType
     * @return \Illuminate\Http\Response
     */
    /**
     * @OA\Delete(
     *      path="/api/outlaytype/delete/{id}",
     *      operationId="delete outlay type",
     *      tags={"Outlay Type"},
     *      security={{"sanctum":{}}},
     *      summary="delete outlay type via id",
     *      description="delete outlay type via id",
     *      @OA\Parameter(
     *          description="outlay type id",
     *          in="path",
     *          name="id",
     *          required=true,
     *          @OA\Schema(type="string"),
     *          @OA\Examples(example="string", value="1", summary="first material"),
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="ok",
     *           @OA\JsonContent(
     *                 @OA\Property(
     *                     property="status",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="message",
     *                     type="string",
     *                 ),
     *                 example={"status":"success","message":"deleted successfully"}
     *          )
     *       ),
     *       @OA\Response(
     *          response=404,
     *          description="not found",
     *           @OA\JsonContent(
     *                 @OA\Property(
     *                     property="status",
     *                     type="string",
     *                     example="error",
     *                 ),
     *                 @OA\Property(
     *                     property="message",
     *                     type="string",
     *                     example="not found",
     *                 ),
     *          )
     *       ),
     *      @OA\Response(
     *          response=401,
     *          description="unathorized",
     *           @OA\JsonContent(
     *                 @OA\Property(
     *                     property="status",
     *                     type="string",
     *                     example="error",
     *                 ),
     *                 @OA\Property(
     *                     property="message",
     *                     type="string",
     *                     example="unathorized",
     *                 ),
     *          )
     *       ),
     *      @OA\Response(
     *          response=500,
     *          description="server error",
     *           @OA\JsonContent(
     *                 @OA\Property(
     *                     property="status",
     *                     type="string",
     *                     example="error",
     *                 ),
     *                 @OA\Property(
     *                     property="message",
     *                     type="string",
     *                     example="server error",
     *                 ),
     *          )
     *       ),
     *     )
     */

    public function destroy(Request $request, OutlayType $outlaytype)
    {

        try {
            $outlaytype->delete();
            if ($request->wantsJson()) {
                return response()->json([
                    'status' => 'success',
                    'message' => 'deleted successfully'
                ]);
            } else {
                return back()->with('success', 'Outlaytype deleted successfully');
            }
        } catch (\Throwable $th) {
            if ($request->wantsJson()) {
                return response()->json([
                    'status' => 'fail',
                    'errors' => ['error' => 'something wrong happened']
                ]);
            } else {
                return back()->with('error', 'something wrong happened');
            }
        }
    }
}
