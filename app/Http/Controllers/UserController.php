<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreUserRequest;
use App\Http\Requests\UpdateUserRequest;
use App\Http\Resources\userResource;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Role;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    /**
     * @OA\Get(
     *      path="/api/user?page={page}",
     *      operationId="get all users Type",
     *      security={{"sanctum":{}}},
     *      tags={"User"},
     *      summary="get All users Type",
     *      description="get all users Type with details id, name, email, etc...",
     *      @OA\Parameter(
     *          description="page number",
     *          in="query",
     *          name="page",
     *          required=true,
     *          @OA\Schema(type="string"),
     *          @OA\Examples(example="string", value="1", summary="first page"),
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="ok",
     *           @OA\JsonContent(
     *                 @OA\Property(
     *                     property="status",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="data",
     *                     type="array",
     *                     @OA\items(ref="#/components/schemas/User")
     *                 ),
     *                 example={"status":"success","data":{{"id":1,"name":"hussein","email":"husya@gmail.com","password":"123123123@a","privilege":"member","updated_at": "2024-05-14T19:01:30.000000Z","created_at": "2024-05-14T19:01:30.000000Z"}}}
     *           )
     *       ),
     *      @OA\Response(
     *          response=404,
     *          description="not found",
     *           @OA\JsonContent(
     *                 @OA\Property(
     *                     property="status",
     *                     type="string",
     *                     example="error",
     *                 ),
     *                 @OA\Property(
     *                     property="message",
     *                     type="string",
     *                     example="not found",
     *                 ),
     *          )
     *       ),
     *      @OA\Response(
     *          response=401,
     *          description="unathorized",
     *           @OA\JsonContent(
     *                 @OA\Property(
     *                     property="status",
     *                     type="string",
     *                     example="error",
     *                 ),
     *                 @OA\Property(
     *                     property="message",
     *                     type="string",
     *                     example="unathorized",
     *                 ),
     *          )
     *       ),
     *      @OA\Response(
     *          response=500,
     *          description="server error",
     *           @OA\JsonContent(
     *                 @OA\Property(
     *                     property="status",
     *                     type="string",
     *                     example="error",
     *                 ),
     *                 @OA\Property(
     *                     property="message",
     *                     type="string",
     *                     example="server error",
     *                 ),
     *          )
     *       ),
     *     )
     */

    public function index(Request $request)
    {
        try {
            $users = User::where('email', '!=', auth()->user()->email)->get();
            if ($request->wantsJson()) {
                return response()->json([
                    'status' => 'success',
                    'data' => userResource::collection($users)
                ]);
            } else {
                return view('admin.dashboard_users', compact('users'));
            }
        } catch (\Throwable $th) {
            if ($request->wantsJson()) {
                return response()->json([
                    'status' => 'fail',
                    'errors' => ['error' => 'something wrong happened']
                ]);
            } else {
                return back()->with('error', 'Something Wrong Happened!');
            }
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    /**
     * @OA\Post(
     *      path="/api/user",
     *      operationId="add user",
     *      tags={"User"},
     *      security={{"sanctum":{}}},
     *      summary="add new user",
     *      description="add new user with data name,email, password",
     *      @OA\RequestBody(
     *          @OA\JsonContent(ref="#/components/schemas/User",example={"name":"hussein","email":"husya@gmail.com","password":"123123123@a","privilege":"member"}),
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="ok",
     *           @OA\JsonContent(
     *                 @OA\Property(
     *                     property="status",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="data",
     *                     ref="#/components/schemas/User"
     *                 ),
     *                 example={"status":"success","data":{"name":"hussein","email":"husya@gmail.com","password":"123123123@a","privilege":"member"}}
     *           )
     *       ),
     *      @OA\Response(
     *          response=404,
     *          description="not found",
     *           @OA\JsonContent(
     *                 @OA\Property(
     *                     property="status",
     *                     type="string",
     *                     example="error",
     *                 ),
     *                 @OA\Property(
     *                     property="message",
     *                     type="string",
     *                     example="not found",
     *                 ),
     *          )
     *       ),
     *      @OA\Response(
     *          response=401,
     *          description="unathorized",
     *           @OA\JsonContent(
     *                 @OA\Property(
     *                     property="status",
     *                     type="string",
     *                     example="error",
     *                 ),
     *                 @OA\Property(
     *                     property="message",
     *                     type="string",
     *                     example="unathorized",
     *                 ),
     *          )
     *       ),
     *      @OA\Response(
     *          response=500,
     *          description="server error",
     *           @OA\JsonContent(
     *                 @OA\Property(
     *                     property="status",
     *                     type="string",
     *                     example="error",
     *                 ),
     *                 @OA\Property(
     *                     property="message",
     *                     type="string",
     *                     example="server error",
     *                 ),
     *          )
     *       ),
     *     )
     */

    public function store(StoreUserRequest $request)
    {
        try {
            DB::beginTransaction();
            $user = User::create([
                'name' => $request->name,
                'email' => $request->email,
                'password' => $request->password,
            ]);
            if ($request->privilege === 'member') {
                $user->assignRole('member');
            } else if ($request->privilege === 'super-admin') {
                $user->assignRole('super-admin');
            }
            DB::commit();
            if ($request->wantsJson()) {

                return response()->json([
                    'status' => 'success',
                    'data' => [
                        'name' => $user->name,
                        'email' => $user->email,
                        'privilege' => $request->privilege,

                    ]

                ]);
            } else {
                return back()->with('success', 'User Added Successfully!');
            }
        } catch (\Throwable $th) {
            DB::rollBack();
            if ($request->wantsJson()) {
                return response()->json([
                    'status' => 'fail',
                    'errors' => ['error' => 'something wrong happened']
                ]);
            } else {
                return back()->with('error', 'Something Wrong Happened!');
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    /**
     * @OA\Get(
     *      path="/api/user/{id}",
     *      operationId="get single user",
     *      security={{"sanctum":{}}},
     *      tags={"User"},
     *      summary="get single user",
     *      description="get single user with details id, name, email, etc...",
     *      @OA\Parameter(
     *          description="user id",
     *          in="path",
     *          name="id",
     *          required=true,
     *          @OA\Schema(type="string"),
     *          @OA\Examples(example="string", value="1", summary="first user"),
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="ok",
     *           @OA\JsonContent(
     *                 @OA\Property(
     *                     property="status",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="data",
     *                     ref="#/components/schemas/User"
     *                 ),
     *                 example={"status":"success","data":{"id":1,"name":"hussein","email":"husya@gmail.com","password":"123123123@a","privilege":"member","updated_at": "2024-05-14T19:01:30.000000Z","created_at": "2024-05-14T19:01:30.000000Z"}}
     *           )
     *       ),
     *      @OA\Response(
     *          response=404,
     *          description="not found",
     *           @OA\JsonContent(
     *                 @OA\Property(
     *                     property="status",
     *                     type="string",
     *                     example="error",
     *                 ),
     *                 @OA\Property(
     *                     property="message",
     *                     type="string",
     *                     example="not found",
     *                 ),
     *          )
     *       ),
     *      @OA\Response(
     *          response=401,
     *          description="unathorized",
     *           @OA\JsonContent(
     *                 @OA\Property(
     *                     property="status",
     *                     type="string",
     *                     example="error",
     *                 ),
     *                 @OA\Property(
     *                     property="message",
     *                     type="string",
     *                     example="unathorized",
     *                 ),
     *          )
     *       ),
     *      @OA\Response(
     *          response=500,
     *          description="server error",
     *           @OA\JsonContent(
     *                 @OA\Property(
     *                     property="status",
     *                     type="string",
     *                     example="error",
     *                 ),
     *                 @OA\Property(
     *                     property="message",
     *                     type="string",
     *                     example="server error",
     *                 ),
     *          )
     *       ),
     *     )
     */

    public function show(User $user)
    {
        try {
            return new userResource($user);
        } catch (\Throwable $th) {
            return response()->json([
                'status' => 'fail',
                'errors' => ['error' => 'something wrong happened']
            ]);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    /**
     * @OA\Put(
     *      path="/api/user/update/{id}",
     *      operationId="update user",
     *      tags={"User"},
     *      security={{"sanctum":{}}},
     *      summary="update user via id",
     *      description="update user data name,email password via material id",
     *      @OA\Parameter(
     *          description="id",
     *          in="path",
     *          name="id",
     *          required=true,
     *          @OA\Schema(type="string"),
     *          @OA\Examples(example="string", value="1", summary="first user"),
     *      ),
     *      @OA\RequestBody(
     *          @OA\JsonContent(ref="#/components/schemas/User",example={"name":"hussein","email":"husya@gmail.com","password":"123123123@a","privilege":"member"}),
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="ok",
     *           @OA\JsonContent(
     *                 @OA\Property(
     *                     property="status",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="data",
     *                     ref="#/components/schemas/User"
     *                 ),
     *                 example={"status":"success","data":{"name":"hussein","email":"husya@gmail.com","password":"123123123@a","privilege":"member"}}
     *           )
     *       ),
     *      @OA\Response(
     *          response=404,
     *          description="not found",
     *           @OA\JsonContent(
     *                 @OA\Property(
     *                     property="status",
     *                     type="string",
     *                     example="error",
     *                 ),
     *                 @OA\Property(
     *                     property="message",
     *                     type="string",
     *                     example="not found",
     *                 ),
     *          )
     *       ),
     *      @OA\Response(
     *          response=401,
     *          description="unathorized",
     *           @OA\JsonContent(
     *                 @OA\Property(
     *                     property="status",
     *                     type="string",
     *                     example="error",
     *                 ),
     *                 @OA\Property(
     *                     property="message",
     *                     type="string",
     *                     example="unathorized",
     *                 ),
     *          )
     *       ),
     *      @OA\Response(
     *          response=500,
     *          description="server error",
     *           @OA\JsonContent(
     *                 @OA\Property(
     *                     property="status",
     *                     type="string",
     *                     example="error",
     *                 ),
     *                 @OA\Property(
     *                     property="message",
     *                     type="string",
     *                     example="server error",
     *                 ),
     *          )
     *       ),
     *     )
     */
    public function update(UpdateUserRequest $request, User $user)
    {

        try {
            $user->update([
                'name' => $request->name,
                'email' => $request->email,
                'password' => $request->password,
            ]);
            $role = $user->roles[0]->name;
            if ($role !== $request->privilege) {
                $user->removeRole($role);
                $user->assignRole($request->privilege);
            }
            if ($request->wantsJson()) {
                return response()->json([
                    'status' => 'success',
                    'data' => [
                        'name' => $user->name,
                        'email' => $user->email,
                        'privilege' => $user->roles()->pluck('name')->implode('-')

                    ]
                ]);
            } else {
                return back()->with('success', 'User Updated Successfully!');
            }
        } catch (\Throwable $th) {
            if ($request->wantsJson()) {
                return response()->json([
                    'status' => 'fail',
                    'errors' => ['error' => 'something wrong happened']
                ]);
            } else {
                return back()->with('error', 'Something Wrong Happened!');
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    /**
     * @OA\Delete(
     *      path="/api/user/delete/{id}",
     *      operationId="delete user",
     *      tags={"User"},
     *      security={{"sanctum":{}}},
     *      summary="delete user via id",
     *      description="delete user via id",
     *      @OA\Parameter(
     *          description="user id",
     *          in="path",
     *          name="id",
     *          required=true,
     *          @OA\Schema(type="string"),
     *          @OA\Examples(example="string", value="1", summary="first material"),
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="ok",
     *           @OA\JsonContent(
     *                 @OA\Property(
     *                     property="status",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="message",
     *                     type="string",
     *                 ),
     *                 example={"status":"success","message":"deleted successfully"}
     *          )
     *       ),
     *       @OA\Response(
     *          response=404,
     *          description="not found",
     *           @OA\JsonContent(
     *                 @OA\Property(
     *                     property="status",
     *                     type="string",
     *                     example="error",
     *                 ),
     *                 @OA\Property(
     *                     property="message",
     *                     type="string",
     *                     example="not found",
     *                 ),
     *          )
     *       ),
     *      @OA\Response(
     *          response=401,
     *          description="unathorized",
     *           @OA\JsonContent(
     *                 @OA\Property(
     *                     property="status",
     *                     type="string",
     *                     example="error",
     *                 ),
     *                 @OA\Property(
     *                     property="message",
     *                     type="string",
     *                     example="unathorized",
     *                 ),
     *          )
     *       ),
     *      @OA\Response(
     *          response=500,
     *          description="server error",
     *           @OA\JsonContent(
     *                 @OA\Property(
     *                     property="status",
     *                     type="string",
     *                     example="error",
     *                 ),
     *                 @OA\Property(
     *                     property="message",
     *                     type="string",
     *                     example="server error",
     *                 ),
     *          )
     *       ),
     *     )
     */

    public function destroy(Request $request, User $user)
    {
        try {
            if ($request->wantsJson()) {
                $user->tokens()->delete();
            }
            $user->delete();
            if ($request->wantsJson()) {
                return response()->json([
                    'status' => 'success',
                    'message' => 'account deleted'
                ]);
            } else {
                return back()->with('success', 'User Deleted Successfully!');
            }
        } catch (\Throwable $th) {
            if ($request->wantsJson()) {
                return response()->json([
                    'status' => 'fail',
                    'errors' => ['error' => 'something wrong happened']
                ]);
            } else {
                return back()->with('error', 'Something Wrong Happened!');
            }
        }
    }
}
