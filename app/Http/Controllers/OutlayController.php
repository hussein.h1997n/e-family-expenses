<?php

namespace App\Http\Controllers;

use App\Http\Requests\ExpenseDuringMonthRequest;
use App\Http\Requests\ExpenseDuringYearRequest;
use App\Models\Outlay;
use App\Http\Requests\StoreOutlayRequest;
use App\Http\Requests\UpdateOutlayRequest;
use App\Http\Resources\MaterialOutlaysResource;
use App\Http\Resources\OutlayResource;
use App\Http\Resources\ServiceOutlaysCollection;
use App\Http\Resources\ServiceOutlaysResource;
use App\Http\Resources\UserOutlaysResource;
use App\Models\Material;
use App\Models\OutlayType;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Collection;

use function PHPSTORM_META\map;

class OutlayController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    /**
     * @OA\Get(
     *      path="/api/outlay?page={page}",
     *      operationId="get all Outlay",
     *      security={{"sanctum":{}}},
     *      tags={"Outlay"},
     *      summary="get All Outlay",
     *      description="get all Outlay with details id, name, description, etc...",
     *      @OA\Parameter(
     *          description="page number",
     *          in="query",
     *          name="page",
     *          required=true,
     *          @OA\Schema(type="string"),
     *          @OA\Examples(example="string", value="1", summary="first page"),
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="ok",
     *           @OA\JsonContent(
     *                 @OA\Property(
     *                     property="status",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="data",
     *                     type="array",
     *                     @OA\items(ref="#/components/schemas/Outlay")
     *                 ),
     *                 example={"status":"success","data":{{"id":1,"material_id":2,"outlaytype_id":2,"user_id":3,"price":1000,"date":"2022-01-09","description":"from admin to john","updated_at": "2024-05-14T19:01:30.000000Z","created_at": "2024-05-14T19:01:30.000000Z"}}}
     *           )
     *       ),
     *      @OA\Response(
     *          response=404,
     *          description="not found",
     *           @OA\JsonContent(
     *                 @OA\Property(
     *                     property="status",
     *                     type="string",
     *                     example="error",
     *                 ),
     *                 @OA\Property(
     *                     property="message",
     *                     type="string",
     *                     example="not found",
     *                 ),
     *          )
     *       ),
     *      @OA\Response(
     *          response=401,
     *          description="unathorized",
     *           @OA\JsonContent(
     *                 @OA\Property(
     *                     property="status",
     *                     type="string",
     *                     example="error",
     *                 ),
     *                 @OA\Property(
     *                     property="message",
     *                     type="string",
     *                     example="unathorized",
     *                 ),
     *          )
     *       ),
     *      @OA\Response(
     *          response=500,
     *          description="server error",
     *           @OA\JsonContent(
     *                 @OA\Property(
     *                     property="status",
     *                     type="string",
     *                     example="error",
     *                 ),
     *                 @OA\Property(
     *                     property="message",
     *                     type="string",
     *                     example="server error",
     *                 ),
     *          )
     *       ),
     *     )
     */

    public function index(Request $request)
    {

        try {
            $materials = Material::all();
            $outlaytypes = OutlayType::all();
            if (auth()->user()->hasRole('super-admin')) {
                $outlays = Outlay::all();
                $users = User::all();
                $date =  json_encode($this->returnDates());
                $date = json_decode($date, true);
                $years = collect($date['original']['data']['years']);
                $months = collect($date['original']['data']['months']);
            } else if (auth()->user()->hasRole('member')) {
                $outlays = auth()->user()->outlays()->get();
            }
            if ($request->wantsJson()) {
                return response()->json([
                    'status' => 'success',
                    'data' =>  OutlayResource::collection($outlays)
                ]);
            } else {

                if (auth()->user()->hasRole('super-admin')) {
                    $outlays = Outlay::paginate(10);
                    return view('admin.dashboard_outlays', compact(
                        'outlays',
                        'users',
                        'materials',
                        'outlaytypes',
                        'years',
                        'months'
                    ));
                } elseif (auth()->user()->hasRole('member')) {
                    $outlays = auth()->user()->outlays()->paginate(10);
                    return view('user.main', compact(
                        'outlays',
                        'materials',
                        'outlaytypes',
                    ));
                } else {
                    return 'error';
                }
            }
        } catch (\Throwable $th) {
            if ($request->wantsJson()) {
                return response()->json([
                    'status' => 'fail',
                    'errors' => ['error' => 'something wrong happened']
                ]);
            } else {
                return back()->with('error', 'Something Wrong Happened!');
            }
        }
    }


    // public function main(Request $request)
    // {

    //     try {
    //         if (auth()->user()->hasRole('super-admin')) {
    //             $outlays = Outlay::paginate(10);
    //             $users = User::all();
    //             $materials = Material::all();
    //             $outlaytypes = OutlayType::all();
    //             $date =  json_encode($this->returnDates());
    //             $date = json_decode($date, true);
    //             $years = collect($date['original']['data']['years']);
    //             $months = collect($date['original']['data']['months']);
    //         } else if (auth()->user()->hasRole('member')) {
    //             $outlays = auth()->user()->outlays()->paginate(10);
    //         }

    //         if (auth()->user()->hasRole('super-admin')) {
    //             return view('admin.dashboard_outlays', compact(
    //                 'outlays',
    //                 'users',
    //                 'materials',
    //                 'outlaytypes',
    //                 'years',
    //                 'months'
    //             ));
    //         } elseif (auth()->user()->hasRole('member')) {
    //             return view('user.main', compact(
    //                 'outlays',
    //                 'materials',
    //                 'outlaytypes',
    //             ));
    //         } else {
    //             return 'error';
    //         }
    //     } catch (\Throwable $th) {
    //         return back()->with('error', 'Something Wrong Happened!');
    //     }
    // }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreOutlayRequest  $request
     * @return \Illuminate\Http\Response
     */

    /**
     * @OA\Post(
     *      path="/api/outlay",
     *      operationId="add outlay",
     *      tags={"Outlay"},
     *      security={{"sanctum":{}}},
     *      summary="add new outlay",
     *      description="add new outlay with data name,description",
     *      @OA\RequestBody(
     *          @OA\JsonContent(ref="#/components/schemas/Outlay",example={"material_id":2,"outlaytype_id":2,"user_id":3,"price":1000,"date":"2022-01-09","description":"from admin to john"}),
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="ok",
     *           @OA\JsonContent(
     *                 @OA\Property(
     *                     property="status",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="data",
     *                     ref="#/components/schemas/Outlay"
     *                 ),
     *                 example={"status":"success","data":{"id":1,"material_id":2,"outlaytype_id":2,"user_id":3,"price":1000,"date":"2022-01-09","description":"from admin to john","updated_at": "2024-05-14T19:01:30.000000Z","created_at": "2024-05-14T19:01:30.000000Z"}}
     *           )
     *       ),
     *      @OA\Response(
     *          response=404,
     *          description="not found",
     *           @OA\JsonContent(
     *                 @OA\Property(
     *                     property="status",
     *                     type="string",
     *                     example="error",
     *                 ),
     *                 @OA\Property(
     *                     property="message",
     *                     type="string",
     *                     example="not found",
     *                 ),
     *          )
     *       ),
     *      @OA\Response(
     *          response=401,
     *          description="unathorized",
     *           @OA\JsonContent(
     *                 @OA\Property(
     *                     property="status",
     *                     type="string",
     *                     example="error",
     *                 ),
     *                 @OA\Property(
     *                     property="message",
     *                     type="string",
     *                     example="unathorized",
     *                 ),
     *          )
     *       ),
     *      @OA\Response(
     *          response=500,
     *          description="server error",
     *           @OA\JsonContent(
     *                 @OA\Property(
     *                     property="status",
     *                     type="string",
     *                     example="error",
     *                 ),
     *                 @OA\Property(
     *                     property="message",
     *                     type="string",
     *                     example="server error",
     *                 ),
     *          )
     *       ),
     *     )
     */

    public function store(StoreOutlayRequest $request)
    {
        try {
            $outlay = Outlay::create([
                'material_id' => $request->material_id,
                'outlaytype_id' => $request->outlaytype_id,
                'user_id' => auth()->user()->hasRole('super-admin') ? $request->user_id : auth()->user()->id,
                'price' => $request->price,
                'description' => $request->description,
                'date' => Carbon::createFromFormat('Y-m-d', $request->date)
            ]);
            if ($request->wantsJson()) {
                return ['status' => 'success', 'data' => $outlay];
            } else {
                return back()->with('success', 'Outlay added successfully');
            }
        } catch (\Throwable $th) {
            if ($request->wantsJson()) {
                return response()->json([
                    'status' => 'fail',
                    'errors' => ['error' => 'something wrong happened']
                ]);
            } else {
                return back()->with('error', 'something wrong happened');
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Outlay  $outlay
     * @return \Illuminate\Http\Response
     */
    public function show(Outlay $outlay)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Outlay  $outlay
     * @return \Illuminate\Http\Response
     */
    public function edit(Outlay $outlay)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateOutlayRequest  $request
     * @param  \App\Models\Outlay  $outlay
     * @return \Illuminate\Http\Response
     */
    /**
     * @OA\Put(
     *      path="/api/outlay/update/{id}",
     *      operationId="update Outlay",
     *      tags={"Outlay"},
     *      security={{"sanctum":{}}},
     *      summary="update Outlay via id",
     *      description="update Outlay data via outlay id",
     *      @OA\Parameter(
     *          description="id",
     *          in="path",
     *          name="id",
     *          required=true,
     *          @OA\Schema(type="string"),
     *          @OA\Examples(example="string", value="1", summary="first outlay"),
     *      ),
     *      @OA\RequestBody(
     *          @OA\JsonContent(ref="#/components/schemas/Outlay",example={"material_id":2,"outlaytype_id":2,"user_id":3,"price":1000,"date":"2022-01-09","description":"from admin to john"}),
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="ok",
     *           @OA\JsonContent(
     *                 @OA\Property(
     *                     property="status",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="data",
     *                     ref="#/components/schemas/Outlay"
     *                 ),
     *                 example={"status":"success","data":{"id":1,"material_id":2,"outlaytype_id":2,"user_id":3,"price":1000,"date":"2022-01-09","description":"from admin to john","updated_at": "2024-05-14T19:01:30.000000Z","created_at": "2024-05-14T19:01:30.000000Z"}}
     *           )
     *       ),
     *      @OA\Response(
     *          response=404,
     *          description="not found",
     *           @OA\JsonContent(
     *                 @OA\Property(
     *                     property="status",
     *                     type="string",
     *                     example="error",
     *                 ),
     *                 @OA\Property(
     *                     property="message",
     *                     type="string",
     *                     example="not found",
     *                 ),
     *          )
     *       ),
     *      @OA\Response(
     *          response=401,
     *          description="unathorized",
     *           @OA\JsonContent(
     *                 @OA\Property(
     *                     property="status",
     *                     type="string",
     *                     example="error",
     *                 ),
     *                 @OA\Property(
     *                     property="message",
     *                     type="string",
     *                     example="unathorized",
     *                 ),
     *          )
     *       ),
     *      @OA\Response(
     *          response=500,
     *          description="server error",
     *           @OA\JsonContent(
     *                 @OA\Property(
     *                     property="status",
     *                     type="string",
     *                     example="error",
     *                 ),
     *                 @OA\Property(
     *                     property="message",
     *                     type="string",
     *                     example="server error",
     *                 ),
     *          )
     *       ),
     *     )
     */

    public function update(UpdateOutlayRequest $request, $id)
    {
        try {
            if (auth()->user()->hasRole('super-admin')) {
                $outlay = Outlay::find($id);
            } else if (auth()->user()->hasRole('member')) {
                $outlay = auth()->user()->outlays->where('id', $id)->first();
            }
            if ($outlay) {
                $outlay->material_id = $request->material_id;
                $outlay->outlaytype_id = $request->outlaytype_id;
                $outlay->user_id = auth()->user()->hasRole('super-admin') ? $request->user_id : auth()->user()->id;
                // $outlay->user_id = auth()->user()->id;
                $outlay->price = $request->price;
                $outlay->description = $request->description;
                $outlay->date = Carbon::createFromFormat('Y-m-d', $request->date);
                $outlay->save();
                if ($request->wantsJson()) {

                    return ['status' => 'success', 'data' => $outlay];
                } else {
                    return back()->with('success', 'Outlay updated successfully');
                }
            } else {
                if ($request->wantsJson()) {

                    return response()->json([
                        "status" => "fail",
                        "errors" => [
                            "error" => "Data not found."
                        ]
                    ]);
                } else {
                    return back()->with('error', 'Data not found.');
                }
            }
        } catch (\Throwable $th) {
            if ($request->wantsJson()) {
                return response()->json([
                    'status' => 'fail',
                    'errors' => ['error' => 'something wrong happened']
                ]);
            } else {
                return back()->with('error', 'something wrong happened');
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Outlay  $outlay
     * @return \Illuminate\Http\Response
     */
    /**
     * @OA\Delete(
     *      path="/api/outlay/delete/{id}",
     *      operationId="delete outlay",
     *      tags={"Outlay"},
     *      security={{"sanctum":{}}},
     *      summary="delete outlay via id",
     *      description="delete outlay via id",
     *      @OA\Parameter(
     *          description="outlay id",
     *          in="path",
     *          name="id",
     *          required=true,
     *          @OA\Schema(type="string"),
     *          @OA\Examples(example="string", value="1", summary="first outlay"),
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="ok",
     *           @OA\JsonContent(
     *                 @OA\Property(
     *                     property="status",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="message",
     *                     type="string",
     *                 ),
     *                 example={"status":"success","message":"deleted successfully"}
     *          )
     *       ),
     *       @OA\Response(
     *          response=404,
     *          description="not found",
     *           @OA\JsonContent(
     *                 @OA\Property(
     *                     property="status",
     *                     type="string",
     *                     example="error",
     *                 ),
     *                 @OA\Property(
     *                     property="message",
     *                     type="string",
     *                     example="not found",
     *                 ),
     *          )
     *       ),
     *      @OA\Response(
     *          response=401,
     *          description="unathorized",
     *           @OA\JsonContent(
     *                 @OA\Property(
     *                     property="status",
     *                     type="string",
     *                     example="error",
     *                 ),
     *                 @OA\Property(
     *                     property="message",
     *                     type="string",
     *                     example="unathorized",
     *                 ),
     *          )
     *       ),
     *      @OA\Response(
     *          response=500,
     *          description="server error",
     *           @OA\JsonContent(
     *                 @OA\Property(
     *                     property="status",
     *                     type="string",
     *                     example="error",
     *                 ),
     *                 @OA\Property(
     *                     property="message",
     *                     type="string",
     *                     example="server error",
     *                 ),
     *          )
     *       ),
     *     )
     */

    public function destroy(Request $request, $id)
    {
        try {
            if (auth()->user()->hasRole('super-admin')) {
                $outlay = Outlay::find($id);
            } else if (auth()->user()->hasRole('member')) {
                $outlay = auth()->user()->outlays->where('id', $id)->first();
            }
            if ($outlay) {
                $outlay->delete();
                if ($request->wantsJson()) {
                    return response()->json([
                        'status' => 'success',
                        'message' => 'deleted successfully'
                    ]);
                } else {
                    if (auth()->user()->hasRole('super-admin')) {
                        return  redirect(url('dashboard_outlays'))->with('success', 'Outlay deleted successfully');
                    } else if (auth()->user()->hasRole('member')) {
                        return  redirect(url('/home'))->with('success', 'Outlay deleted successfully');
                    }
                }
            } else {
                if ($request->wantsJson()) {

                    return response()->json([
                        "status" => "fail",
                        "errors" => [
                            "error" => "Data not found."
                        ]
                    ]);
                } else {
                    if (auth()->user()->hasRole('super-admin')) {
                        return redirect(route('dashboard_outlays'))->with('error', 'Data not found.');
                    } else if (auth()->user()->hasRole('member')) {
                        return  redirect(url('/home'))->with('error', 'Data not found.');
                    }
                }
            }
        } catch (\Throwable $th) {
            if ($request->wantsJson()) {
                return response()->json([
                    'status' => 'fail',
                    'errors' => ['error' => 'something wrong happened']
                ]);
            } else {
                return back()->with('error', 'something wrong happened');
            }
        }
    }

    /**
     *
     * @OA\Get(
     *      path="/api/dates?page={page}",
     *      operationId="filter on dates",
     *      security={{"sanctum":{}}},
     *      tags={"Outlay"},
     *      summary="filter on dates",
     *      description="filter on dates with details id, name, description, etc...",
     *      @OA\Parameter(
     *          description="page number",
     *          in="query",
     *          name="page",
     *          required=true,
     *          @OA\Schema(type="string"),
     *          @OA\Examples(example="string", value="1", summary="first page"),
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="ok",
     *           @OA\JsonContent(
     *                 @OA\Property(
     *                     property="status",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="data",
     *                     type="array",
     *                     @OA\items(ref="#/components/schemas/Outlay")
     *                 ),
     *                 example={"status":"success","data":{{"id":1,"material_id":2,"outlaytype_id":2,"user_id":3,"price":1000,"date":"2022-01-09","description":"from admin to john","updated_at": "2024-05-14T19:01:30.000000Z","created_at": "2024-05-14T19:01:30.000000Z"}}}
     *           )
     *       ),
     *      @OA\Response(
     *          response=404,
     *          description="not found",
     *           @OA\JsonContent(
     *                 @OA\Property(
     *                     property="status",
     *                     type="string",
     *                     example="error",
     *                 ),
     *                 @OA\Property(
     *                     property="message",
     *                     type="string",
     *                     example="not found",
     *                 ),
     *          )
     *       ),
     *      @OA\Response(
     *          response=401,
     *          description="unathorized",
     *           @OA\JsonContent(
     *                 @OA\Property(
     *                     property="status",
     *                     type="string",
     *                     example="error",
     *                 ),
     *                 @OA\Property(
     *                     property="message",
     *                     type="string",
     *                     example="unathorized",
     *                 ),
     *          )
     *       ),
     *      @OA\Response(
     *          response=500,
     *          description="server error",
     *           @OA\JsonContent(
     *                 @OA\Property(
     *                     property="status",
     *                     type="string",
     *                     example="error",
     *                 ),
     *                 @OA\Property(
     *                     property="message",
     *                     type="string",
     *                     example="server error",
     *                 ),
     *          )
     *       ),
     *     )
     */

    public function returnDates()
    {
        try {
            $dates = Outlay::all()->pluck('date');
            $years = [];
            $months = [];
            foreach ($dates as $key => $date) {
                $date_arr = explode('-', $date);
                array_push($years, $date_arr[0]);
                array_push($months, $date_arr[0] . '-' . $date_arr[1]);
            }
            return response()->json([
                'status' => 'success',
                'data' => ['years' => array_values(array_unique($years)), 'months' => array_values(array_unique($months))]
            ]);
        } catch (\Throwable $th) {
            return response()->json([
                'status' => 'fail',
                'errors' => ['error' => 'something wrong happened']
            ]);
        }
    }

    /**
     *
     * @OA\Get(
     *      path="/api/filter/year?page={page}",
     *      operationId="filter during year",
     *      security={{"sanctum":{}}},
     *      tags={"Outlay"},
     *      summary="filter during year",
     *      description="filter during year with details id, name, description, etc...",
     *      @OA\Parameter(
     *          description="page number",
     *          in="query",
     *          name="page",
     *          required=true,
     *          @OA\Schema(type="string"),
     *          @OA\Examples(example="string", value="1", summary="first page"),
     *      ),
     *      @OA\Parameter(
     *          description="year",
     *          in="query",
     *          name="year",
     *          required=true,
     *          @OA\Schema(type="string"),
     *          @OA\Examples(example="string", value="2020", summary="specific year"),
     *      ),
     *
     *      @OA\Response(
     *          response=200,
     *          description="ok",
     *           @OA\JsonContent(
     *                 @OA\Property(
     *                     property="status",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="data",
     *                     type="array",
     *                     @OA\items(ref="#/components/schemas/Outlay")
     *                 ),
     *                 example={"status":"success","data":{{"id":1,"material_id":2,"outlaytype_id":2,"user_id":3,"price":1000,"date":"2022-01-09","description":"from admin to john","updated_at": "2024-05-14T19:01:30.000000Z","created_at": "2024-05-14T19:01:30.000000Z"}}}
     *           )
     *       ),
     *      @OA\Response(
     *          response=404,
     *          description="not found",
     *           @OA\JsonContent(
     *                 @OA\Property(
     *                     property="status",
     *                     type="string",
     *                     example="error",
     *                 ),
     *                 @OA\Property(
     *                     property="message",
     *                     type="string",
     *                     example="not found",
     *                 ),
     *          )
     *       ),
     *      @OA\Response(
     *          response=401,
     *          description="unathorized",
     *           @OA\JsonContent(
     *                 @OA\Property(
     *                     property="status",
     *                     type="string",
     *                     example="error",
     *                 ),
     *                 @OA\Property(
     *                     property="message",
     *                     type="string",
     *                     example="unathorized",
     *                 ),
     *          )
     *       ),
     *      @OA\Response(
     *          response=500,
     *          description="server error",
     *           @OA\JsonContent(
     *                 @OA\Property(
     *                     property="status",
     *                     type="string",
     *                     example="error",
     *                 ),
     *                 @OA\Property(
     *                     property="message",
     *                     type="string",
     *                     example="server error",
     *                 ),
     *          )
     *       ),
     *     )
     */

    public function expensesDuringYear(ExpenseDuringYearRequest $request)
    {
        try {
            $outlays = Outlay::whereYear('date', $request->year)->get();
            $total_arr = $outlays->pluck('price')->toArray();
            $total = 0;
            if (count($total_arr))
                $total = array_reduce($total_arr, function ($acc, $item) {
                    $acc += $item;
                    return $acc;
                });
            if ($request->wantsJson()) {
                return response()->json([
                    'status' => 'success',
                    'data' => ["outlays" =>  OutlayResource::collection($outlays), "total" => $total]
                ]);
            } else {
                $paginatedOutlays = $outlays->paginate(100);
                return back()->with(['outlays' => $paginatedOutlays, 'total' => $total, 'year' => $request->year, "filter" => "filter_year_button"]);
            }
        } catch (\Throwable $th) {
            if ($request->wantsJson()) {
                return response()->json([
                    'status' => 'fail',
                    'errors' => ['error' => 'something wrong happened']
                ]);
            } else {
                return back()->with('error', 'Something Wrong Happened!');
            }
        }
    }

    /**
     *
     * @OA\Get(
     *      path="/api/filter/month?page={page}",
     *      operationId="filter during month",
     *      security={{"sanctum":{}}},
     *      tags={"Outlay"},
     *      summary="filter during month",
     *      description="filter during month with details id, name, description, etc...",
     *      @OA\Parameter(
     *          description="page number",
     *          in="query",
     *          name="page",
     *          required=true,
     *          @OA\Schema(type="string"),
     *          @OA\Examples(example="string", value="1", summary="first page"),
     *      ),
     *      @OA\Parameter(
     *          description="date",
     *          in="query",
     *          name="date",
     *          required=true,
     *          @OA\Schema(type="string"),
     *          @OA\Examples(example="string", value="2020-05", summary="specific date"),
     *      ),
     *
     *      @OA\Response(
     *          response=200,
     *          description="ok",
     *           @OA\JsonContent(
     *                 @OA\Property(
     *                     property="status",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="data",
     *                     type="array",
     *                     @OA\items(ref="#/components/schemas/Outlay")
     *                 ),
     *                 example={"status":"success","data":{{"id":1,"material_id":2,"outlaytype_id":2,"user_id":3,"price":1000,"date":"2022-01-09","description":"from admin to john","updated_at": "2024-05-14T19:01:30.000000Z","created_at": "2024-05-14T19:01:30.000000Z"}}}
     *           )
     *       ),
     *      @OA\Response(
     *          response=404,
     *          description="not found",
     *           @OA\JsonContent(
     *                 @OA\Property(
     *                     property="status",
     *                     type="string",
     *                     example="error",
     *                 ),
     *                 @OA\Property(
     *                     property="message",
     *                     type="string",
     *                     example="not found",
     *                 ),
     *          )
     *       ),
     *      @OA\Response(
     *          response=401,
     *          description="unathorized",
     *           @OA\JsonContent(
     *                 @OA\Property(
     *                     property="status",
     *                     type="string",
     *                     example="error",
     *                 ),
     *                 @OA\Property(
     *                     property="message",
     *                     type="string",
     *                     example="unathorized",
     *                 ),
     *          )
     *       ),
     *      @OA\Response(
     *          response=500,
     *          description="server error",
     *           @OA\JsonContent(
     *                 @OA\Property(
     *                     property="status",
     *                     type="string",
     *                     example="error",
     *                 ),
     *                 @OA\Property(
     *                     property="message",
     *                     type="string",
     *                     example="server error",
     *                 ),
     *          )
     *       ),
     *     )
     */

    public function expensesDuringMonth(ExpenseDuringMonthRequest $request)
    {
        try {
            $dt = Carbon::createFromFormat('Y-m', $request->date);
            $outlays = Outlay::whereYear('date', $dt->year)->whereMonth('date', $dt->month)->get();
            $total_arr = $outlays->pluck('price')->toArray();
            $total = 0;
            if (count($total_arr))
                $total = array_reduce($total_arr, function ($acc, $item) {
                    $acc += $item;
                    return $acc;
                });
            if ($request->wantsJson()) {
                return response()->json([
                    'status' => 'success',
                    'data' => ["outlays" =>  OutlayResource::collection($outlays), "total" => $total]
                ]);
            } else {
                $paginatedOutlays = $outlays->paginate(10);
                return back()->with([
                    'outlays' => $paginatedOutlays, 'total' => $total, 'month' => $request->date,
                    "filter" => "filter_month_button"
                ]);
            }
        } catch (\Throwable $th) {
            if ($request->wantsJson()) {
                return response()->json([
                    'status' => 'fail',
                    'errors' => ['error' => 'something wrong happened']
                ]);
            } else {
                return back()->with('error', 'Something Wrong Happened!');
            }
        }
    }

    /**
     *
     * @OA\Get(
     *      path="/api/filter/users?page={page}",
     *      operationId="filter on users",
     *      security={{"sanctum":{}}},
     *      tags={"Outlay"},
     *      summary="filter on users",
     *      description="filter on users with details id, name, description, etc...",
     *      @OA\Parameter(
     *          description="page number",
     *          in="query",
     *          name="page",
     *          required=true,
     *          @OA\Schema(type="string"),
     *          @OA\Examples(example="string", value="1", summary="first page"),
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="ok",
     *           @OA\JsonContent(
     *                 @OA\Property(
     *                     property="status",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="data",
     *                     type="array",
     *                     @OA\items(ref="#/components/schemas/Outlay")
     *                 ),
     *                 example={"status":"success","data":{{"id":1,"material_id":2,"outlaytype_id":2,"user_id":3,"price":1000,"date":"2022-01-09","description":"from admin to john","updated_at": "2024-05-14T19:01:30.000000Z","created_at": "2024-05-14T19:01:30.000000Z"}}}
     *           )
     *       ),
     *      @OA\Response(
     *          response=404,
     *          description="not found",
     *           @OA\JsonContent(
     *                 @OA\Property(
     *                     property="status",
     *                     type="string",
     *                     example="error",
     *                 ),
     *                 @OA\Property(
     *                     property="message",
     *                     type="string",
     *                     example="not found",
     *                 ),
     *          )
     *       ),
     *      @OA\Response(
     *          response=401,
     *          description="unathorized",
     *           @OA\JsonContent(
     *                 @OA\Property(
     *                     property="status",
     *                     type="string",
     *                     example="error",
     *                 ),
     *                 @OA\Property(
     *                     property="message",
     *                     type="string",
     *                     example="unathorized",
     *                 ),
     *          )
     *       ),
     *      @OA\Response(
     *          response=500,
     *          description="server error",
     *           @OA\JsonContent(
     *                 @OA\Property(
     *                     property="status",
     *                     type="string",
     *                     example="error",
     *                 ),
     *                 @OA\Property(
     *                     property="message",
     *                     type="string",
     *                     example="server error",
     *                 ),
     *          )
     *       ),
     *     )
     */

    public function expensesUsers(Request $request)
    {

        try {
            $users = User::all();
            if ($request->wantsJson()) {
                return [
                    'status' => 'success',
                    'data' => UserOutlaysResource::collection($users)
                ];
            } else {
                $user_info =  UserOutlaysResource::collection($users);
                // dd(UserOutlaysResource::collection($users));
                $paginatedUsers = $users->pluck('outlays')->flatten()->sortBy('id')->paginate(10);

                return back()->with(['outlays' => $paginatedUsers, 'user_info' => json_encode($user_info), "filter" => "filter_user_button"]);
            }
        } catch (\Throwable $th) {
            if ($request->wantsJson()) {
                return response()->json([
                    'status' => 'fail',
                    'errors' => ['error' => 'something wrong happened']
                ]);
            } else {
                return back()->with('error', 'Something Wrong Happened!');
            }
        }
    }

    /**
     *
     * @OA\Get(
     *      path="/api/filter/materials?page={page}",
     *      operationId="filter on expenses materials",
     *      security={{"sanctum":{}}},
     *      tags={"Outlay"},
     *      summary="filter on expenses materials",
     *      description="filter on expenses materials with details id, name, description, etc...",
     *      @OA\Parameter(
     *          description="page number",
     *          in="query",
     *          name="page",
     *          required=true,
     *          @OA\Schema(type="string"),
     *          @OA\Examples(example="string", value="1", summary="first page"),
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="ok",
     *           @OA\JsonContent(
     *                 @OA\Property(
     *                     property="status",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="data",
     *                     type="array",
     *                     @OA\items(ref="#/components/schemas/Outlay")
     *                 ),
     *                 example={"status":"success","data":{{"id":1,"material_id":2,"outlaytype_id":2,"user_id":3,"price":1000,"date":"2022-01-09","description":"from admin to john","updated_at": "2024-05-14T19:01:30.000000Z","created_at": "2024-05-14T19:01:30.000000Z"}}}
     *           )
     *       ),
     *      @OA\Response(
     *          response=404,
     *          description="not found",
     *           @OA\JsonContent(
     *                 @OA\Property(
     *                     property="status",
     *                     type="string",
     *                     example="error",
     *                 ),
     *                 @OA\Property(
     *                     property="message",
     *                     type="string",
     *                     example="not found",
     *                 ),
     *          )
     *       ),
     *      @OA\Response(
     *          response=401,
     *          description="unathorized",
     *           @OA\JsonContent(
     *                 @OA\Property(
     *                     property="status",
     *                     type="string",
     *                     example="error",
     *                 ),
     *                 @OA\Property(
     *                     property="message",
     *                     type="string",
     *                     example="unathorized",
     *                 ),
     *          )
     *       ),
     *      @OA\Response(
     *          response=500,
     *          description="server error",
     *           @OA\JsonContent(
     *                 @OA\Property(
     *                     property="status",
     *                     type="string",
     *                     example="error",
     *                 ),
     *                 @OA\Property(
     *                     property="message",
     *                     type="string",
     *                     example="server error",
     *                 ),
     *          )
     *       ),
     *     )
     */

    public function expensesMaterials(Request $request)
    {
        try {
            $materials = Material::all();
            if ($request->wantsJson()) {
                return [
                    'status' => 'success',
                    'data' => $this->paginate(MaterialOutlaysResource::collection($materials))
                ];
            } else {
                $material_info = MaterialOutlaysResource::collection($materials);
                $materialPagination = $materials->pluck('outlays')->flatten()->sortBy('id')->paginate(10);
                return back()->with([
                    'outlays' => $materialPagination,
                    'material_info' => json_encode($material_info), "filter" => "filter_material_button"
                ]);
            }
        } catch (\Throwable $th) {
            if ($request->wantsJson()) {
                return response()->json([
                    'status' => 'fail',
                    'errors' => ['error' => 'something wrong happened']
                ]);
            } else {
                return back()->with('error', 'Something Wrong Happened!');
            }
        }
    }

    /**
     * /api/filter/materials
     * @OA\Get(
     *      path="/api/filter/services?page={page}",
     *      operationId="filter on expenses services",
     *      security={{"sanctum":{}}},
     *      tags={"Outlay"},
     *      summary="filter on expenses services",
     *      description="filter on expenses services with details id, name, description, etc...",
     *      @OA\Parameter(
     *          description="page number",
     *          in="query",
     *          name="page",
     *          required=true,
     *          @OA\Schema(type="string"),
     *          @OA\Examples(example="string", value="1", summary="first page"),
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="ok",
     *           @OA\JsonContent(
     *                 @OA\Property(
     *                     property="status",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="data",
     *                     type="array",
     *                     @OA\items(ref="#/components/schemas/Outlay")
     *                 ),
     *                 example={"status":"success","data":{{"id":1,"material_id":2,"outlaytype_id":2,"user_id":3,"price":1000,"date":"2022-01-09","description":"from admin to john","updated_at": "2024-05-14T19:01:30.000000Z","created_at": "2024-05-14T19:01:30.000000Z"}}}
     *           )
     *       ),
     *      @OA\Response(
     *          response=404,
     *          description="not found",
     *           @OA\JsonContent(
     *                 @OA\Property(
     *                     property="status",
     *                     type="string",
     *                     example="error",
     *                 ),
     *                 @OA\Property(
     *                     property="message",
     *                     type="string",
     *                     example="not found",
     *                 ),
     *          )
     *       ),
     *      @OA\Response(
     *          response=401,
     *          description="unathorized",
     *           @OA\JsonContent(
     *                 @OA\Property(
     *                     property="status",
     *                     type="string",
     *                     example="error",
     *                 ),
     *                 @OA\Property(
     *                     property="message",
     *                     type="string",
     *                     example="unathorized",
     *                 ),
     *          )
     *       ),
     *      @OA\Response(
     *          response=500,
     *          description="server error",
     *           @OA\JsonContent(
     *                 @OA\Property(
     *                     property="status",
     *                     type="string",
     *                     example="error",
     *                 ),
     *                 @OA\Property(
     *                     property="message",
     *                     type="string",
     *                     example="server error",
     *                 ),
     *          )
     *       ),
     *     )
     */

    public function expensesServices(Request $request)
    {
        try {
            $materials = Material::where('isService', true)->get();
            if ($request->wantsJson()) {
                return [
                    'status' => 'success',
                    'data' => new ServiceOutlaysCollection($materials)
                ];
            } else {
                $service_info = new ServiceOutlaysCollection($materials);
                $service_info = $service_info->toArray($materials);
                $service_info_pagination = $service_info['services']->pluck('outlays')->flatten()->sortBy('id');
                return back()->with([
                    'outlays' => $service_info_pagination->paginate(10),
                    'service_info' => json_encode($service_info), "filter" => "filter_service_button"
                ]);
            }
        } catch (\Throwable $th) {
            if ($request->wantsJson()) {
                return response()->json([
                    'status' => 'fail',
                    'errors' => ['error' => 'something wrong happened']
                ]);
            } else {
                return back()->with('error', 'Something Wrong Happened!');
            }
        }
    }
}
