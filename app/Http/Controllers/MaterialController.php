<?php

namespace App\Http\Controllers;

use App\Models\Material;
use App\Http\Requests\StoreMaterialRequest;
use App\Http\Requests\UpdateMaterialRequest;
use Illuminate\Http\Request;

class MaterialController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    /**
     * @OA\Get(
     *      path="/api/material?page={page}",
     *      operationId="get all materials",
     *      security={{ "sanctum":{} }},
     *      tags={"Materials"},
     *      summary="get All materials",
     *      description="get all materials with details id, name, description, etc...",
     *      @OA\Parameter(
     *          description="page number",
     *          in="query",
     *          name="page",
     *          required=true,
     *          @OA\Schema(type="string"),
     *          @OA\Examples(example="string", value="1", summary="first page"),
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="ok",
     *           @OA\JsonContent(
     *                 @OA\Property(
     *                     property="status",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="data",
     *                     type="array",
     *                     @OA\items(ref="#/components/schemas/Material")
     *                 ),
     *                  example={"status":"success","data":{{"id": 1,"name": "Electricity Service","isService": 1,"description": "pay money for electricity 24/24", "created_at": "2024-05-14T10:00:14.000000Z","updated_at": "2024-05-14T10:00:14.000000Z"}}}
     *           )
     *       ),
     *      @OA\Response(
     *          response=404,
     *          description="not found",
     *           @OA\JsonContent(
     *                 @OA\Property(
     *                     property="status",
     *                     type="string",
     *                     example="error",
     *                 ),
     *                 @OA\Property(
     *                     property="message",
     *                     type="string",
     *                     example="not found",
     *                 ),
     *          )
     *       ),
     *      @OA\Response(
     *          response=401,
     *          description="unathorized",
     *           @OA\JsonContent(
     *                 @OA\Property(
     *                     property="status",
     *                     type="string",
     *                     example="error",
     *                 ),
     *                 @OA\Property(
     *                     property="message",
     *                     type="string",
     *                     example="unathorized",
     *                 ),
     *          )
     *       ),
     *      @OA\Response(
     *          response=500,
     *          description="server error",
     *           @OA\JsonContent(
     *                 @OA\Property(
     *                     property="status",
     *                     type="string",
     *                     example="error",
     *                 ),
     *                 @OA\Property(
     *                     property="message",
     *                     type="string",
     *                     example="server error",
     *                 ),
     *          )
     *       ),
     *     )
     */

    public function index(Request $request)
    {
        try {
            $materials = Material::all();
            if ($request->wantsJson()) {
                return ['status' => 'success', 'data' => $materials];
            } else {
                return view('admin.dashboard_materials', compact('materials'));
            }
        } catch (\Throwable $th) {
            if ($request->wantsJson()) {
                return response()->json([
                    'status' => 'fail',
                    'errors' => ['error' => 'something wrong happened']
                ]);
            } else {
                return back()->with('error', 'Something Wrong Happened!');
            }
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreMaterialRequest  $request
     * @return \Illuminate\Http\Response
     */

    /**
     * @OA\Post(
     *      path="/api/material",
     *      operationId="add material",
     *      tags={"Materials"},
     *      security={{ "sanctum":{} }},
     *      summary="add new material",
     *      description="add new material with data name,description,isService,etc...",
     *      @OA\RequestBody(
     *          @OA\JsonContent(
     *                 @OA\Property(
     *                     property="name",
     *                     type="string",
     *                 ),
     *                 @OA\Property(
     *                     property="description",
     *                     type="string",
     *                 ),
     *                 @OA\Property(
     *                     property="isService",
     *                     type="boolean",
     *                 ),
     *                 example={"name":"water","description":"pure water","isService":true}
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="ok",
     *          @OA\JsonContent(
     *              @OA\Property(
     *                  property="status",
     *                  type="string",
     *              ),
     *              @OA\Property( property="data",ref="#/components/schemas/Material"),
     *              example={"status":"success","data":{"id": 1,"name": "Electricity Service","isService": 1,"description": "pay money for electricity 24/24", "created_at": "2024-05-14T10:00:14.000000Z", "updated_at": "2024-05-14T10:00:14.000000Z"}}
     *          ),
     *      ),
     *      @OA\Response(
     *          response=404,
     *          description="not found",
     *           @OA\JsonContent(
     *                 @OA\Property(
     *                     property="status",
     *                     type="string",
     *                     example="error",
     *                 ),
     *                 @OA\Property(
     *                     property="message",
     *                     type="string",
     *                     example="not found",
     *                 ),
     *          )
     *       ),
     *      @OA\Response(
     *          response=401,
     *          description="unathorized",
     *           @OA\JsonContent(
     *                 @OA\Property(
     *                     property="status",
     *                     type="string",
     *                     example="error",
     *                 ),
     *                 @OA\Property(
     *                     property="message",
     *                     type="string",
     *                     example="unathorized",
     *                 ),
     *          )
     *       ),
     *      @OA\Response(
     *          response=500,
     *          description="server error",
     *           @OA\JsonContent(
     *                 @OA\Property(
     *                     property="status",
     *                     type="string",
     *                     example="error",
     *                 ),
     *                 @OA\Property(
     *                     property="message",
     *                     type="string",
     *                     example="server error",
     *                 ),
     *          )
     *       ),
     *     )
     */

    public function store(StoreMaterialRequest $request)
    {
        try {
            $material = Material::create([
                'name' => $request->name,
                'description' => $request->description,
                'isService' => ($request->isService === null) ? false : true,
            ]);
            if ($request->wantsJson()) {
                return ['status' => 'success', 'data' => $material];
            } else {
                return back()->with('success', 'Material added successfully');
            }
        } catch (\Throwable $th) {
            if ($request->wantsJson()) {
                return response()->json([
                    'status' => 'fail',
                    'errors' => ['error' => 'something wrong happened']
                ]);
            } else {
                return back()->with('error', 'something wrong happened');
            }
        }
    }
    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Material  $material
     * @return \Illuminate\Http\Response
     */

    /**
     * @OA\Get(
     *      path="/api/material/{id}",
     *      operationId="get single material",
     *      security={{ "sanctum":{} }},
     *      tags={"Materials"},
     *      summary="get single material",
     *      description="get single material with details id, name, description, etc...",
     *      @OA\Parameter(
     *          description="material id",
     *          in="path",
     *          name="id",
     *          required=true,
     *          @OA\Schema(type="string"),
     *          @OA\Examples(example="string", value="1", summary="first material"),
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="ok",
     *          @OA\JsonContent(
     *              @OA\Property(
     *                  property="status",
     *                  type="string",
     *              ),
     *              @OA\Property( property="data",ref="#/components/schemas/Material"),
     *              example={"status":"success","data":{"id": 1,"name": "Electricity Service","isService": 1,"description": "pay money for electricity 24/24", "created_at": "2024-05-14T10:00:14.000000Z", "updated_at": "2024-05-14T10:00:14.000000Z"}}
     *          ),
     *      ),
     *      @OA\Response(
     *          response=404,
     *          description="not found",
     *           @OA\JsonContent(
     *                 @OA\Property(
     *                     property="status",
     *                     type="string",
     *                     example="error",
     *                 ),
     *                 @OA\Property(
     *                     property="message",
     *                     type="string",
     *                     example="not found",
     *                 ),
     *          )
     *       ),
     *      @OA\Response(
     *          response=401,
     *          description="unathorized",
     *           @OA\JsonContent(
     *                 @OA\Property(
     *                     property="status",
     *                     type="string",
     *                     example="error",
     *                 ),
     *                 @OA\Property(
     *                     property="message",
     *                     type="string",
     *                     example="unathorized",
     *                 ),
     *          )
     *       ),
     *      @OA\Response(
     *          response=500,
     *          description="server error",
     *           @OA\JsonContent(
     *                 @OA\Property(
     *                     property="status",
     *                     type="string",
     *                     example="error",
     *                 ),
     *                 @OA\Property(
     *                     property="message",
     *                     type="string",
     *                     example="server error",
     *                 ),
     *          )
     *       ),
     *     )
     */
    public function show(Material $material)
    {
        try {
            return response()->json([
                'status' => 'success',
                'data' => $material
            ]);
        } catch (\Throwable $th) {
            return response()->json([
                'status' => 'fail',
                'errors' => ['error' => 'something wrong happened']
            ]);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Material  $material
     * @return \Illuminate\Http\Response
     */
    public function edit(Material $material)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateMaterialRequest  $request
     * @param  \App\Models\Material  $material
     * @return \Illuminate\Http\Response
     */

    /**
     * @OA\Put(
     *      path="/api/material/update/{id}",
     *      operationId="update material",
     *      tags={"Materials"},
     *      security={{ "sanctum":{} }},
     *      summary="update material via id",
     *      description="update material data name,description,isService,etc... via material id",
     *      @OA\Parameter(
     *          description="id",
     *          in="path",
     *          name="id",
     *          required=true,
     *          @OA\Schema(type="string"),
     *          @OA\Examples(example="string", value="1", summary="first material"),
     *      ),
     *      @OA\RequestBody(
     *          @OA\JsonContent(
     *                 @OA\Property(
     *                     property="name",
     *                     type="string",
     *                 ),
     *                 @OA\Property(
     *                     property="description",
     *                     type="string",
     *                 ),
     *                 @OA\Property(
     *                     property="isService",
     *                     type="boolean",
     *                 ),
     *                 example={"name":"water","description":"pure water","isService":true}
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="ok",
     *          @OA\JsonContent(
     *              @OA\Property(
     *                  property="status",
     *                  type="string",
     *              ),
     *              @OA\Property( property="data",ref="#/components/schemas/Material"),
     *              example={"status":"success","data":{"id": 1,"name": "Electricity Service","isService": 1,"description": "pay money for electricity 24/24", "created_at": "2024-05-14T10:00:14.000000Z", "updated_at": "2024-05-14T10:00:14.000000Z"}}
     *          ),
     *      ),
     *      @OA\Response(
     *          response=404,
     *          description="not found",
     *           @OA\JsonContent(
     *                 @OA\Property(
     *                     property="status",
     *                     type="string",
     *                     example="error",
     *                 ),
     *                 @OA\Property(
     *                     property="message",
     *                     type="string",
     *                     example="not found",
     *                 ),
     *          )
     *       ),
     *      @OA\Response(
     *          response=401,
     *          description="unathorized",
     *           @OA\JsonContent(
     *                 @OA\Property(
     *                     property="status",
     *                     type="string",
     *                     example="error",
     *                 ),
     *                 @OA\Property(
     *                     property="message",
     *                     type="string",
     *                     example="unathorized",
     *                 ),
     *          )
     *       ),
     *      @OA\Response(
     *          response=500,
     *          description="server error",
     *           @OA\JsonContent(
     *                 @OA\Property(
     *                     property="status",
     *                     type="string",
     *                     example="error",
     *                 ),
     *                 @OA\Property(
     *                     property="message",
     *                     type="string",
     *                     example="server error",
     *                 ),
     *          )
     *       ),
     *     )
     */

    public function update(UpdateMaterialRequest $request, Material $material)
    {
        try {
            $material->name = $request->name;
            $material->description = $request->description;
            $material->isService = ($request->isService === null) ? false : true;
            $material->save();
            if ($request->wantsJson()) {
                return response()->json([
                    'status' => 'success',
                    'data' => $material,
                ]);
            } else {
                return back()->with('success', 'Material updated successfully');
            }
        } catch (\Throwable $th) {
            if ($request->wantsJson()) {
                return response()->json([
                    'status' => 'fail',
                    'errors' => ['error' => 'something wrong happened']
                ]);
            } else {
                return back()->with('error', 'something wrong happened');
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Material  $material
     * @return \Illuminate\Http\Response
     */

    /**
     * @OA\Delete(
     *      path="/api/material/delete/{id}",
     *      operationId="delete meterial",
     *      tags={"Materials"},
     *      security={{ "sunctom":{} }},
     *      summary="delete material via id",
     *      description="delete material via id",
     *      @OA\Parameter(
     *          description="material id",
     *          in="path",
     *          name="id",
     *          required=true,
     *          @OA\Schema(type="string"),
     *          @OA\Examples(example="string", value="1", summary="first material"),
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="ok",
     *           @OA\JsonContent(
     *                 @OA\Property(
     *                     property="status",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="message",
     *                     type="string",
     *                 ),
     *                 example={"status":"success","message":"deleted successfully"}
     *          )
     *       ),
     * @OA\Response(
     *          response=404,
     *          description="not found",
     *           @OA\JsonContent(
     *                 @OA\Property(
     *                     property="status",
     *                     type="string",
     *                     example="error",
     *                 ),
     *                 @OA\Property(
     *                     property="message",
     *                     type="string",
     *                     example="not found",
     *                 ),
     *          )
     *       ),
     *      @OA\Response(
     *          response=401,
     *          description="unathorized",
     *           @OA\JsonContent(
     *                 @OA\Property(
     *                     property="status",
     *                     type="string",
     *                     example="error",
     *                 ),
     *                 @OA\Property(
     *                     property="message",
     *                     type="string",
     *                     example="unathorized",
     *                 ),
     *          )
     *       ),
     *      @OA\Response(
     *          response=500,
     *          description="server error",
     *           @OA\JsonContent(
     *                 @OA\Property(
     *                     property="status",
     *                     type="string",
     *                     example="error",
     *                 ),
     *                 @OA\Property(
     *                     property="message",
     *                     type="string",
     *                     example="server error",
     *                 ),
     *          )
     *       ),
     *     )
     */

    public function destroy(Request $request, Material $material)
    {
        try {
            if ($material) {
                $material->delete();
                if ($request->wantsJson()) {
                    return response()->json([
                        'status' => 'success',
                        'message' => 'deleted successfully'
                    ]);
                } else {
                    return back()->with('success', 'Material deleted successfully');
                }
            } else {
                if ($request->wantsJson()) {
                    return response()->json([
                        'status' => 'fail',
                        'errors' => ['error' => 'no material with id ' + $material->id]
                    ]);
                } else {
                    return back()->with('error', 'no material with id ' + $material->id);
                }
            }
        } catch (\Throwable $th) {
            if ($request->wantsJson()) {
                return response()->json([
                    'status' => 'fail',
                    'errors' => ['error' => 'something wrong happened']
                ]);
            } else {
                return back()->with('error', 'something wrong happened');
            }
        }
    }
}
