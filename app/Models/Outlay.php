<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Outlay extends Model
{
    use HasFactory;
    protected $fillable = [
        'price', 'description', 'material_id', 'outlaytype_id', 'user_id', 'date'
    ];
    public function material()
    {
        return $this->belongsTo(Material::class, 'material_id', 'id');
    }
    public function outlaytype()
    {
        return $this->belongsTo(OutlayType::class, 'outlaytype_id', 'id');
    }
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
