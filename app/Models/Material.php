<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Material extends Model
{
    use HasFactory;
    protected $fillable = [
        'name',
        'isService',
        'description',
    ];
    public function outlays()
    {
        return $this->hasMany(Outlay::class, 'material_id', 'id');
    }
}
