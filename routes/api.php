<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\MaterialController;
use App\Http\Controllers\OutlayController;
use App\Http\Controllers\OutlayTypeController;
use App\Http\Controllers\UserController;
use App\Models\OutlayType;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->group(function () {
    Route::get('/userinfo', [AuthController::class, 'userInfo']);
    Route::post('/logout', [AuthController::class, 'logout']);
    Route::delete('/outlay/delete/{outlay}', [OutlayController::class, 'destroy'])->name('outlay_destroy');
    Route::put('/outlay/update/{outlay}', [OutlayController::class, 'update'])->name('outlay_update');
    Route::apiResource('/outlay', OutlayController::class);
    Route::get('/outlaytype', [OutlayTypeController::class, 'index']);
    Route::get('/material', [MaterialController::class, 'index']);
    Route::apiResource('/material', MaterialController::class)->only('index', 'show');
});

Route::middleware(['auth:sanctum', 'role:super-admin'])->group(function () {
    Route::delete('/outlaytype/delete/{outlaytype}', [OutlayTypeController::class, 'destroy'])
        ->name('outlaytype_destroy');
    Route::put('/outlaytype/update/{outlaytype}', [OutlayTypeController::class, 'update'])
        ->name('outlaytype_update');
    Route::delete('/user/delete/{user}', [UserController::class, 'destroy'])->name('user_destroy');
    Route::put('/user/update/{user}', [UserController::class, 'update'])->name('user_update');
    Route::delete('/material/delete/{material}', [MaterialController::class, 'destroy'])
        ->name('material_destroy');
    Route::put('/material/update/{material}', [MaterialController::class, 'update'])
        ->name('material_update');
    Route::apiResource('/outlaytype', OutlayTypeController::class)->except('index');
    Route::apiResource('/material', MaterialController::class)->except('index', 'show');
    Route::apiResource('/user', UserController::class);
    Route::get('/dates', [OutlayController::class, 'returnDates']);
    Route::get('/filter/year', [OutlayController::class, 'expensesDuringYear']);
    Route::get('/filter/month', [OutlayController::class, 'expensesDuringMonth']);
    Route::get('/filter/users', [OutlayController::class, 'expensesUsers']);
    Route::get('/filter/materials', [OutlayController::class, 'expensesMaterials']);
    Route::get('/filter/services', [OutlayController::class, 'expensesServices']);
});


Route::post('/login', [AuthController::class, 'Login']);
