<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\MaterialController;
use App\Http\Controllers\OutlayController;
use App\Http\Controllers\OutlayTypeController;
use App\Http\Controllers\UserController;
use App\Models\Outlay;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/login');
});
Route::middleware('auth')->group(function () {
    Route::get('/userinfo', function (Request $request) {
        return $request->user()->only(['id', 'name', 'email']);
    });
    Route::post('/logout', [AuthController::class, 'logout'])->name('logout');
    Route::apiResource('/outlay', OutlayController::class);
    Route::get('/outlaytype', [OutlayTypeController::class, 'index']);
    Route::get('/material', [MaterialController::class, 'index']);
    Route::get('/home', [OutlayController::class, 'index'])->name('main');
});

Route::middleware(['auth', 'role:super-admin'])->group(function () {
    Route::apiResource('/outlaytype', OutlayTypeController::class)->except('index');
    Route::apiResource('/material', MaterialController::class)->except('index');
    Route::apiResource('/user', UserController::class);
    Route::get('/dates', [OutlayController::class, 'returnDates']);
    Route::get('/filter-year', [OutlayController::class, 'expensesDuringYear'])->name('filter_year');
    Route::get('/filter/month', [OutlayController::class, 'expensesDuringMonth'])->name('filter_month');
    Route::get('/filter/users', [OutlayController::class, 'expensesUsers'])->name('filter_user');
    Route::get('/filter/materials', [OutlayController::class, 'expensesMaterials'])->name('filter_material');
    Route::get('/filter/services', [OutlayController::class, 'expensesServices'])->name('filter_service');
    Route::get('/dashboard_users', [UserController::class, 'index'])->name('dashboard_usres');
    Route::get('/dashboard_materials', [MaterialController::class, 'index'])->name('dashboard_materials');
    Route::get('/dashboard_outlaytypes', [OutlayTypeController::class, 'index'])->name('dashboard_outlaytypes');
    Route::get('/dashboard_outlays', [OutlayController::class, 'index'])->name('dashboard_outlays');
});

Route::middleware('guest')->group(function () {
    Route::get('/login', [AuthController::class, 'showLogin'])->name('login');
    Route::post('/login', [AuthController::class, 'Login'])->name('login');
});
