<div>
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 style="font-family: 'Cairo', sans-serif;" class="modal-title" id="exampleModalLabel">
                        {{ $title }}
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <!-- add_form -->
                    <form action="{{ $action }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="row">
                            <div class="col">
                                <label for="name" class="mr-sm-2">الاسم
                                    :</label>
                                <input id="Name" type="text" name="name" class="form-control">
                            </div>

                            <div class="col">
                                <label for="inputName" class="control-label">اسم الفئة</label>
                                <select name="category_id" class="custom-select" onchange="console.log($(this).val())">
                                    <!--placeholder-->
                                    <option value="" selected disabled> --اختر الفئة --
                                    </option>
                                    @foreach ($doctors as $cat)
                                    <option value="{{ $cat->id }}"> {{ $cat->name }}
                                    </option>
                                    @endforeach
                                </select>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col">
                                <label for="name" class="mr-sm-2">السعر
                                    :</label>
                                <input id="Name" type="text" name="price" class="form-control">
                            </div>
                            <div class="col">
                                <label for="Name_en" class="mr-sm-2">ساعات الكورس
                                    :</label>
                                <input type="text" class="form-control" name="hours_number">
                            </div>
                        </div>

                        <div class="row">
                            <div class="col">
                                <label for="name" class="mr-sm-2">عدد الجلسات
                                    :</label>
                                <input id="Name" type="text" name="sections_number" class="form-control">
                            </div>

                            <div class="col">
                                <label for="name" class="mr-sm-2">اسم المدرس
                                    :</label>
                                <select class="form-control" name="teacher_id">
                                </select>
                            </div>

                        </div>

                        <div class="form-group">
                            <label for="exampleFormControlTextarea1">التفاصيل
                                :</label>
                            <textarea class="form-control" name="details" id="exampleFormControlTextarea1"
                                rows="3"></textarea>
                        </div>

                        <div class="form-group">
                            <label for="img">صورة الاعلان
                                :</label>
                            <input id="img" type="file" name="img">
                        </div>
                        <br><br>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">إالغاء</button>
                            <button type="submit" class="btn btn-success">إرسال</button>
                        </div>
                    </form>

                </div>
            </div>
        </div>



    </div>
</div>