@extends('layouts.master')
@section('css')
<link rel="stylesheet" type="text/css" href="https://code.jquery.com/ui/1.13.2/themes/base/jquery-ui.css" />
@section('title')
Users
@stop
@endsection
@section('page-header')
<!-- breadcrumb -->
<div class="page-title" style="margin-top: 4rem;">
    <div class="row">

        <div class="col-sm-6">
            <h4 class="mb-3">Users Table</h4>
        </div>
    </div>
</div>
<!-- breadcrumb -->
@endsection
@section('content')
<div class="row">
    <div class="col-md-12 mb-30">
        <div class="card card-statistics h-100">
            <div class="card-body">

                <div class="col-xl-12 mb-30 w-100" style="padding:0">
                    <div class=" h-100">
                        <div class="">
                            @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                            @endif
                            <div class="container bootstrap snippet ">
                                <div class="row" style="flex-direction: col-reverse;">
                                    <div class="col-sm-12" style="padding:0">
                                        <div class="tab-pane fade show active" id="users">
                                            <div name="exclude" id="exclude">
                                                <button type="button"
                                                    class="btn btn-primary btn-sm ml-auto d-block py-2 px-3 rounded my-3"
                                                    data-toggle="modal" data-target="#add_user"
                                                    title="Add new user   ">Add <i class="fa fa-add"></i></button>
                                                <div class="table-responsive">
                                                    <table id="datatable1"
                                                        class="table  table-hover table-md table-borded  p-2"
                                                        data-page-length="50" style="text-align: center">
                                                        <thead>
                                                            <tr>
                                                                <th>#</th>
                                                                <th>Name</th>
                                                                <th>Email</th>
                                                                <th>Password</th>
                                                                <th>privilege</th>
                                                                <th>Actions</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <?php $i = 0; ?>
                                                            @foreach ($users as $user)
                                                            <?php $i++; $user_privilege = $user->roles->pluck('name')->implode('-'); ?>
                                                            <tr>
                                                                <th>{{ $i }}</th>
                                                                <th>{{ $user->name }}</th>
                                                                <th>{{ $user->email }}</th>
                                                                <th>{{ $user->password }}</th>
                                                                <th>{{
                                                                    $user_privilege
                                                                    }}</th>
                                                                <td>
                                                                    <button type="button"
                                                                        class="btn btn-info btn-sm d-inline"
                                                                        data-toggle="modal"
                                                                        data-target="#edit_user{{ $user->id }}"
                                                                        title="Edit"><i class="fa fa-edit"></i></button>
                                                                    <button type="button"
                                                                        class="btn btn-danger btn-sm d-inline"
                                                                        data-toggle="modal"
                                                                        data-target="#delete_modal{{ $user->id }}"
                                                                        title="Delete User"><i
                                                                            class="fa fa-trash"></i></button>
                                                                </td>

                                                            </tr>

                                                            <x-edit-modal :id="$user->id" title="user" action="user">
                                                                <div class="mb-3 mt-2"
                                                                    style="display:flex;flex-direction:column;gap:1.5rem;padding:1rem;padding-top:0;padding-bottom:0">
                                                                    <div>
                                                                        <label for="name" class="mr-sm-2">Name
                                                                            :</label>
                                                                        <input id="name" value="{{ $user->name }}"
                                                                            type="text" name="name"
                                                                            class="form-control" />
                                                                    </div>
                                                                    <div>
                                                                        <label for="email" class="mr-sm-2">Email
                                                                            :</label>
                                                                        <input type="text" value="{{ $user->email }}"
                                                                            class="form-control" name="email" />
                                                                    </div>
                                                                    <div>
                                                                        <label for="password" class="mr-sm-2">Password
                                                                            :</label>
                                                                        <input type="text" value="{{ $user->password }}"
                                                                            class="form-control" name="password" />
                                                                    </div>
                                                                    <div>
                                                                        <label for="privileges"
                                                                            class="mr-sm-2">privileges
                                                                            :</label>
                                                                        <select
                                                                            class="privileges form-select-lg mb-3 form-control h-100"
                                                                            id="privileges"
                                                                            aria-label="Default select example"
                                                                            name="privilege">
                                                                            <option value="super-admin"
                                                                                {{$user_privilege=="super-admin"
                                                                                ?'selected':''}}>
                                                                                Super Admin
                                                                            </option>
                                                                            <option value="member"
                                                                                {{$user_privilege=="member"
                                                                                ?'selected':''}}>Member</option>

                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </x-edit-modal>


                                                            <x-delete-modal :id="$user->id" :title="$user->name"
                                                                action="user.destroy" />

                                                            @endforeach
                                                        </tbody>


                                                    </table>
                                                </div>
                                            </div>
                                        </div>

                                        <x-add-modal title="user" action='user.store'>
                                            <div class="mb-3 mt-2"
                                                style="display:flex;flex-direction:column;gap:1.5rem;padding:1rem;padding-top:0;padding-bottom:0">
                                                <div>
                                                    <label for="name" class="mr-sm-2">Name
                                                        :</label>
                                                    <input id="Name" type="text" name="name" class="form-control"
                                                        value="{{ old('name') }}" />
                                                </div>
                                                <div>
                                                    <label for="Name_en" class="mr-sm-2">Email
                                                        :</label>
                                                    <input type="text" class="form-control" name="email" value="{{
                                                        old('email') }}" />
                                                </div>
                                                <div>
                                                    <label for="password" class="mr-sm-2">Password
                                                        :</label>
                                                    <input id="password" name="password" type="text"
                                                        class="form-control" value="{{ old('password') }}" />
                                                </div>
                                                <div>
                                                    <label for="privileges" class="mr-sm-2">privileges
                                                        :</label>
                                                    <select class="privileges form-select-lg mb-3 form-control h-100"
                                                        id="privileges" aria-label="Default select example"
                                                        name="privilege">
                                                        <option {{ old('privilege')=="" ?'selected':"" }} disabled
                                                            value="">--choose
                                                            a specially --</option>
                                                        <option value="super-admin" {{ old('privilege')=="super-admin"
                                                            ?'selected':"" }}>Super Admin
                                                        </option>
                                                        <option value="member" {{ old('privilege')=="member"
                                                            ?'selected':"" }}>Member</option>

                                                    </select>
                                                </div>
                                            </div>
                                        </x-add-modal>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('js')
<script src="https://code.jquery.com/ui/1.13.1/jquery-ui.min.js"
    integrity="sha256-eTyxS0rkjpLEo16uXTS0uVCS4815lc40K2iVpWDvdSY=" crossorigin="anonymous">
</script>
<script>
    $(document).ready( function () {
    $('#datatable1').DataTable({bPaginate:false});
    $('.outlay_date').datepicker({showButtonPanel: true,minDate: -30, maxDate: "+1M +10D",dateFormat:"yy-mm-dd" });
    $( "#anim" ).on( "change", function() {
      $( "#datepicker" ).datepicker( "option", "showAnim", $( this ).val() );
    });

    toastr.options.timeOut = 2000;
            @if (Session::has('error'))
                toastr.error('{{ Session::get('error') }}');
            @elseif(Session::has('success'))
                toastr.success('{{ Session::get('success') }}');
            @endif
} );
</script>
@endsection
