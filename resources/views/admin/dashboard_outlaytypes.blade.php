@extends('layouts.master')
@section('css')
<link rel="stylesheet" type="text/css" href="https://code.jquery.com/ui/1.13.2/themes/base/jquery-ui.css" />
@section('title')
OutlayTypes
@stop
@endsection
@section('page-header')
<!-- breadcrumb -->
<div class="page-title" style="margin-top: 4rem;">
    <div class="row">

        <div class="col-sm-6">
            <h4 class="mb-3">Outlay Types Table</h4>
        </div>
    </div>
</div>
<!-- breadcrumb -->
@endsection
@section('content')
<div class="row">
    <div class="col-md-12 mb-30">
        <div class="card card-statistics h-100">
            <div class="card-body">

                <div class="col-xl-12 mb-30 w-100" style="padding:0">
                    <div class=" h-100">
                        <div class="">
                            @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                            @endif
                            <div class="container bootstrap snippet ">
                                <div class="row" style="flex-direction: col-reverse;">
                                    <div class="col-sm-12" style="padding:0">
                                        <div class="tab-pane fade show" id="outlaytypes">
                                            <div name="exclude" id="exclude">
                                                {{--
                                                <hr> --}}
                                                <button type="button"
                                                    class="btn btn-primary btn-sm ml-auto d-block py-2 px-3 rounded my-3"
                                                    data-toggle="modal" data-target="#add_outlaytype"
                                                    title="Add new outlaytype   ">Add <i class="fa fa-add"></i></button>
                                                <div class="table-responsive">
                                                    <table id="datatable3"
                                                        class="table  table-hover table-md table-borded  p-2"
                                                        data-page-length="50" style="text-align: center">
                                                        <thead>
                                                            <tr>
                                                                <th>#</th>
                                                                <th>Name</th>
                                                                <th>Description</th>
                                                                <th>Actions</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <?php $i = 0; ?>
                                                            @foreach ($outlaytypes as $outlaytype)
                                                            <?php $i++; ?>
                                                            <tr>
                                                                <th>{{ $i }}</th>
                                                                <th>{{ $outlaytype->name }}</th>
                                                                <th>{{ $outlaytype->description }}</th>

                                                                <td>
                                                                    <button type="button"
                                                                        class="btn btn-info btn-sm d-inline"
                                                                        data-toggle="modal"
                                                                        data-target="#edit_outlaytype{{ $outlaytype->id }}"
                                                                        title="Edit"><i class="fa fa-edit"></i></button>
                                                                    <button type="button"
                                                                        class="btn btn-danger btn-sm d-inline"
                                                                        data-toggle="modal"
                                                                        data-target="#delete_modal{{ $outlaytype->id }}"
                                                                        title="Delete outlaytype"><i
                                                                            class="fa fa-trash"></i></button>
                                                                </td>

                                                            </tr>

                                                            <x-edit-modal :id="$outlaytype->id" title="outlaytype"
                                                                action="outlaytype">
                                                                <div class="mb-3 mt-2"
                                                                    style="display:flex;flex-direction:column;gap:1.5rem;padding:1rem;padding-top:0;padding-bottom:0">
                                                                    <div>
                                                                        <label for="name" class="mr-sm-2">Name
                                                                            :</label>
                                                                        <input id="name" value="{{ $outlaytype->name }}"
                                                                            type="text" name="name"
                                                                            class="form-control" />
                                                                    </div>
                                                                    <div>
                                                                        <label for="description"
                                                                            class="mr-sm-2">Description
                                                                            :</label>
                                                                        <input type="text"
                                                                            value="{{ $outlaytype->description }}"
                                                                            class="form-control" name="description" />
                                                                    </div>
                                                                </div>
                                                            </x-edit-modal>


                                                            <x-delete-modal :id="$outlaytype->id"
                                                                :title="$outlaytype->name"
                                                                action="outlaytype.destroy" />

                                                            @endforeach
                                                        </tbody>


                                                    </table>
                                                </div>
                                            </div>
                                        </div>

                                        <x-add-modal title="outlaytype" action='outlaytype.store'>
                                            <div class="mb-3 mt-2"
                                                style="display:flex;flex-direction:column;gap:1.5rem;padding:1rem;padding-top:0;padding-bottom:0">
                                                <div>
                                                    <label for="name" class="mr-sm-2">Name
                                                        :</label>
                                                    <input id="name" type="text" name="name" class="form-control" />
                                                </div>
                                                <div>
                                                    <label for="description" class="mr-sm-2">Description
                                                        :</label>
                                                    <input type="text" class="form-control" name="description" />
                                                </div>

                                            </div>


                                        </x-add-modal>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('js')
<script src="https://code.jquery.com/ui/1.13.1/jquery-ui.min.js"
    integrity="sha256-eTyxS0rkjpLEo16uXTS0uVCS4815lc40K2iVpWDvdSY=" crossorigin="anonymous">
</script>
<script>
    $(document).ready( function () {
    $('#datatable3').DataTable({bPaginate:false});
    $('.outlay_date').datepicker({showButtonPanel: true,minDate: -30, maxDate: "+1M +10D",dateFormat:"yy-mm-dd" });
    $( "#anim" ).on( "change", function() {
      $( "#datepicker" ).datepicker( "option", "showAnim", $( this ).val() );
    });

    toastr.options.timeOut = 2000;
            @if (Session::has('error'))
                toastr.error('{{ Session::get('error') }}');
            @elseif(Session::has('success'))
                toastr.success('{{ Session::get('success') }}');
            @endif
} );
</script>
@endsection
