@extends('layouts.master')
@section('css')
<link rel="stylesheet" type="text/css" href="https://code.jquery.com/ui/1.13.2/themes/base/jquery-ui.css" />
<style>
    .accordion1-button {
        position: relative;
        display: flex;
        align-items: center;
        width: 100%;
        padding: 1rem 1.25rem;
        font-size: 1rem;
        color: #212529;
        text-align: left;
        background-color: #fff;
        border: 0;
        border-radius: 0;
        overflow-anchor: none;
        transition: color .15s ease-in-out, background-color .15s ease-in-out, border-color .15s ease-in-out, box-shadow .15s ease-in-out, border-radius .15s ease
    }

    @media (prefers-reduced-motion:reduce) {
        .accordion1-button {
            transition: none
        }
    }

    .accordion1-button:not(.collapsed) {
        color: #0c63e4;
        background-color: #e7f1ff;
        box-shadow: inset 0 -1px 0 rgba(0, 0, 0, .125)
    }

    .accordion1-button:not(.collapsed)::after {
        background-image: url("data:image/svg+xml,%3csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 16 16' fill='%230c63e4'%3e%3cpath fill-rule='evenodd' d='M1.646 4.646a.5.5 0 0 1 .708 0L8 10.293l5.646-5.647a.5.5 0 0 1 .708.708l-6 6a.5.5 0 0 1-.708 0l-6-6a.5.5 0 0 1 0-.708z'/%3e%3c/svg%3e");
        transform: rotate(-180deg)
    }

    .accordion1-button::after {
        flex-shrink: 0;
        width: 1.25rem;
        height: 1.25rem;
        margin-left: auto;
        content: "";
        background-image: url("data:image/svg+xml,%3csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 16 16' fill='%23212529'%3e%3cpath fill-rule='evenodd' d='M1.646 4.646a.5.5 0 0 1 .708 0L8 10.293l5.646-5.647a.5.5 0 0 1 .708.708l-6 6a.5.5 0 0 1-.708 0l-6-6a.5.5 0 0 1 0-.708z'/%3e%3c/svg%3e");
        background-repeat: no-repeat;
        background-size: 1.25rem;
        transition: transform .2s ease-in-out
    }

    @media (prefers-reduced-motion:reduce) {
        .accordion1-button::after {
            transition: none
        }
    }

    .accordion1-button:hover {
        z-index: 2
    }

    .accordion1-button:focus {
        z-index: 3;
        border-color: #86b7fe;
        outline: 0;
        box-shadow: 0 0 0 .25rem rgba(13, 110, 253, .25)
    }

    .accordion1-header {
        margin-bottom: 0
    }

    .accordion1-item {
        background-color: #fff;
        border: 1px solid rgba(0, 0, 0, .125)
    }

    .accordion1-item:first-of-type {
        border-top-left-radius: .25rem;
        border-top-right-radius: .25rem
    }

    .accordion1-item:first-of-type .accordion1-button {
        border-top-left-radius: calc(.25rem - 1px);
        border-top-right-radius: calc(.25rem - 1px)
    }

    .accordion1-item:not(:first-of-type) {
        border-top: 0
    }

    .accordion1-item:last-of-type {
        border-bottom-right-radius: .25rem;
        border-bottom-left-radius: .25rem
    }

    .accordion1-item:last-of-type .accordion1-button.collapsed {
        border-bottom-right-radius: calc(.25rem - 1px);
        border-bottom-left-radius: calc(.25rem - 1px)
    }

    .accordion1-item:last-of-type .accordion1-collapse {
        border-bottom-right-radius: .25rem;
        border-bottom-left-radius: .25rem
    }

    .accordion1-body {
        padding: 1rem 1.25rem
    }

    .accordion1-flush .accordion1-collapse {
        border-width: 0
    }

    .accordion1-flush .accordion1-item {
        border-right: 0;
        border-left: 0;
        border-radius: 0
    }

    .accordion1-flush .accordion1-item:first-child {
        border-top: 0
    }

    .accordion1-flush .accordion1-item:last-child {
        border-bottom: 0
    }

    .accordion1-flush .accordion1-item .accordion1-button {
        border-radius: 0
    }
</style>
@section('title')
Outlayes
@stop
@endsection
@section('page-header')
<!-- breadcrumb -->
<div class="page-title" style="margin-top: 4rem;">
    <div class="row">

        <div class="col-sm-6">
            <h4 class="mb-3">Outlays Table</h4>
        </div>
    </div>
</div>
<!-- breadcrumb -->
@endsection
@section('content')
<div class="row">
    <div class="col-md-12 mb-30">
        <div class="card card-statistics h-100">
            <div class="card-body">

                <div class="col-xl-12 mb-30 w-100" style="padding:0">
                    <div class=" h-100">
                        <div class="">
                            @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                            @endif
                            <?php
                            if(Session::has('outlays')){
                                $outlays = Session::get('outlays');
                            }
                            ?>
                            <div class="container bootstrap snippet ">
                                <div class="row" style="flex-direction: col-reverse;">
                                    <div class="col-sm-12" style="padding:0">
                                        <div class="tab-pane fade show" id="outlays">
                                            <div name="exclude" id="exclude" style="display: flex;
                                            flex-direction: column;">

                                                <div class="btn-group" role="group"
                                                    style="float: right;width:100%;height: 5.4rem"
                                                    aria-label="Button group with nested dropdown">
                                                    <button id="btnGroupDrop1" type="button"
                                                        style="border-radius: 5px 0 0 5px;"
                                                        class="btn btn-primary btn-md ml-auto d-block py-2 px-3  my-3"
                                                        data-toggle="modal" data-target="#add_outlay"
                                                        title="Add new outlay">Add <i class="fa fa-add"></i></button>
                                                    <select {{--
                                                        class="filter_user form-select-lg mb-3 form-control h-100" --}}
                                                        class=" btn-md d-block py-2 px-3 my-3"
                                                        style="width:20%;border-radius: 0 5px 5px 0;" id="filter_input"
                                                        aria-label="Default select example" name="filter">

                                                        <option value="no_filter" @if(!Session::has('filter')) selected
                                                            @endif id="no_filter_button">
                                                            No filter</option>
                                                        <option value="filter_month" id="filter_month_button"
                                                            @if(Session::has('filter') &&
                                                            Session::get('filter')=="filter_month_button" ) selected
                                                            @endif>
                                                            Filter
                                                            on
                                                            Month</option>
                                                        <option value="filter_year" id="filter_year_button"
                                                            @if(Session::has('filter') &&
                                                            Session::get('filter')=="filter_year_button" ) selected
                                                            @endif>
                                                            Filter on
                                                            Year</option>

                                                        <option value="filter_user" id="filter_user_button"
                                                            @if(Session::has('filter') &&
                                                            Session::get('filter')=="filter_user_button" ) selected
                                                            @endif>

                                                            Filter on
                                                            User
                                                        </option>
                                                        <option value="filter_material" id="filter_material_button"
                                                            @if(Session::has('filter') &&
                                                            Session::get('filter')=="filter_material_button" ) selected
                                                            @endif>
                                                            Filter on
                                                            Material</option>
                                                        <option value="filter_service" id="filter_service_button"
                                                            @if(Session::has('filter') &&
                                                            Session::get('filter')=="filter_service_button" ) selected
                                                            @endif>
                                                            Filter on
                                                            Service</option>
                                                    </select>

                                                </div>

                                                {{-- --}}
                                                <div class="filter">
                                                    <div class="accordion" id="accordion1Example">
                                                        <div class="accordion1-item">
                                                            <h2 class="accordion1-header " id="headingOne">
                                                                <button class="accordion1-button collapsed"
                                                                    type="button" data-bs-toggle="collapse"
                                                                    data-bs-target="#collapseOne" aria-expanded="true"
                                                                    aria-controls="collapseOne">
                                                                    Filters
                                                                </button>
                                                            </h2>
                                                            <div id="collapseOne" class="accordion1-collapse collapse"
                                                                aria-labelledby="headingOne"
                                                                data-bs-parent="#accordion1Example">
                                                                <div class="accordion1-body">
                                                                    <div id="filter" style="">
                                                                        <div id="filter_user">
                                                                            <label for="service" class="mr-sm-2">Filter
                                                                                On User
                                                                                :</label>
                                                                            <form id="filter_user_form"
                                                                                action="{{ route('filter_user') }}"
                                                                                method="GET"
                                                                                enctype="multipart/form-data"
                                                                                style="display:flex;flex-direction:row;gap:20px">
                                                                                <input type="submit"
                                                                                    class="btn btn-success filter_submit"
                                                                                    style="height: 100%;margin:auto 0;display:none"
                                                                                    value="Search">

                                                                            </form>
                                                                            @if (Session::has('user_info'))
                                                                            <?php $user_info = json_decode(Session::get('user_info'));?>
                                                                            <div class="mt-4">
                                                                                <label>Details:</label>

                                                                                <div class="table-responsive">
                                                                                    <table class="table table-striped
                                                                                    table-hover
                                                                                    table-borderless
                                                                                    table-primary
                                                                                    align-middle">
                                                                                        <thead class="table-light">

                                                                                            <tr>
                                                                                                <th>Name</th>
                                                                                                <th>Total</th>

                                                                                            </tr>
                                                                                        </thead>
                                                                                        <tbody
                                                                                            class="table-group-divider">
                                                                                            @foreach ($user_info as $key
                                                                                            =>
                                                                                            $user_in)
                                                                                            <tr class="table-primary">

                                                                                                <td scope="row"
                                                                                                    style="background-color: #fff !important">
                                                                                                    {{
                                                                                                    $user_in->name }}
                                                                                                </td>
                                                                                                <td
                                                                                                    style="background-color: #fff !important">
                                                                                                    {{ $user_in->total
                                                                                                    }}</td>
                                                                                            </tr>
                                                                                            @endforeach
                                                                                        </tbody>
                                                                                        <tfoot>

                                                                                        </tfoot>
                                                                                    </table>
                                                                                </div>

                                                                                {{-- <div class="table-responsive">
                                                                                    <table class="table table-primary">
                                                                                        <thead>
                                                                                            <tr>
                                                                                                <th scope="col">name
                                                                                                </th>
                                                                                                <th scope="col">total
                                                                                                </th>

                                                                                            </tr>
                                                                                        </thead>
                                                                                        <tbody>
                                                                                            @foreach ($user_info as $key
                                                                                            =>
                                                                                            $user_in)
                                                                                            <tr class="">

                                                                                                <td>{{ $user_in->name }}
                                                                                                </td>
                                                                                                <td>{{ $user_in->total
                                                                                                    }}</td>
                                                                                            </tr>
                                                                                            @endforeach
                                                                                        </tbody>
                                                                                    </table>
                                                                                </div> --}}

                                                                                {{-- <span class="d-block fw-bold"
                                                                                    style="margin: auto 0;">name: {{
                                                                                    $user_in->name
                                                                                    }}</span>

                                                                                <span class="d-block fw-bold"
                                                                                    style="margin: auto 0;">Total: {{
                                                                                    $user_in->total
                                                                                    }}</span> --}}


                                                                            </div>

                                                                            @endif
                                                                        </div>
                                                                        {{-- <div>
                                                                            <label for="User" class="mr-sm-2">User
                                                                                :</label>
                                                                            <select
                                                                                class="filter_user form-select-lg mb-3 form-control h-100"
                                                                                id="filter_user_input"
                                                                                aria-label="Default select example"
                                                                                name="user_id">
                                                                                @foreach ($users as $key =>
                                                                                $user) {
                                                                                <option value="{{ $user->id }}">{{
                                                                                    $user->name
                                                                                    }}</option>
                                                                                }
                                                                                @endforeach
                                                                            </select>
                                                                        </div> --}}

                                                                        {{-- <div>
                                                                            <label for="material"
                                                                                class="mr-sm-2">Material
                                                                                :</label>
                                                                            <select
                                                                                class="filter_material form-select-lg mb-3 form-control h-100"
                                                                                id="filter_service_input"
                                                                                aria-label="Default select example"
                                                                                name="material_id">
                                                                                @foreach ($materials as $key =>
                                                                                $material) {
                                                                                <option value="{{ $material->id }}">
                                                                                    {{
                                                                                    $material->name
                                                                                    }}</option>
                                                                                }
                                                                                @endforeach
                                                                            </select>
                                                                        </div> --}}
                                                                        <div id="filter_year">
                                                                            <label for="service" class="mr-sm-2">Filter
                                                                                On Year
                                                                                :</label>
                                                                            <form action="{{ route('filter_year') }}"
                                                                                method="GET"
                                                                                enctype="multipart/form-data"
                                                                                style="display:flex;flex-direction:row;gap:20px">
                                                                                <select style="width:30%"
                                                                                    class="filter_service form-select-lg form-control h-100"
                                                                                    id="filter_service_input"
                                                                                    aria-label="Default select example"
                                                                                    name="year">
                                                                                    @foreach ($years as $key =>
                                                                                    $year) {
                                                                                    <option value="{{ $year }}" {{
                                                                                        Session::has('year')&&(Session::get('year')==$year)?'selected':''
                                                                                        }}>
                                                                                        {{
                                                                                        $year
                                                                                        }}</option>
                                                                                    }
                                                                                    @endforeach
                                                                                </select>
                                                                                <input type="submit"
                                                                                    class="btn btn-success filter_submit"
                                                                                    style="height: 100%;margin:auto 0"
                                                                                    display:none value="Search">

                                                                            </form>
                                                                            @if (Session::has('total'))
                                                                            <div class="mt-4">
                                                                                <label>Details:</label>

                                                                                <span class="d-block fw-bold"
                                                                                    style="margin: auto 0;">Total: {{
                                                                                    Session::get('total')
                                                                                    }}</span>

                                                                            </div>

                                                                            @endif
                                                                        </div>

                                                                        {{-- filter_month --}}
                                                                        <div id="filter_month">
                                                                            <label for="service" class="mr-sm-2">Filter
                                                                                On Month
                                                                                :</label>
                                                                            <form action="{{ route('filter_month') }}"
                                                                                method="GET"
                                                                                enctype="multipart/form-data"
                                                                                style="display:flex;flex-direction:row;gap:20px">
                                                                                <select style="width:30%"
                                                                                    class="filter_service form-select-lg form-control h-100"
                                                                                    id="filter_service_input"
                                                                                    aria-label="Default select example"
                                                                                    name="date">
                                                                                    @foreach ($months as $key =>
                                                                                    $month) {
                                                                                    <option value="{{ $month }}" {{
                                                                                        Session::has('month')&&(Session::get('month')==$month)?'selected':''
                                                                                        }}>
                                                                                        {{
                                                                                        $month
                                                                                        }}</option>
                                                                                    }
                                                                                    @endforeach
                                                                                </select>
                                                                                <input type="submit"
                                                                                    class="btn btn-success filter_submit"
                                                                                    style="height: 100%;margin:auto 0"
                                                                                    display:none value="Search">

                                                                            </form>
                                                                            @if (Session::has('total'))
                                                                            <div class="mt-4">
                                                                                <label>Details:</label>

                                                                                <span class="d-block fw-bold"
                                                                                    style="margin: auto 0;">Total: {{
                                                                                    Session::get('total')
                                                                                    }}</span>

                                                                            </div>

                                                                            @endif
                                                                        </div>

                                                                        {{-- filter_material --}}
                                                                        <div id="filter_material">
                                                                            <label for="service" class="mr-sm-2">Filter
                                                                                On Material
                                                                                :</label>
                                                                            <form id="filter_material_form"
                                                                                action="{{ route('filter_material') }}"
                                                                                method="GET"
                                                                                enctype="multipart/form-data"
                                                                                style="display:flex;flex-direction:row;gap:20px">
                                                                                <input type="submit"
                                                                                    class="btn btn-success filter_submit"
                                                                                    style="height: 100%;margin:auto 0;display:none"
                                                                                    display:none value="Search">

                                                                            </form>
                                                                            @if (Session::has('material_info'))
                                                                            <?php $material_info = json_decode(Session::get('material_info'));?>
                                                                            <div class="mt-4">
                                                                                <label>Details:</label>
                                                                                <div class="table-responsive">
                                                                                    <table class="table table-striped
                                                                                    table-hover
                                                                                    table-borderless
                                                                                    table-primary
                                                                                    align-middle">
                                                                                        <thead class="table-light">

                                                                                            <tr>
                                                                                                <th>Name</th>
                                                                                                <th>Total</th>

                                                                                            </tr>
                                                                                        </thead>
                                                                                        <tbody
                                                                                            class="table-group-divider">
                                                                                            @foreach ($material_info
                                                                                            as $key
                                                                                            =>
                                                                                            $material_in)
                                                                                            <tr class="table-primary">

                                                                                                <td scope="row"
                                                                                                    style="background-color: #fff !important">
                                                                                                    {{
                                                                                                    $material_in->name
                                                                                                    }}
                                                                                                </td>
                                                                                                <td
                                                                                                    style="background-color: #fff !important">
                                                                                                    {{
                                                                                                    $material_in->total
                                                                                                    }}</td>
                                                                                            </tr>
                                                                                            @endforeach
                                                                                        </tbody>
                                                                                        <tfoot>

                                                                                        </tfoot>
                                                                                    </table>
                                                                                </div>

                                                                            </div>

                                                                            @endif
                                                                        </div>
                                                                        {{-- Filter on Services --}}
                                                                        <div id="filter_service">
                                                                            <label for="service" class="mr-sm-2">Filter
                                                                                On Service
                                                                                :</label>
                                                                            <form id="filter_service_form"
                                                                                action="{{ route('filter_service') }}"
                                                                                method="GET"
                                                                                enctype="multipart/form-data"
                                                                                style="display:flex;flex-direction:row;gap:20px">
                                                                                <input type="submit"
                                                                                    class="btn btn-success filter_submit"
                                                                                    style="height: 100%;margin:auto 0; display:none"
                                                                                    display:none value="Search">

                                                                            </form>
                                                                            @if (Session::has('service_info'))
                                                                            <?php $service_info = json_decode(Session::get('service_info'));?>
                                                                            <div class="mt-4">
                                                                                <label>Details:</label>
                                                                                <div class="table-responsive">
                                                                                    <table class="table table-striped
                                                                                    table-hover
                                                                                    table-borderless
                                                                                    table-primary
                                                                                    align-middle">
                                                                                        <thead class="table-light">

                                                                                            <tr>
                                                                                                <th>Sum</th>
                                                                                                <th>Total</th>

                                                                                            </tr>
                                                                                        </thead>
                                                                                        <tbody
                                                                                            class="table-group-divider">

                                                                                            <tr class="table-primary">

                                                                                                <td scope="row"
                                                                                                    style="background-color: #fff !important">
                                                                                                    {{
                                                                                                    $service_info->sum
                                                                                                    }}
                                                                                                </td>
                                                                                                <td
                                                                                                    style="background-color: #fff !important">
                                                                                                    {{
                                                                                                    $service_info->total
                                                                                                    }}</td>
                                                                                            </tr>

                                                                                        </tbody>
                                                                                        <tfoot>

                                                                                        </tfoot>
                                                                                    </table>
                                                                                </div>
                                                                                {{-- <span class="d-block fw-bold"
                                                                                    style="margin: auto 0;">sum: {{
                                                                                    $service_info->sum
                                                                                    }}</span>

                                                                                <span class="d-block fw-bold"
                                                                                    style="margin: auto 0;">Total: {{
                                                                                    $service_info->total
                                                                                    }}</span> --}}


                                                                            </div>

                                                                            @endif
                                                                        </div>
                                                                        {{-- <div id="filter_year">
                                                                            <label for="service" class="mr-sm-2">Filter
                                                                                On Material
                                                                                :</label>
                                                                            <form
                                                                                action="{{ route('filter_material') }}"
                                                                                method="GET"
                                                                                enctype="multipart/form-data"
                                                                                style="display:flex;flex-direction:row;gap:20px">
                                                                                <input type="submit"
                                                                                    class="btn btn-success filter_submit"
                                                                                    style="height: 100%;margin:auto 0"
                                                                                    display:none value="Search">
                                                                            </form>
                                                                            @if (Session::has('total'))
                                                                            <div class="mt-4">
                                                                                <label>Details:</label>

                                                                                <span class="d-block fw-bold"
                                                                                    style="margin: auto 0;">Total: {{
                                                                                    Session::get('total')
                                                                                    }}</span>

                                                                            </div>

                                                                            @endif
                                                                        </div> --}}

                                                                    </div>
                                                                </div>
                                                            </div>

                                                        </div>


                                                        {{-- <div id="filter_month_input" class="d-none">
                                                            <label>Filters</label>
                                                            <form class="mb-3 w-50">
                                                                <label class="mr-sm-2">Month
                                                                    :</label>
                                                                <input type="text" class="form-control outlay_date"
                                                                    name="date" value="{{ old('date') }}" />
                                                            </form>
                                                        </div>
                                                        <div id="filter_month_input" class="d-none">
                                                            <label>Filters</label>
                                                            <form class="mb-3 w-50">
                                                                <label class="mr-sm-2">Month
                                                                    :</label>
                                                                <input type="text" class="form-control outlay_date"
                                                                    name="date" value="{{ old('date') }}" />
                                                            </form>
                                                        </div> --}}

                                                    </div>
                                                </div>
                                            </div>
                                            <div class="table-responsive">
                                                <table id="datatable4"
                                                    class="table  table-hover table-md table-borded  p-2"
                                                    data-page-length="50" style="text-align: center">
                                                    <thead>
                                                        <tr>
                                                            <th>Id</th>
                                                            <th>User</th>
                                                            <th>Outlay Type</th>
                                                            <th>Material</th>
                                                            <th>Price</th>
                                                            <th>Description</th>
                                                            <th>Date</th>
                                                            <th>Actions</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php $i = 0;?>
                                                        @if(count($outlays)>0)
                                                        @foreach ($outlays as $outlay)
                                                        <?php $i++; ?>
                                                        <tr>
                                                            <th>{{ $outlay->id }}</th>
                                                            <th>{{ $outlay->user->name }}</th>
                                                            <th>{{ $outlay->outlaytype->name }}</th>
                                                            <th>{{ $outlay->material->name }}</th>
                                                            <th>{{ $outlay->price }}</th>
                                                            <th>{{ $outlay->description }}</th>
                                                            <th>{{ $outlay->date }}</th>
                                                            <td>
                                                                <button type="button"
                                                                    class="btn btn-info btn-sm d-inline"
                                                                    data-toggle="modal"
                                                                    data-target="#edit_outlay{{ $outlay->id }}"
                                                                    title="Edit"><i class="fa fa-edit"></i></button>
                                                                <button type="button"
                                                                    class="btn btn-danger btn-sm d-inline"
                                                                    data-toggle="modal"
                                                                    data-target="#delete_modal{{ $outlay->id }}"
                                                                    title="Delete outlay"><i
                                                                        class="fa fa-trash"></i></button>
                                                            </td>

                                                        </tr>

                                                        <x-edit-modal :id="$outlay->id" title="outlay" action="outlay">
                                                            <div class="mb-3 mt-2"
                                                                style="display:flex;flex-direction:column;gap:1.5rem;padding:1rem;padding-top:0;padding-bottom:0">
                                                                <div>
                                                                    <label for="User" class="mr-sm-2">User
                                                                        :</label>
                                                                    <select
                                                                        class="privileges form-select-lg mb-3 form-control h-100"
                                                                        id="users" aria-label="Default select example"
                                                                        name="user_id">
                                                                        @foreach ($users as $key =>
                                                                        $user) {
                                                                        <option value="{{ $user->id }}" {{$outlay ->
                                                                            user_id == $user->id
                                                                            ?'selected':''}}>{{
                                                                            $user->name
                                                                            }}</option>
                                                                        }
                                                                        @endforeach
                                                                    </select>

                                                                </div>

                                                                <div>
                                                                    <label for="Material" class="mr-sm-2">Material
                                                                        :</label>
                                                                    <select
                                                                        class="privileges form-select-lg mb-3 form-control h-100"
                                                                        id="materials"
                                                                        aria-label="Default select example"
                                                                        name="material_id">
                                                                        @foreach ($materials as $key =>
                                                                        $material) {
                                                                        <option value="{{ $material->id }}" {{$outlay->
                                                                            material_id==$material->id
                                                                            ?'selected':''}}>{{
                                                                            $material->name
                                                                            }}</option>
                                                                        }
                                                                        @endforeach
                                                                    </select>

                                                                </div>
                                                                <div>
                                                                    <label for="OutlayType" class="mr-sm-2">Outlay
                                                                        Type
                                                                        :</label>
                                                                    <select
                                                                        class="privileges form-select-lg mb-3 form-control h-100"
                                                                        id="OutlayType"
                                                                        aria-label="Default select example"
                                                                        name="outlaytype_id">
                                                                        @foreach ($outlaytypes as $key =>
                                                                        $outlaytype) {
                                                                        <option value="{{ $outlaytype->id }}"
                                                                            {{$outlay->
                                                                            outlaytype_id==$outlaytype->id
                                                                            ?'selected':''}}>{{
                                                                            $outlaytype->name
                                                                            }}</option>
                                                                        }
                                                                        @endforeach
                                                                    </select>

                                                                </div>
                                                                <div>
                                                                    <label class="mr-sm-2">Date
                                                                        :</label>
                                                                    <input type="text" class="form-control outlay_date"
                                                                        name="date" value="{{ $outlay->date }}" />
                                                                </div>
                                                                <div>
                                                                    <label for="description" class="mr-sm-2">Description
                                                                        :</label>
                                                                    <input type="text"
                                                                        value="{{ $outlay->description }}"
                                                                        class="form-control" name="description" />
                                                                </div>
                                                                <div>
                                                                    <label for="price" class="mr-sm-2">Price
                                                                        :</label>
                                                                    <input type="text" id="price"
                                                                        value="{{ $outlay->price }}"
                                                                        class="form-control" name="price" />
                                                                </div>


                                                            </div>
                                                        </x-edit-modal>


                                                        <x-delete-modal :id="$outlay->id" :title="$outlay->name"
                                                            action="outlay.destroy" />

                                                        @endforeach
                                                        @endif
                                                    </tbody>


                                                </table>
                                                <div class="d-flex">

                                                    {!!$outlays->links() !!}

                                                </div>
                                            </div>
                                        </div>
                                        {{-- @if (count($outlays)>0) --}}
                                        <x-add-modal title="outlay" action='outlay.store'>
                                            <div class="mb-3 mt-2"
                                                style="display:flex;flex-direction:column;gap:1.5rem;padding:1rem;padding-top:0;padding-bottom:0">
                                                <div>
                                                    <label for="User" class="mr-sm-2">User
                                                        :</label>
                                                    <select class="privileges form-select-lg mb-3 form-control h-100"
                                                        id="users" aria-label="Default select example" name="user_id">
                                                        <option {{ old('user_id')=="" ?'selected':"" }} disabled
                                                            value="">--choose
                                                            a specially --</option>
                                                        @foreach ($users as $key =>
                                                        $user) {
                                                        <option value="{{ $user->id }}" {{old($outlay->
                                                            user_id)==$user->id
                                                            ?'selected':''}}>{{
                                                            $user->name
                                                            }}</option>
                                                        }
                                                        @endforeach
                                                    </select>

                                                </div>

                                                <div>
                                                    <label for="Material" class="mr-sm-2">Material
                                                        :</label>
                                                    <select class="privileges form-select-lg mb-3 form-control h-100"
                                                        id="materials" aria-label="Default select example"
                                                        name="material_id">
                                                        <option {{ old('material_id')=="" ?'selected':"" }} disabled
                                                            value="">--choose
                                                            a specially --</option>
                                                        @foreach ($materials as $key =>
                                                        $material) {
                                                        <option value="{{ $material->id }}" {{old('$outlay->
                                                            material_id')==$material->id
                                                            ?'selected':''}}>{{
                                                            $material->name
                                                            }}</option>
                                                        }
                                                        @endforeach
                                                    </select>

                                                </div>
                                                <div>
                                                    <label for="OutlayType" class="mr-sm-2">Outlay Type
                                                        :</label>
                                                    <select class="privileges form-select-lg mb-3 form-control h-100"
                                                        id="OutlayType" aria-label="Default select example"
                                                        name="outlaytype_id">
                                                        <option {{ old('outlaytype_id')=="" ?'selected':"" }} disabled
                                                            value="">--choose
                                                            a specially --</option>
                                                        @foreach ($outlaytypes as $key =>
                                                        $outlaytype) {
                                                        <option value="{{ $outlaytype->id }}" {{old('$outlay->
                                                            outlaytype_id')==$outlaytype->id
                                                            ?'selected':''}}>{{
                                                            $outlaytype->name
                                                            }}</option>
                                                        }
                                                        @endforeach
                                                    </select>

                                                </div>
                                                <div>
                                                    <label class="mr-sm-2">Date
                                                        :</label>
                                                    <input type="text" id="outlay_date.{{ $outlay->id }}"
                                                        class="form-control outlay_date" name="date"
                                                        value="{{old('$outlay->date')}}" />
                                                </div>
                                                <div>
                                                    <label for="description" class="mr-sm-2">Description
                                                        :</label>
                                                    <input type="text" value="{{old('$outlay->description') }}"
                                                        class="form-control" name="description" />
                                                </div>
                                                <div>
                                                    <label for="price" class="mr-sm-2">Price
                                                        :</label>
                                                    <input type="text" id="price" value="{{ old('$outlay->price') }}"
                                                        class="form-control" name="price" />
                                                </div>

                                            </div>


                                        </x-add-modal>
                                        {{-- @endif --}}

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('js')
<script src="https://code.jquery.com/ui/1.13.1/jquery-ui.min.js"
    integrity="sha256-eTyxS0rkjpLEo16uXTS0uVCS4815lc40K2iVpWDvdSY=" crossorigin="anonymous">
</script>
<script>
    $(document).ready( function () {
    $('#datatable4').DataTable({bPaginate: false});
    $('.filter_submit').on('click',function(){

    })
    $('.outlay_date').each(function(index,datepicker){
        $(datepicker).datepicker({showButtonPanel: true,minDate: -30, maxDate: "+1M +10D",dateFormat:"yy-mm-dd"});
    });
    $('#filter_year').hide();
        $('#filter_month').hide();
        $('#filter_user').hide();
        $('#filter_service').hide();
        $('#filter_material').hide();
    $('#filter_input').on('change',function(){
        var input = $(this).find(":selected").val()
        if(input=="no_filter"){
        $('#filter_year').hide();
        $('#filter_month').hide();
        $('#filter_user').hide();
        $('#filter_service').hide();
        $('#filter_material').hide();
        window.location.reload();
        }else if(input == 'filter_user'){
            $("#filter_user_form").submit();
        }else if(input == 'filter_material'){
            $("#filter_material_form").submit();
        }else if(input == 'filter_service'){
            $("#filter_service_form").submit();
        }
        $('#filter_year').hide();
        $('#filter_month').hide();
        $('#filter_user').hide();
        $('#filter_service').hide();
        $('#filter_material').hide();
        $('#'+input).show();
        $('#collapseOne').collapse('show')
        // console.log($(this).find(":selected").val());
    })

    @if (Session::has('filter'))
        filterSection = "<?php echo Session::get('filter');?>"
        filterSection =filterSection.replace('_button','');
        $('#filter_year').hide();
        $('#filter_month').hide();
        $('#filter_user').hide();
        $('#filter_service').hide();
        $('#filter_material').hide();
        $('#'+filterSection).show();
                $('#collapseOne').collapse('show');
            @endif
    // .each(function(index,datepicker){
    //     $(datepicker).datepicker({showButtonPanel: true,minDate: -30, maxDate: "+1M +10D",dateFormat:"yy-mm-dd"});
    // });

toastr.options.timeOut = 2000;
            @if (Session::has('error'))
                toastr.error('{{ Session::get('error') }}');
            @elseif(Session::has('success'))
                toastr.success('{{ Session::get('success') }}');
            @endif
} );
</script>
@endsection
