@extends('layouts.master')
@section('css')
<link rel="stylesheet" type="text/css" href="https://code.jquery.com/ui/1.13.2/themes/base/jquery-ui.css" />
<style>
    .accordion1-button {
        position: relative;
        display: flex;
        align-items: center;
        width: 100%;
        padding: 1rem 1.25rem;
        font-size: 1rem;
        color: #212529;
        text-align: left;
        background-color: #fff;
        border: 0;
        border-radius: 0;
        overflow-anchor: none;
        transition: color .15s ease-in-out, background-color .15s ease-in-out, border-color .15s ease-in-out, box-shadow .15s ease-in-out, border-radius .15s ease
    }

    @media (prefers-reduced-motion:reduce) {
        .accordion1-button {
            transition: none
        }
    }

    .accordion1-button:not(.collapsed) {
        color: #0c63e4;
        background-color: #e7f1ff;
        box-shadow: inset 0 -1px 0 rgba(0, 0, 0, .125)
    }

    .accordion1-button:not(.collapsed)::after {
        background-image: url("data:image/svg+xml,%3csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 16 16' fill='%230c63e4'%3e%3cpath fill-rule='evenodd' d='M1.646 4.646a.5.5 0 0 1 .708 0L8 10.293l5.646-5.647a.5.5 0 0 1 .708.708l-6 6a.5.5 0 0 1-.708 0l-6-6a.5.5 0 0 1 0-.708z'/%3e%3c/svg%3e");
        transform: rotate(-180deg)
    }

    .accordion1-button::after {
        flex-shrink: 0;
        width: 1.25rem;
        height: 1.25rem;
        margin-left: auto;
        content: "";
        background-image: url("data:image/svg+xml,%3csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 16 16' fill='%23212529'%3e%3cpath fill-rule='evenodd' d='M1.646 4.646a.5.5 0 0 1 .708 0L8 10.293l5.646-5.647a.5.5 0 0 1 .708.708l-6 6a.5.5 0 0 1-.708 0l-6-6a.5.5 0 0 1 0-.708z'/%3e%3c/svg%3e");
        background-repeat: no-repeat;
        background-size: 1.25rem;
        transition: transform .2s ease-in-out
    }

    @media (prefers-reduced-motion:reduce) {
        .accordion1-button::after {
            transition: none
        }
    }

    .accordion1-button:hover {
        z-index: 2
    }

    .accordion1-button:focus {
        z-index: 3;
        border-color: #86b7fe;
        outline: 0;
        box-shadow: 0 0 0 .25rem rgba(13, 110, 253, .25)
    }

    .accordion1-header {
        margin-bottom: 0
    }

    .accordion1-item {
        background-color: #fff;
        border: 1px solid rgba(0, 0, 0, .125)
    }

    .accordion1-item:first-of-type {
        border-top-left-radius: .25rem;
        border-top-right-radius: .25rem
    }

    .accordion1-item:first-of-type .accordion1-button {
        border-top-left-radius: calc(.25rem - 1px);
        border-top-right-radius: calc(.25rem - 1px)
    }

    .accordion1-item:not(:first-of-type) {
        border-top: 0
    }

    .accordion1-item:last-of-type {
        border-bottom-right-radius: .25rem;
        border-bottom-left-radius: .25rem
    }

    .accordion1-item:last-of-type .accordion1-button.collapsed {
        border-bottom-right-radius: calc(.25rem - 1px);
        border-bottom-left-radius: calc(.25rem - 1px)
    }

    .accordion1-item:last-of-type .accordion1-collapse {
        border-bottom-right-radius: .25rem;
        border-bottom-left-radius: .25rem
    }

    .accordion1-body {
        padding: 1rem 1.25rem
    }

    .accordion1-flush .accordion1-collapse {
        border-width: 0
    }

    .accordion1-flush .accordion1-item {
        border-right: 0;
        border-left: 0;
        border-radius: 0
    }

    .accordion1-flush .accordion1-item:first-child {
        border-top: 0
    }

    .accordion1-flush .accordion1-item:last-child {
        border-bottom: 0
    }

    .accordion1-flush .accordion1-item .accordion1-button {
        border-radius: 0
    }
</style>
@section('title')
السجلات
@stop
@endsection
@section('page-header')
<!-- breadcrumb -->
<div class="page-title" style="margin-top: 4rem;">
    <div class="row">

        <div class="col-sm-6">
            <h4 class="mb-3">Records</h4>
        </div>
        {{-- <div class="col-sm-6">
            <ol class="breadcrumb pt-0 pr-0 float-left float-sm-right ">
                <li class="breadcrumb-item"><a href="#" class="default-color">{{$student->name}}</a></li>
                <li class="breadcrumb-item active">سجل الطلاب</li>
            </ol>
        </div> --}}
    </div>
</div>
<!-- breadcrumb -->
@endsection
@section('content')
<!-- row -->
{{-- {{dd (Auth::user()->visits()) }} --}}
<div class="row">
    {{-- {{dd($student->student_account)}} --}}
    <div class="col-md-12 mb-30">
        <div class="card card-statistics h-100">
            <div class="card-body">

                <div class="col-xl-12 mb-30 w-100" style="padding:0">
                    <div class=" h-100">
                        <div class="">
                            @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                            @endif
                            {{--
                            <link href="{{ asset('css/bootstrap-iso.css') }}" rel="stylesheet"> --}}

                            <div class="container bootstrap snippet ">
                                <div class="bootstrap-iso">

                                    {{-- <div class="row">
                                        <div class="col-sm-10 pt-3 pb-3">
                                            <h1>{{$user->name}}</h1>
                                        </div>
                                    </div> --}}
                                </div>
                                <div class="row" style="flex-direction: col-reverse;">
                                    <div class="col-sm-12" style="padding:0">

                                        <ul class="nav nav-tabs">
                                            <li class="nav-item"><a class="nav-link active" data-toggle="tab"
                                                    href="#users">Users</a></li>
                                            <li class="nav-item"><a class="nav-link" data-toggle="tab"
                                                    href="#materials">Materials</a></li>
                                            <li class="nav-item"><a class="nav-link" data-toggle="tab"
                                                    href="#outlaytypes">Outlay types</a>
                                            <li class="nav-item"><a class="nav-link" data-toggle="tab"
                                                    href="#outlays">Outlays</a></li>

                                        </ul>


                                        <div class="tab-content">

                                            <div class="tab-pane fade show active" id="users">
                                                <div name="exclude" id="exclude">
                                                    {{--
                                                    <hr> --}}
                                                    <button type="button"
                                                        class="btn btn-primary btn-sm ml-auto d-block py-2 px-3 rounded my-3"
                                                        data-toggle="modal" data-target="#add_user"
                                                        title="Add new user   ">Add <i class="fa fa-add"></i></button>
                                                    <div class="table-responsive">
                                                        <table id="datatable1"
                                                            class="table  table-hover table-md table-borded  p-2"
                                                            data-page-length="50" style="text-align: center">
                                                            <thead>
                                                                <tr>
                                                                    <th>#</th>
                                                                    <th>Name</th>
                                                                    <th>Email</th>
                                                                    <th>Password</th>
                                                                    <th>privilege</th>
                                                                    <th>Actions</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <?php $i = 0; ?>
                                                                @foreach ($users as $user)
                                                                <?php $i++; $user_privilege = $user->roles->pluck('name')->implode('-'); ?>
                                                                <tr>
                                                                    <th>{{ $i }}</th>
                                                                    <th>{{ $user->name }}</th>
                                                                    <th>{{ $user->email }}</th>
                                                                    <th>{{ $user->password }}</th>
                                                                    <th>{{
                                                                        $user_privilege
                                                                        }}</th>
                                                                    <td>
                                                                        <button type="button"
                                                                            class="btn btn-info btn-sm d-inline"
                                                                            data-toggle="modal"
                                                                            data-target="#edit_user{{ $user->id }}"
                                                                            title="Edit"><i
                                                                                class="fa fa-edit"></i></button>
                                                                        <button type="button"
                                                                            class="btn btn-danger btn-sm d-inline"
                                                                            data-toggle="modal"
                                                                            data-target="#delete_modal{{ $user->id }}"
                                                                            title="Delete User"><i
                                                                                class="fa fa-trash"></i></button>
                                                                    </td>

                                                                </tr>

                                                                <x-edit-modal :id="$user->id" title="user"
                                                                    action="user">
                                                                    <div class="mb-3 mt-2"
                                                                        style="display:flex;flex-direction:column;gap:1.5rem;padding:1rem;padding-top:0;padding-bottom:0">
                                                                        <div>
                                                                            <label for="name" class="mr-sm-2">Name
                                                                                :</label>
                                                                            <input id="name" value="{{ $user->name }}"
                                                                                type="text" name="name"
                                                                                class="form-control" />
                                                                        </div>
                                                                        <div>
                                                                            <label for="email" class="mr-sm-2">Email
                                                                                :</label>
                                                                            <input type="text"
                                                                                value="{{ $user->email }}"
                                                                                class="form-control" name="email" />
                                                                        </div>
                                                                        <div>
                                                                            <label for="password"
                                                                                class="mr-sm-2">Password
                                                                                :</label>
                                                                            <input type="text"
                                                                                value="{{ $user->password }}"
                                                                                class="form-control" name="password" />
                                                                        </div>
                                                                        <div>
                                                                            <label for="privileges"
                                                                                class="mr-sm-2">privileges
                                                                                :</label>
                                                                            <select
                                                                                class="privileges form-select-lg mb-3 form-control h-100"
                                                                                id="privileges"
                                                                                aria-label="Default select example"
                                                                                name="privilege">
                                                                                <option value="super-admin"
                                                                                    {{$user_privilege=="super-admin"
                                                                                    ?'selected':''}}>
                                                                                    Super Admin
                                                                                </option>
                                                                                <option value="member"
                                                                                    {{$user_privilege=="member"
                                                                                    ?'selected':''}}>Member</option>

                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                </x-edit-modal>


                                                                <x-delete-modal :id="$user->id" :title="$user->name"
                                                                    action="user.destroy" />

                                                                @endforeach
                                                            </tbody>


                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                            {{-- End Users Table --}}
                                            {{-- Start Materials Table --}}
                                            <div class="tab-pane fade show" id="materials">
                                                <div name="exclude" id="exclude">
                                                    {{--
                                                    <hr> --}}
                                                    <button type="button"
                                                        class="btn btn-primary btn-sm ml-auto d-block py-2 px-3 rounded my-3"
                                                        data-toggle="modal" data-target="#add_material"
                                                        title="Add new material   ">Add <i
                                                            class="fa fa-add"></i></button>
                                                    <div class="table-responsive">
                                                        <table id="datatable2"
                                                            class="table  table-hover table-md table-borded  p-2"
                                                            data-page-length="50" style="text-align: center">
                                                            <thead>
                                                                <tr>
                                                                    <th>#</th>
                                                                    <th>Name</th>
                                                                    <th>IsService</th>
                                                                    <th>Description</th>
                                                                    <th>Actions</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <?php $i = 0; ?>
                                                                @foreach ($materials as $material)
                                                                <?php $i++; ?>
                                                                <tr>
                                                                    <th>{{ $i }}</th>
                                                                    <th>{{ $material->name }}</th>
                                                                    <th>{{ $material->isService?"true":"false" }}</th>
                                                                    <th>{{ $material->description }}</th>

                                                                    <td>
                                                                        <button type="button"
                                                                            class="btn btn-info btn-sm d-inline"
                                                                            data-toggle="modal"
                                                                            data-target="#edit_material{{ $material->id }}"
                                                                            title="Edit"><i
                                                                                class="fa fa-edit"></i></button>
                                                                        <button type="button"
                                                                            class="btn btn-danger btn-sm d-inline"
                                                                            data-toggle="modal"
                                                                            data-target="#delete_modal{{ $material->id }}"
                                                                            title="Delete material"><i
                                                                                class="fa fa-trash"></i></button>
                                                                    </td>

                                                                </tr>

                                                                <x-edit-modal :id="$material->id" title="material"
                                                                    action="material">
                                                                    <div class="mb-3 mt-2"
                                                                        style="display:flex;flex-direction:column;gap:1.5rem;padding:1rem;padding-top:0;padding-bottom:0">
                                                                        <div>
                                                                            <label for="name" class="mr-sm-2">Name
                                                                                :</label>
                                                                            <input id="name"
                                                                                value="{{ $material->name }}"
                                                                                type="text" name="name"
                                                                                class="form-control" />
                                                                        </div>
                                                                        <div>
                                                                            <label for="description"
                                                                                class="mr-sm-2">Description
                                                                                :</label>
                                                                            <input type="text"
                                                                                value="{{ $material->description }}"
                                                                                class="form-control"
                                                                                name="description" />
                                                                        </div>
                                                                        <div class="form-check">
                                                                            <input class="form-check-input"
                                                                                type="checkbox" name="isService"
                                                                                id="isService" {{
                                                                                $material->isService?'checked':"" }}/>
                                                                            <label class="form-check-label"
                                                                                for="isService">
                                                                                Is Service
                                                                            </label>
                                                                        </div>

                                                                    </div>
                                                                </x-edit-modal>


                                                                <x-delete-modal :id="$material->id"
                                                                    :title="$material->name"
                                                                    action="material.destroy" />

                                                                @endforeach
                                                            </tbody>


                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                            {{-- End Materials Table --}}

                                            {{-- Start outlaytypes Table --}}
                                            <div class="tab-pane fade show" id="outlaytypes">
                                                <div name="exclude" id="exclude">
                                                    {{--
                                                    <hr> --}}
                                                    <button type="button"
                                                        class="btn btn-primary btn-sm ml-auto d-block py-2 px-3 rounded my-3"
                                                        data-toggle="modal" data-target="#add_outlaytype"
                                                        title="Add new outlaytype   ">Add <i
                                                            class="fa fa-add"></i></button>
                                                    <div class="table-responsive">
                                                        <table id="datatable3"
                                                            class="table  table-hover table-md table-borded  p-2"
                                                            data-page-length="50" style="text-align: center">
                                                            <thead>
                                                                <tr>
                                                                    <th>#</th>
                                                                    <th>Name</th>
                                                                    <th>Description</th>
                                                                    <th>Actions</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <?php $i = 0; ?>
                                                                @foreach ($outlaytypes as $outlaytype)
                                                                <?php $i++; ?>
                                                                <tr>
                                                                    <th>{{ $i }}</th>
                                                                    <th>{{ $outlaytype->name }}</th>
                                                                    <th>{{ $outlaytype->description }}</th>

                                                                    <td>
                                                                        <button type="button"
                                                                            class="btn btn-info btn-sm d-inline"
                                                                            data-toggle="modal"
                                                                            data-target="#edit_outlaytype{{ $outlaytype->id }}"
                                                                            title="Edit"><i
                                                                                class="fa fa-edit"></i></button>
                                                                        <button type="button"
                                                                            class="btn btn-danger btn-sm d-inline"
                                                                            data-toggle="modal"
                                                                            data-target="#delete_modal{{ $outlaytype->id }}"
                                                                            title="Delete outlaytype"><i
                                                                                class="fa fa-trash"></i></button>
                                                                    </td>

                                                                </tr>

                                                                <x-edit-modal :id="$outlaytype->id" title="outlaytype"
                                                                    action="outlaytype">
                                                                    <div class="mb-3 mt-2"
                                                                        style="display:flex;flex-direction:column;gap:1.5rem;padding:1rem;padding-top:0;padding-bottom:0">
                                                                        <div>
                                                                            <label for="name" class="mr-sm-2">Name
                                                                                :</label>
                                                                            <input id="name"
                                                                                value="{{ $outlaytype->name }}"
                                                                                type="text" name="name"
                                                                                class="form-control" />
                                                                        </div>
                                                                        <div>
                                                                            <label for="description"
                                                                                class="mr-sm-2">Description
                                                                                :</label>
                                                                            <input type="text"
                                                                                value="{{ $outlaytype->description }}"
                                                                                class="form-control"
                                                                                name="description" />
                                                                        </div>
                                                                    </div>
                                                                </x-edit-modal>


                                                                <x-delete-modal :id="$outlaytype->id"
                                                                    :title="$outlaytype->name"
                                                                    action="outlaytype.destroy" />

                                                                @endforeach
                                                            </tbody>


                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                            {{-- End outlaytypes Table --}}

                                            {{-- Start outlays Table --}}
                                            <div class="tab-pane fade show" id="outlays">
                                                <div name="exclude" id="exclude" style="display: flex;
                                                flex-direction: column;">


                                                    <div class="btn-group" role="group" style="float: right;width:100%"
                                                        aria-label="Button group with nested dropdown">
                                                        <button id="btnGroupDrop1" type="button"
                                                            class="btn btn-primary btn-md ml-auto d-block py-2 px-3  my-3"
                                                            data-toggle="modal" data-target="#add_outlay"
                                                            title="Add new outlay">Add <i
                                                                class="fa fa-add"></i></button>
                                                        <div class="btn-group" role="group">

                                                            <button id="filter_dropdown" type="button"
                                                                class="btn btn-secondary dropdown-toggle btn-md ml-auto d-block py-2 px-3  my-3"
                                                                data-toggle="dropdown" aria-haspopup="true"
                                                                aria-expanded="false">
                                                                Filters
                                                            </button>
                                                            <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                                                                <a type="button" {{--
                                                                    class="btn btn-primary btn-sm ml-auto d-block py-2 px-3 rounded my-3"
                                                                    --}} class="dropdown-item" data-toggle="modal"
                                                                    data-target="#filter_year"
                                                                    title="Add new outlay">Filter
                                                                    on Year</i></a>
                                                                <a type="button" {{--
                                                                    class="btn btn-primary btn-sm ml-auto d-block py-2 px-3 rounded my-3"
                                                                    --}} class="dropdown-item" data-toggle="modal"
                                                                    data-target="#filter_month"
                                                                    title="Add new outlay">Filter on
                                                                    Month</i></a>
                                                                <a type="button" {{--
                                                                    class="btn btn-primary btn-sm ml-auto d-block py-2 px-3 rounded my-3"
                                                                    --}} class="dropdown-item" data-toggle="modal"
                                                                    data-target="#filter_user"
                                                                    title="Add new outlay">Filter
                                                                    on
                                                                    User</i></a>
                                                                <a type="button" {{--
                                                                    class="btn btn-primary btn-sm ml-auto d-block py-2 px-3 rounded my-3"
                                                                    --}} class="dropdown-item" data-toggle="modal"
                                                                    data-target="#filter_material"
                                                                    title="Add new outlay">Filter on
                                                                    Material</i></a>
                                                                <a type="button" {{--
                                                                    class="btn btn-primary btn-sm ml-auto d-block py-2 px-3 rounded my-3"
                                                                    --}} class="dropdown-item" data-toggle="modal"
                                                                    data-target="#filter_service"
                                                                    title="Add new outlay">Filter on
                                                                    Service</i></a>
                                                                {{-- <a class="dropdown-item" href="#">Dropdown link</a>
                                                                <a class="dropdown-item" href="#">Dropdown link</a> --}}
                                                            </div>
                                                        </div>
                                                    </div>
                                                    {{-- --}}
                                                    <div class="filter">
                                                        <div class="accordion1" id="accordion1Example">
                                                            <div class="accordion1-item">
                                                                <h2 class="accordion1-header" id="headingOne">
                                                                    <button class="accordion1-button" type="button"
                                                                        data-bs-toggle="collapse"
                                                                        data-bs-target="#collapseOne"
                                                                        aria-expanded="true"
                                                                        aria-controls="collapseOne">
                                                                        Filters
                                                                    </button>
                                                                </h2>
                                                                <div id="collapseOne"
                                                                    class="accordion1-collapse collapse show"
                                                                    aria-labelledby="headingOne"
                                                                    data-bs-parent="#accordion1Example">
                                                                    <div class="accordion1-body">
                                                                        <div id="filter" style="width:30%">
                                                                            <div>
                                                                                <label for="User" class="mr-sm-2">User
                                                                                    :</label>
                                                                                <select
                                                                                    class="filter_user form-select-lg mb-3 form-control h-100"
                                                                                    id="filter_user_input"
                                                                                    aria-label="Default select example"
                                                                                    name="user_id">
                                                                                    @foreach ($users as $key =>
                                                                                    $user) {
                                                                                    <option value="{{ $user->id }}">{{
                                                                                        $user->name
                                                                                        }}</option>
                                                                                    }
                                                                                    @endforeach
                                                                                </select>
                                                                            </div>

                                                                            <div>
                                                                                <label for="material"
                                                                                    class="mr-sm-2">Material
                                                                                    :</label>
                                                                                <select
                                                                                    class="filter_material form-select-lg mb-3 form-control h-100"
                                                                                    id="filter_service_input"
                                                                                    aria-label="Default select example"
                                                                                    name="material_id">
                                                                                    @foreach ($materials as $key =>
                                                                                    $material) {
                                                                                    <option value="{{ $material->id }}">
                                                                                        {{
                                                                                        $material->name
                                                                                        }}</option>
                                                                                    }
                                                                                    @endforeach
                                                                                </select>
                                                                            </div>
                                                                            <div>
                                                                                <label for="service"
                                                                                    class="mr-sm-2">service Expenses
                                                                                    :</label>
                                                                                {{-- <select
                                                                                    class="filter_service form-select-lg mb-3 form-control h-100"
                                                                                    id="filter_service_input"
                                                                                    aria-label="Default select example"
                                                                                    name="service_id">
                                                                                    @foreach ($services as $key =>
                                                                                    $services) {
                                                                                    <option value="{{ $material->id }}">
                                                                                        {{
                                                                                        $material->name
                                                                                        }}</option>
                                                                                    }
                                                                                    @endforeach
                                                                                </select> --}}
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                            </div>


                                                            {{-- <div id="filter_month_input" class="d-none">
                                                                <label>Filters</label>
                                                                <form class="mb-3 w-50">
                                                                    <label class="mr-sm-2">Month
                                                                        :</label>
                                                                    <input type="text" class="form-control outlay_date"
                                                                        name="date" value="{{ old('date') }}" />
                                                                </form>
                                                            </div>
                                                            <div id="filter_month_input" class="d-none">
                                                                <label>Filters</label>
                                                                <form class="mb-3 w-50">
                                                                    <label class="mr-sm-2">Month
                                                                        :</label>
                                                                    <input type="text" class="form-control outlay_date"
                                                                        name="date" value="{{ old('date') }}" />
                                                                </form>
                                                            </div> --}}

                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="table-responsive">
                                                    <table id="datatable4"
                                                        class="table  table-hover table-md table-borded  p-2"
                                                        data-page-length="50" style="text-align: center">
                                                        <thead>
                                                            <tr>
                                                                <th>#</th>
                                                                <th>User</th>
                                                                <th>Outlay Type</th>
                                                                <th>Material</th>
                                                                <th>Price</th>
                                                                <th>Description</th>
                                                                <th>Date</th>
                                                                <th>Actions</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <?php $i = 0; ?>
                                                            @foreach ($outlays as $outlay)
                                                            <?php $i++; ?>
                                                            <tr>
                                                                <th>{{ $i }}</th>
                                                                <th>{{ $outlay->user->name }}</th>
                                                                <th>{{ $outlay->outlaytype->name }}</th>
                                                                <th>{{ $outlay->material->name }}</th>
                                                                <th>{{ $outlay->price }}</th>
                                                                <th>{{ $outlay->description }}</th>
                                                                <th>{{ $outlay->date }}</th>
                                                                <td>
                                                                    <button type="button"
                                                                        class="btn btn-info btn-sm d-inline"
                                                                        data-toggle="modal"
                                                                        data-target="#edit_outlay{{ $outlay->id }}"
                                                                        title="Edit"><i class="fa fa-edit"></i></button>
                                                                    <button type="button"
                                                                        class="btn btn-danger btn-sm d-inline"
                                                                        data-toggle="modal"
                                                                        data-target="#delete_modal{{ $outlay->id }}"
                                                                        title="Delete outlay"><i
                                                                            class="fa fa-trash"></i></button>
                                                                </td>

                                                            </tr>

                                                            <x-edit-modal :id="$outlay->id" title="outlay"
                                                                action="outlay">
                                                                <div class="mb-3 mt-2"
                                                                    style="display:flex;flex-direction:column;gap:1.5rem;padding:1rem;padding-top:0;padding-bottom:0">
                                                                    <div>
                                                                        <label for="User" class="mr-sm-2">User
                                                                            :</label>
                                                                        <select
                                                                            class="privileges form-select-lg mb-3 form-control h-100"
                                                                            id="users"
                                                                            aria-label="Default select example"
                                                                            name="user_id">
                                                                            @foreach ($users as $key =>
                                                                            $user) {
                                                                            <option value="{{ $user->id }}" {{$outlay->
                                                                                user_id==$user->id
                                                                                ?'selected':''}}>{{
                                                                                $user->name
                                                                                }}</option>
                                                                            }
                                                                            @endforeach
                                                                        </select>

                                                                    </div>

                                                                    <div>
                                                                        <label for="Material" class="mr-sm-2">Material
                                                                            :</label>
                                                                        <select
                                                                            class="privileges form-select-lg mb-3 form-control h-100"
                                                                            id="materials"
                                                                            aria-label="Default select example"
                                                                            name="material_id">
                                                                            @foreach ($materials as $key =>
                                                                            $material) {
                                                                            <option value="{{ $material->id }}"
                                                                                {{$outlay->
                                                                                material_id==$material->id
                                                                                ?'selected':''}}>{{
                                                                                $material->name
                                                                                }}</option>
                                                                            }
                                                                            @endforeach
                                                                        </select>

                                                                    </div>
                                                                    <div>
                                                                        <label for="OutlayType" class="mr-sm-2">Outlay
                                                                            Type
                                                                            :</label>
                                                                        <select
                                                                            class="privileges form-select-lg mb-3 form-control h-100"
                                                                            id="OutlayType"
                                                                            aria-label="Default select example"
                                                                            name="outlaytype_id">
                                                                            @foreach ($outlaytypes as $key =>
                                                                            $outlaytype) {
                                                                            <option value="{{ $outlaytype->id }}"
                                                                                {{$outlay->
                                                                                outlaytype_id==$outlaytype->id
                                                                                ?'selected':''}}>{{
                                                                                $outlaytype->name
                                                                                }}</option>
                                                                            }
                                                                            @endforeach
                                                                        </select>

                                                                    </div>
                                                                    <div>
                                                                        <label for="description"
                                                                            class="mr-sm-2">Description
                                                                            :</label>
                                                                        <input type="text"
                                                                            value="{{ $outlay->description }}"
                                                                            class="form-control" name="description" />
                                                                    </div>
                                                                    <div>
                                                                        <label for="price" class="mr-sm-2">Price
                                                                            :</label>
                                                                        <input type="text" id="price"
                                                                            value="{{ $outlay->price }}"
                                                                            class="form-control" name="price" />
                                                                    </div>
                                                                    <div>
                                                                        <label class="mr-sm-2">Date
                                                                            :</label>
                                                                        <input type="text"
                                                                            class="form-control outlay_date" name="date"
                                                                            value="{{ $outlay->date }}" />
                                                                    </div>

                                                                </div>
                                                            </x-edit-modal>


                                                            <x-delete-modal :id="$outlay->id" :title="$outlay->name"
                                                                action="outlay.destroy" />

                                                            @endforeach
                                                        </tbody>


                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                        {{-- End outlays Table --}}
                                        <!--End receipt_tab_pane-->
                                        <!--start payment_tab_pane-->

                                        <!--end payment_tab_pane-->

                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

<!-- row closed -->
{{-- --}}



{{-- Add processes --}}
<!-- Start User Add -->
<x-add-modal title="user" action='user.store'>
    <div class="mb-3 mt-2"
        style="display:flex;flex-direction:column;gap:1.5rem;padding:1rem;padding-top:0;padding-bottom:0">
        <div>
            <label for="name" class="mr-sm-2">Name
                :</label>
            <input id="Name" type="text" name="name" class="form-control" value={{ old('name') }} />
        </div>
        <div>
            <label for="Name_en" class="mr-sm-2">Email
                :</label>
            <input type="text" class="form-control" name="email" value={{ old('email') }} />
        </div>
        <div>
            <label for="password" class="mr-sm-2">Password {{ old('password') }}
                :</label>
            <input id="password" name="password" type="text" class="form-control" value={{ old('password') }} />
        </div>
        <div>
            <label for="privileges" class="mr-sm-2">privileges {{ old('privilege')=="" ?'selected':"" }}
                :</label>
            <select class="privileges form-select-lg mb-3 form-control h-100" id="privileges"
                aria-label="Default select example" name="privilege">
                <option {{ old('privilege')=="" ?'selected':"" }} disabled value="">--choose
                    a specially --</option>
                <option value="super-admin" {{ old('privilege')=="super-admin" ?'selected':"" }}>Super Admin
                </option>
                <option value="member" {{ old('privilege')=="member" ?'selected':"" }}>Member</option>

            </select>
        </div>
    </div>


</x-add-modal>
{{-- End User Add --}}
{{-- Start Materials Add --}}
<x-add-modal title="material" action='material.store'>
    <div class="mb-3 mt-2"
        style="display:flex;flex-direction:column;gap:1.5rem;padding:1rem;padding-top:0;padding-bottom:0">
        <div>
            <label for="name" class="mr-sm-2">Name
                :</label>
            <input id="name" type="text" name="name" class="form-control" />
        </div>
        <div>
            <label for="description" class="mr-sm-2">Description
                :</label>
            <input type="text" class="form-control" name="description" />
        </div>
        <div class="form-check">
            <input class="form-check-input" type="checkbox" name="isService" id="isService" />
            <label class="form-check-label" for="isService">
                Is Service
            </label>
        </div>
    </div>


</x-add-modal>
{{-- End Materials Add --}}

{{-- Start Outlaytype Add --}}
<x-add-modal title="outlaytype" action='outlaytype.store'>
    <div class="mb-3 mt-2"
        style="display:flex;flex-direction:column;gap:1.5rem;padding:1rem;padding-top:0;padding-bottom:0">
        <div>
            <label for="name" class="mr-sm-2">Name
                :</label>
            <input id="name" type="text" name="name" class="form-control" />
        </div>
        <div>
            <label for="description" class="mr-sm-2">Description
                :</label>
            <input type="text" class="form-control" name="description" />
        </div>

    </div>


</x-add-modal>
{{-- End Outlaytype Add --}}
{{-- Start Outlays Add --}}
<x-add-modal title="outlay" action='outlay.store'>
    <div class="mb-3 mt-2"
        style="display:flex;flex-direction:column;gap:1.5rem;padding:1rem;padding-top:0;padding-bottom:0">
        <div>
            <label for="User" class="mr-sm-2">User
                :</label>
            <select class="privileges form-select-lg mb-3 form-control h-100" id="users"
                aria-label="Default select example" name="user_id">
                <option {{ old('user_id')=="" ?'selected':"" }} disabled value="">--choose
                    a specially --</option>
                @foreach ($users as $key =>
                $user) {
                <option value="{{ $user->id }}" {{old($outlay->user_id)==$user->id
                    ?'selected':''}}>{{
                    $user->name
                    }}</option>
                }
                @endforeach
            </select>

        </div>

        <div>
            <label for="Material" class="mr-sm-2">Material
                :</label>
            <select class="privileges form-select-lg mb-3 form-control h-100" id="materials"
                aria-label="Default select example" name="material_id">
                <option {{ old('material_id')=="" ?'selected':"" }} disabled value="">--choose
                    a specially --</option>
                @foreach ($materials as $key =>
                $material) {
                <option value="{{ $material->id }}" {{old($outlay->material_id)==$material->id
                    ?'selected':''}}>{{
                    $material->name
                    }}</option>
                }
                @endforeach
            </select>

        </div>
        <div>
            <label for="OutlayType" class="mr-sm-2">Outlay Type
                :</label>
            <select class="privileges form-select-lg mb-3 form-control h-100" id="OutlayType"
                aria-label="Default select example" name="outlaytype_id">
                <option {{ old('outlaytype_id')=="" ?'selected':"" }} disabled value="">--choose
                    a specially --</option>
                @foreach ($outlaytypes as $key =>
                $outlaytype) {
                <option value="{{ $outlaytype->id }}" {{old($outlay->outlaytype_id)==$outlaytype->id
                    ?'selected':''}}>{{
                    $outlaytype->name
                    }}</option>
                }
                @endforeach
            </select>

        </div>
        <div>
            <label for="description" class="mr-sm-2">Description
                :</label>
            <input type="text" value="{{old($outlay->description) }}" class="form-control" name="description" />
        </div>
        <div>
            <label for="price" class="mr-sm-2">Price
                :</label>
            <input type="text" id="price" value="{{ old($outlay->price) }}" class="form-control" name="price" />
        </div>
        <div>
            <label for="outlay_date" class="mr-sm-2">Date
                :</label>
            <input type="text" id="outlay_date" class="form-control outlay_date" name="date"
                value="{{old($outlay->date)}}" />
        </div>
    </div>


</x-add-modal>
{{-- End Outlays Add --}}
@endsection
@section('js')
<script src="https://code.jquery.com/ui/1.13.1/jquery-ui.min.js"
    integrity="sha256-eTyxS0rkjpLEo16uXTS0uVCS4815lc40K2iVpWDvdSY=" crossorigin="anonymous"></script>
{{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/datepicker/1.0.10/datepicker.min.js"></script> --}}

<script>
    $(document).ready( function () {
    $('#datatable1').DataTable();
    $('#datatable2').DataTable();
    $('#datatable3').DataTable();
    $('#datatable4').DataTable();
    $('.outlay_date').datepicker({showButtonPanel: true,minDate: -30, maxDate: "+1M +10D",dateFormat:"yy-mm-dd" });



    $( "#anim" ).on( "change", function() {
      $( "#datepicker" ).datepicker( "option", "showAnim", $( this ).val() );
    });

    toastr.options.timeOut = 2000;
            @if (Session::has('error'))
                toastr.error('{{ Session::get('error') }}');
            @elseif(Session::has('success'))
                toastr.success('{{ Session::get('success') }}');
            @endif
} );


</script>
<script>
    $(function() {
        var hash = window.location.hash;
        console.log($('ul.nav a[href="' + hash + '"]'));
        hash && $('ul.nav a[href="' + hash + '"]').tab('show');

        $('.nav-tabs a').click(function(e) {
            var scrollmem = $('body').scrollTop();
            console.log(scrollmem);
            window.location.hash = this.hash;
            $('html,body').scrollTop(scrollmem);
        });
        window.addEventListener('hashchange', function(e) {
            hash = window.location.hash;
            hash && $('ul.nav a[href="' + hash + '"]').tab('show');
            var scrollmem = $('body').scrollTop();
            console.log(scrollmem);
            // window.location.hash = this.hash;
            $('html,body').scrollTop(scrollmem);
        }, false);
    });
</script>
<script>
    $(function() {

        $('#filter_year').on('click',function(d){

        })
    })
</script>
{{-- <script>
    $(document).ready(function() {
    $('.modal').on('shown.bs.modal', function(event){
        if($(event.target).find('select[name="group_id"]').length){

        }else{
        var debit = $(event.target).find('input[name="Debit"]').val();
        var final_balance =  $(event.target).find('input[name="final_balance_edit"]').val();
        var final_balance = parseInt(final_balance.replace(/\,/g,''));
        console.log(debit,final_balance);

        if(debit<=final_balance){
        $(event.target).find('button[type="submit"]').prop('disabled', false);
        }
    }


});
    $('.modal').on('hidden.bs.modal', function (event) {

    var a = $(event.target).find('#original_amount').val();
    $(event.target).find('input[name="Debit"]').val(a);

    $(event.target).find('.danger_receipt').empty();
    $(event.target).find('.danger_receipt').css("display", "none");
    $(event.target).find('textarea[name="description"]').val("");
    $(event.target).find($("[type='submit']")).prop('disabled', true);
    if($(event.target).find('select[name="group_id"]')){
        $(event.target).find('select[name="group_id"]').val('0');
        var selectId = (event.target.id);
        var uiid = selectId.split('_')[1];
        var uiid = uiid.toLowerCase();
        $("input[name=final_balance"+uiid+"]").empty();
        $(event.target).find('input[type="text"]').val('');
        if($(event.target).find('input[type="checkbox"]')){
            $(event.target).find('input[type="checkbox"]').prop('checked',false);
        }

    }

    })

    })
</script>
<script>
    function aprrove_receipt(id,Debit,uiid)
{
    console.log(uiid);
    var uiid = uiid.id;
    final_balance = $("input[name=final_balance" + uiid + "]").val();
    selection = $('#group_id_'+uiid).val();

  if(!isNaN(Debit) && Debit !=0 && selection!=null){
  console.log(Debit,final_balance);
  if(parseInt(Debit.replace(/\,/g,'')) > parseInt(final_balance.replace(/\,/g,''))){
    console.log('debit: ' + parseInt(Debit),'final_balance: ' + parseInt(final_balance));

    $(`#${uiid}`).find($('#alert-danger_receipt'+uiid+id)).empty();
    $(`#${uiid}`).find($('#alert-danger_receipt'+uiid+id)).append('<h6>لا يمكن ادخال مبلغ اكبر من رصيد الطالب</h6>');
    $(`#${uiid}`).find($('#alert-danger_receipt'+uiid+id)).css("display","block");
    $(`#${uiid}`).find($('#btn_receipt'+uiid+id)).prop('disabled', true);
  }else{
   $('#alert-danger_receipt'+uiid+id).empty();
   $(`#${uiid}`).find($('#alert-danger_receipt'+uiid+id)).css("display","none");
   $(`#${uiid}`).find($('#btn_receipt'+uiid+id)).prop('disabled', false);
  }
  }
  else{
      if(selection==null){
        $(`#${uiid}`).find($('#alert-danger_receipt'+uiid+id)).empty();
        $(`#${uiid}`).find($('#alert-danger_receipt'+uiid+id)).append('<h6>يرجى ادخال اسم الغروب اولا </h6>');
        $(`#${uiid}`).find($('#alert-danger_receipt'+uiid+id)).css("display","block");
        $(`#${uiid}`).find($('#btn_receipt'+uiid+id)).prop('disabled', true);
  }
  else if(Debit==0){
    $(`#${uiid}`).find($('#alert-danger_receipt'+uiid+id)).empty();
    $(`#${uiid}`).find($('#alert-danger_receipt'+uiid+id)).append('<h6>يرجى ادخال رقم أكبر من 0 وأصغر من رصيد الطالب </h6>');
    $(`#${uiid}`).find($('#alert-danger_receipt'+uiid+id)).css("display","block");
    $(`#${uiid}`).find($('#btn_receipt'+uiid+id)).prop('disabled', true);
  }
  else if(isNaN(Debit) || Debit =="" && final_balance!=0){
    $(`#${uiid}`).find($('#alert-danger_receipt'+uiid+id)).empty();
    $(`#${uiid}`).find($('#alert-danger_receipt'+uiid+id)).append('<h6>يرجى ادخال رقم </h6>');
    $(`#${uiid}`).find($('#alert-danger_receipt'+uiid+id)).css("display","block");
    $(`#${uiid}`).find($('#btn_receipt'+uiid+id)).prop('disabled', true);
  }
}
}

function aprrove_receipt_edit(id,Debit,final_balance,a,uiid)
{
        if(final_balance==0){

    console.log(a.id);
    var uiid = a.id
    }

    else{
    var uiid = uiid.id;
    console.log(uiid);
    }

    final_balance = $('#final_balance_edit'+uiid+id).val();
  if(!isNaN(Debit) && Debit !=""  && Debit!=0){
    console.log('debit: ' + parseInt(Debit),'final_balance: ' + final_balance);

  if(parseInt(Debit.replace(/\,/g,'')) > parseInt(final_balance.replace(/\,/g,''))){

   $('#alert-danger_receipt_edit'+uiid+id).empty();
   $('#alert-danger_receipt_edit'+uiid+id).append('<h6>لا يمكن ادخال مبلغ اكبر من رصيد الطالب</h6>');

   document.getElementById( 'alert-danger_receipt_edit'+uiid+id).style.display="block";
    document.getElementById('btn_receipt_edit'+uiid+id).disabled=true;
  }else{
   $('#alert-danger_receipt'+id).empty();
    document.getElementById( 'alert-danger_receipt_edit'+uiid+id ).style.display="none";
    document.getElementById('btn_receipt_edit'+uiid+id).disabled=false;
  }

  }else{

   $('#alert-danger_receipt_edit'+uiid+id).empty();
   $('#alert-danger_receipt_edit'+uiid+id).append('<h6>يرجى ادخال رقم </h6>');
   document.getElementById( 'alert-danger_receipt_edit'+uiid+id).style.display="block";
    document.getElementById('btn_receipt_edit'+uiid+id).disabled=true;
  }
}


</script>

<script>
    // StudentCategories/student
    // StudentCourses/student

</script>


<script>
    $(document).ready(function() {
        var student_id = $('input[name="id"]').val();
                $.ajax({
                    url: "{{ URL::to('/StudentGroup/student')}}/" + student_id,
                    type: "GET",
                    dataType: "json",
                    success: function(data) {
                        $.each(data, function(key, value) {
                            console.log([key,value]+" ");

                            $('select[name="group_id"]').append('<option value="' +
                                key + '">' + value + '</option>')
                        });
                    },
                });

        });


</script>
<script>
    $(document).ready(function() {
        $('select[name="group_id"]').on('change', function(event) {
            var selectId = (event.target.id);
            var uiid = selectId.split('_')[2];

            var group_id = $(this).val();
            var student_id = $('input[name="id"]').val();
            if (group_id && student_id) {
                console.log(group_id + student_id);
                $.ajax({
                    url: "{{ URL::to('amount/student')}}/" + student_id +"/group/"+group_id+"/"+uiid,
                    type: "GET",
                    dataType: "json",
                    success: function(data) {
                        $("input[name=final_balance"+uiid+"]").empty();
                       $("input[name=final_balance"+uiid+"]").val(data.amount);
                       $('#Name'+uiid).val("");
                      $('button[type="submit"]').prop('disabled', true);
                       $('#alert-danger_receipt'+uiid+student_id).empty();
                        $('#alert-danger_receipt'+uiid+student_id).css("display","none");

                    },
                });
            } else {
                console.log('AJAX load did not work');
            }
        });
    });

</script>
<script>
    $(document).ready(function() {
        $('#datatable1').DataTable();
        $('#datatable2').DataTable();
        $('#datatable3').DataTable();
} );
</script>
<script>
    $(function(){
  var hash = window.location.hash;
  hash && $('ul.nav a[href="' + hash + '"]').tab('show');

  $('.nav-tabs a').click(function (e) {
    $(this).tab('show');
    var scrollmem = $('body').scrollTop();
    console.log(scrollmem);
    window.location.hash = this.hash;
    $('html,body').scrollTop(scrollmem);
  });
});
</script> --}}
<script>

</script>
@endsection
