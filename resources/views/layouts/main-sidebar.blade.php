<style>
    .fa-light {
        size: 20px;
    }
</style>

<div class="container-fluid">
    <div class="row">
        <!-- Left Sidebar start-->
        <div class="side-menu-fixed">
            <div class="scrollbar side-menu-bg">
                <ul class="nav navbar-nav side-menu" id="sidebarnav">
                    <!-- menu item Dashboard-->
                    <li>
                        <a href="{{ auth()->user()->roles[0]->name==" super-admin"? url('/dashboard_users'):url('/home')
                            }}">
                            <div class="pull-left"><i class="ti-home"></i><span class="right-nav-text">Logic
                                    Board</span>
                            </div>
                            <div class="clearfix"></div>
                        </a>
                    </li>
                    <!-- menu title -->
                    <li class="mt-10 mb-10 text-muted pl-4 font-medium menu-title">Contents </li>
                    <!-- menu item Elements-->
                    @can('admin')

                    <li id="dashboard_users_li">
                        <a href="{{ url( '/dashboard_users') }}">
                            <div class="pull-left"><i class="fa-light fa-user" style="font-size: 20px"></i></i><span
                                    class="right-nav-text">users</span></div>
                            <div class="pull-right"></div>
                            <div class="clearfix"></div>
                        </a>

                    </li>
                    @endcan

                    <!-- menu item calendar-->
                    @can('admin')
                    <li id="dashboard_materials_li">
                        <a href="{{ url('/dashboard_materials') }}">
                            <div class="pull-left"><i class="fa-light fa-rectangle-list"></i><span
                                    class="right-nav-text">Materials</span></div>
                            <div class="pull-right"></div>
                            <div class="clearfix"></div>
                        </a>

                    </li>
                    @endcan

                    <!-- menu item todo-->
                    @can('admin')

                    <li id="dashboard_outlaytypes_li">
                        <a href="{{ url('/dashboard_outlaytypes') }}">
                            <div class="pull-left"><i class="fa-light fa-layer-group"></i><span
                                    class="right-nav-text">OutlayType</span></div>
                            <div class="pull-right"></div>
                            <div class="clearfix"></div>
                        </a>

                    </li>
                    @endcan

                    @can('admin')

                    <li id="dashboard_outlays_li">
                        <a href="{{ url('/dashboard_outlays') }}">
                            <div class="pull-left"><i class="fa-light fa-cart-shopping"></i><span
                                    class="right-nav-text">Outlays</span></div>
                            <div class="pull-right"></div>
                            <div class="clearfix"></div>
                        </a>

                    </li>

                    @endcan

                    <!-- menu item Charts-->


                    <!-- Accounts-->

                </ul>
            </div>
        </div>

        <!-- Left Sidebar End-->

        <!--=================================-->
