<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Login</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css"
        integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous" />
    <link rel="stylesheet" />
    <style>
        * {
            margin: 0;
            box-sizing: border-box;
        }

        @media screen and (min-width:992px) {
            body {
                position: relative;
                height: 100vh;
                overflow: hidden;
            }

            .login {
                width: 60%;
                height: 80%;
                position: absolute;
                top: 50%;
                left: 50%;
                transform: translateY(-50%) translateX(-50%);
                border-radius: 20px;
                display: flex;
                flex-direction: column;
                justify-content: center;
                background: white;
            }



        }

        @media screen and (max-width:992px) {
            body {
                position: relative;
                height: auto
            }

            .login {
                width: 100%;
                height: 100%;
                display: flex;
                flex-direction: column;
                justify-content: center;
                background: white;
            }

            .circles {
                display: none
            }
        }
    </style>
</head>

<body>
    {{-- --}}
    <div class="circles"
        style=" position:absolute;top:0;left:0;width:35rem;height:35rem;background:#9ACDD2;z-index:-1;border-radius:80%;transform:translateX(-50%) translateY(-50%)">
    </div>
    <div class="circles"
        style=" position:absolute;top:0;left:0;width:45rem;height:45rem;background:#B5DBDC;z-index:-2;border-radius:47%;transform:translateX(-50%) translateY(-50%)">
    </div>
    <div class="circles"
        style=" position:absolute;top:0;left:0;width:55rem;height:55rem;background:#D4E9EA;z-index:-3;border-radius:44%;transform:translateX(-50%) translateY(-50%)">
    </div>
    {{-- --}}
    <div class="circles"
        style=" position:absolute;top:100%;left:100%;width:35rem;height:35rem;background:#9ACDD2;z-index:-1;border-radius:80%;transform:translateX(-50%) translateY(-50%)">
    </div>
    <div class="circles"
        style=" position:absolute;top:100%;left:100%;width:45rem;height:45rem;background:#B5DBDC;z-index:-2;border-radius:47%;transform:translateX(-50%) translateY(-50%)">
    </div>
    <div class="circles"
        style=" position:absolute;top:100%;left:100%;width:55rem;height:55rem;background:#D4E9EA;z-index:-3;border-radius:44%;transform:translateX(-50%) translateY(-50%)">
    </div>
    {{-- --}}
    <div class="login border p-md-4 p-0 mx-auto">
        <div class="row g-0">
            <div class="col-lg-5 col-md-12 d-sm-none d-md-block">
                <img src="images/office.png" style="width:100%;height:100%;object-fit:cover" />
            </div>
            <div class="form p-md-5 p-sm-0 col-lg-7 col-md-12 col-sm-12 d-flex"
                style="justify-content:center;flex-direction:column">

                <h3 class="my-2" style="text-align:center;color: rgb(96, 96, 96)">Hello Again!</h3>
                <p class="p-4 p-md-2" style="text-align:center;color:rgb(151, 151, 151)">start organizing your life
                    with
                    E-family reduce
                    your expenses and arranges his priorities
                </p>
                <form method="Post" action="{{ route('login') }}" class="mt-4 px-5 pb-5"
                    style="display:flex;flex-direction:column;align-items:center;width:100%;gap:10px">
                    @csrf
                    <div class="row" style="width:100%">
                        <div class="col-md-12">
                            <div class="mb-3">

                                <input type="email" class="form-control @error('email') error @enderror" name="email"
                                    id="email" aria-describedby="helpId" placeholder="Email" />
                                @error('email')
                                <small id="helpId" class="form-text text-danger">{{ $message }}</small>
                                @enderror
                            </div>
                        </div>

                    </div>
                    <div class="row" style="width:100%">

                        <div class="col-md-12">
                            <div class="mb-3">
                                {{-- <label for="password"
                                    class="form-label @error('password') error @enderror">Password</label> --}}
                                <input type="password" class="form-control" name="password" id="password"
                                    aria-describedby="helpId" placeholder="Password" />
                                @error('password')

                                <small id="helpId" class="form-text text-danger">{{ $message }}</small>
                                @enderror
                                @if (Session::has('auth'))
                                <small id="helpId" class="form-text text-danger">{{ Session::get('auth') }}</small>
                                @endif
                            </div>
                        </div>

                    </div>




                    <button class="btn btn-primary btn-block" style="color:white;width:60%">
                        LOG IN
                    </button>



                </form>


                {{-- <div class="alert alert-danger" style="margin-top: 1rem;
                padding: 7px;">
                    <ul style="list-style: none;padding: 0;
                    margin-bottom: 0;">
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div> --}}

            </div>

        </div>
    </div>
</body>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js"
    integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13" crossorigin="anonymous">
    </s>
<script src="https://cdnjs.cloudflare.com/ajax/libs/holder/2.9.8/holder.min.js"
    integrity="sha512-O6R6IBONpEcZVYJAmSC+20vdsM07uFuGjFf0n/Zthm8sOFW+lAq/OK1WOL8vk93GBDxtMIy6ocbj6lduyeLuqQ=="
    crossorigin="anonymous" referrerpolicy="no-referrer">
</script>

</html>
