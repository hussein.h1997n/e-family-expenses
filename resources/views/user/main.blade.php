@extends('layouts.master')
@section('css')
<link rel="stylesheet" type="text/css" href="https://code.jquery.com/ui/1.13.2/themes/base/jquery-ui.css" />
<style>
    .accordion1-button {
        position: relative;
        display: flex;
        align-items: center;
        width: 100%;
        padding: 1rem 1.25rem;
        font-size: 1rem;
        color: #212529;
        text-align: left;
        background-color: #fff;
        border: 0;
        border-radius: 0;
        overflow-anchor: none;
        transition: color .15s ease-in-out, background-color .15s ease-in-out, border-color .15s ease-in-out, box-shadow .15s ease-in-out, border-radius .15s ease
    }

    @media (prefers-reduced-motion:reduce) {
        .accordion1-button {
            transition: none
        }
    }

    .accordion1-button:not(.collapsed) {
        color: #0c63e4;
        background-color: #e7f1ff;
        box-shadow: inset 0 -1px 0 rgba(0, 0, 0, .125)
    }

    .accordion1-button:not(.collapsed)::after {
        background-image: url("data:image/svg+xml,%3csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 16 16' fill='%230c63e4'%3e%3cpath fill-rule='evenodd' d='M1.646 4.646a.5.5 0 0 1 .708 0L8 10.293l5.646-5.647a.5.5 0 0 1 .708.708l-6 6a.5.5 0 0 1-.708 0l-6-6a.5.5 0 0 1 0-.708z'/%3e%3c/svg%3e");
        transform: rotate(-180deg)
    }

    .accordion1-button::after {
        flex-shrink: 0;
        width: 1.25rem;
        height: 1.25rem;
        margin-left: auto;
        content: "";
        background-image: url("data:image/svg+xml,%3csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 16 16' fill='%23212529'%3e%3cpath fill-rule='evenodd' d='M1.646 4.646a.5.5 0 0 1 .708 0L8 10.293l5.646-5.647a.5.5 0 0 1 .708.708l-6 6a.5.5 0 0 1-.708 0l-6-6a.5.5 0 0 1 0-.708z'/%3e%3c/svg%3e");
        background-repeat: no-repeat;
        background-size: 1.25rem;
        transition: transform .2s ease-in-out
    }

    @media (prefers-reduced-motion:reduce) {
        .accordion1-button::after {
            transition: none
        }
    }

    .accordion1-button:hover {
        z-index: 2
    }

    .accordion1-button:focus {
        z-index: 3;
        border-color: #86b7fe;
        outline: 0;
        box-shadow: 0 0 0 .25rem rgba(13, 110, 253, .25)
    }

    .accordion1-header {
        margin-bottom: 0
    }

    .accordion1-item {
        background-color: #fff;
        border: 1px solid rgba(0, 0, 0, .125)
    }

    .accordion1-item:first-of-type {
        border-top-left-radius: .25rem;
        border-top-right-radius: .25rem
    }

    .accordion1-item:first-of-type .accordion1-button {
        border-top-left-radius: calc(.25rem - 1px);
        border-top-right-radius: calc(.25rem - 1px)
    }

    .accordion1-item:not(:first-of-type) {
        border-top: 0
    }

    .accordion1-item:last-of-type {
        border-bottom-right-radius: .25rem;
        border-bottom-left-radius: .25rem
    }

    .accordion1-item:last-of-type .accordion1-button.collapsed {
        border-bottom-right-radius: calc(.25rem - 1px);
        border-bottom-left-radius: calc(.25rem - 1px)
    }

    .accordion1-item:last-of-type .accordion1-collapse {
        border-bottom-right-radius: .25rem;
        border-bottom-left-radius: .25rem
    }

    .accordion1-body {
        padding: 1rem 1.25rem
    }

    .accordion1-flush .accordion1-collapse {
        border-width: 0
    }

    .accordion1-flush .accordion1-item {
        border-right: 0;
        border-left: 0;
        border-radius: 0
    }

    .accordion1-flush .accordion1-item:first-child {
        border-top: 0
    }

    .accordion1-flush .accordion1-item:last-child {
        border-bottom: 0
    }

    .accordion1-flush .accordion1-item .accordion1-button {
        border-radius: 0
    }
</style>
@section('title')
Outlayes
@stop
@endsection
@section('page-header')
<!-- breadcrumb -->
<div class="page-title" style="margin-top: 4rem;">
    <div class="row">

        <div class="col-sm-6">
            <h4 class="mb-3">Outlays Table</h4>
        </div>
    </div>
</div>
<!-- breadcrumb -->
@endsection
@section('content')
<div class="row">
    <div class="col-md-12 mb-30">
        <div class="card card-statistics h-100">
            <div class="card-body">

                <div class="col-xl-12 mb-30 w-100" style="padding:0">
                    <div class=" h-100">
                        <div class="">
                            @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                            @endif
                            <?php
                            if(Session::has('outlays')){
                                $outlays = Session::get('outlays');
                            }
                            ?>
                            <div class="container bootstrap snippet ">
                                <div class="row" style="flex-direction: col-reverse;">
                                    <div class="col-sm-12" style="padding:0">
                                        <div class="tab-pane fade show" id="outlays">
                                            <div name="exclude" id="exclude" style="display: flex;
                                            flex-direction: column;">


                                                <button id="btnGroupDrop1" type="button"
                                                    style="border-radius: 5px 0 0 5px;"
                                                    class="btn btn-primary btn-md ml-auto d-block py-2 px-3  my-3"
                                                    data-toggle="modal" data-target="#add_outlay"
                                                    title="Add new outlay">Add <i class="fa fa-add"></i></button>




                                                {{-- --}}

                                            </div>
                                            <div class="table-responsive">
                                                <table id="datatable4"
                                                    class="table  table-hover table-md table-borded  p-2"
                                                    data-page-length="50" style="text-align: center">
                                                    <thead>
                                                        <tr>
                                                            <th>Id</th>
                                                            <th>User</th>
                                                            <th>Outlay Type</th>
                                                            <th>Material</th>
                                                            <th>Price</th>
                                                            <th>Description</th>
                                                            <th>Date</th>
                                                            <th>Actions</th>
                                                        </tr>
                                                    </thead>

                                                    <tbody>
                                                        <?php $i = 0;?>
                                                        @if(count($outlays)>0)
                                                        @foreach ($outlays as $outlay)
                                                        <?php $i++; ?>
                                                        <tr>
                                                            <th>{{ $outlay->id }}</th>
                                                            <th>{{ $outlay->user->name }}</th>
                                                            <th>{{ $outlay->outlaytype->name }}</th>
                                                            <th>{{ $outlay->material->name }}</th>
                                                            <th>{{ $outlay->price }}</th>
                                                            <th>{{ $outlay->description }}</th>
                                                            <th>{{ $outlay->date }}</th>
                                                            <td>
                                                                <button type="button"
                                                                    class="btn btn-info btn-sm d-inline"
                                                                    data-toggle="modal"
                                                                    data-target="#edit_outlay{{ $outlay->id }}"
                                                                    title="Edit"><i class="fa fa-edit"></i></button>
                                                                <button type="button"
                                                                    class="btn btn-danger btn-sm d-inline"
                                                                    data-toggle="modal"
                                                                    data-target="#delete_modal{{ $outlay->id }}"
                                                                    title="Delete outlay"><i
                                                                        class="fa fa-trash"></i></button>
                                                            </td>

                                                        </tr>

                                                        <x-edit-modal :id="$outlay->id" title="outlay" action="outlay">
                                                            <div class="mb-3 mt-2"
                                                                style="display:flex;flex-direction:column;gap:1.5rem;padding:1rem;padding-top:0;padding-bottom:0">


                                                                <div>
                                                                    <label for="Material" class="mr-sm-2">Material
                                                                        :</label>
                                                                    <select
                                                                        class="privileges form-select-lg mb-3 form-control h-100"
                                                                        id="materials"
                                                                        aria-label="Default select example"
                                                                        name="material_id">
                                                                        @foreach ($materials as $key =>
                                                                        $material) {
                                                                        <option value="{{ $material->id }}" {{$outlay->
                                                                            material_id==$material->id
                                                                            ?'selected':''}}>{{
                                                                            $material->name
                                                                            }}</option>
                                                                        }
                                                                        @endforeach
                                                                    </select>

                                                                </div>
                                                                <div>
                                                                    <label for="OutlayType" class="mr-sm-2">Outlay
                                                                        Type
                                                                        :</label>
                                                                    <select
                                                                        class="privileges form-select-lg mb-3 form-control h-100"
                                                                        id="OutlayType"
                                                                        aria-label="Default select example"
                                                                        name="outlaytype_id">
                                                                        @foreach ($outlaytypes as $key =>
                                                                        $outlaytype) {
                                                                        <option value="{{ $outlaytype->id }}"
                                                                            {{$outlay->
                                                                            outlaytype_id==$outlaytype->id
                                                                            ?'selected':''}}>{{
                                                                            $outlaytype->name
                                                                            }}</option>
                                                                        }
                                                                        @endforeach
                                                                    </select>

                                                                </div>
                                                                <div>
                                                                    <label class="mr-sm-2">Date
                                                                        :</label>
                                                                    <input type="text" class="form-control outlay_date"
                                                                        name="date" value="{{ $outlay->date }}" />
                                                                </div>
                                                                <div>
                                                                    <label for="description" class="mr-sm-2">Description
                                                                        :</label>
                                                                    <input type="text"
                                                                        value="{{ $outlay->description }}"
                                                                        class="form-control" name="description" />
                                                                </div>
                                                                <div>
                                                                    <label for="price" class="mr-sm-2">Price
                                                                        :</label>
                                                                    <input type="text" id="price"
                                                                        value="{{ $outlay->price }}"
                                                                        class="form-control" name="price" />
                                                                </div>


                                                            </div>
                                                        </x-edit-modal>


                                                        <x-delete-modal :id="$outlay->id" :title="$outlay->name"
                                                            action="outlay.destroy" />

                                                        @endforeach
                                                        @endif
                                                    </tbody>


                                                </table>
                                                <div class="d-flex">
                                                    {!!$outlays->links() !!}
                                                </div>
                                            </div>
                                        </div>
                                        {{-- @if (count($outlays)>0) --}}
                                        <x-add-modal title="outlay" action='outlay.store'>
                                            <div class="mb-3 mt-2"
                                                style="display:flex;flex-direction:column;gap:1.5rem;padding:1rem;padding-top:0;padding-bottom:0">


                                                <div>
                                                    <label for="Material" class="mr-sm-2">Material
                                                        :</label>
                                                    <select class="privileges form-select-lg mb-3 form-control h-100"
                                                        id="materials" aria-label="Default select example"
                                                        name="material_id">
                                                        <option {{ old('material_id')=="" ?'selected':"" }} disabled
                                                            value="">--choose
                                                            a specially --</option>
                                                        @foreach ($materials as $key =>
                                                        $material) {
                                                        <option value="{{ $material->id }}" {{old('$outlay->
                                                            material_id')==$material->id
                                                            ?'selected':''}}>{{
                                                            $material->name
                                                            }}</option>
                                                        }
                                                        @endforeach
                                                    </select>

                                                </div>
                                                <div>
                                                    <label for="OutlayType" class="mr-sm-2">Outlay Type
                                                        :</label>
                                                    <select class="privileges form-select-lg mb-3 form-control h-100"
                                                        id="OutlayType" aria-label="Default select example"
                                                        name="outlaytype_id">
                                                        <option {{ old('outlaytype_id')=="" ?'selected':"" }} disabled
                                                            value="">--choose
                                                            a specially --</option>
                                                        @foreach ($outlaytypes as $key =>
                                                        $outlaytype) {
                                                        <option value="{{ $outlaytype->id }}" {{old('$outlay->
                                                            outlaytype_id')==$outlaytype->id
                                                            ?'selected':''}}>{{
                                                            $outlaytype->name
                                                            }}</option>
                                                        }
                                                        @endforeach
                                                    </select>

                                                </div>
                                                <div>
                                                    <label class="mr-sm-2">Date
                                                        :</label>
                                                    <input type="text" class="form-control outlay_date" name="date"
                                                        value="{{old('$outlay->date')}}" />
                                                </div>
                                                <div>
                                                    <label for="description" class="mr-sm-2">Description
                                                        :</label>
                                                    <input type="text" value="{{old('$outlay->description') }}"
                                                        class="form-control" name="description" />
                                                </div>
                                                <div>
                                                    <label for="price" class="mr-sm-2">Price
                                                        :</label>
                                                    <input type="text" id="price" value="{{ old('$outlay->price') }}"
                                                        class="form-control" name="price" />
                                                </div>

                                            </div>


                                        </x-add-modal>
                                        {{-- @endif --}}

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('js')
<script src="https://code.jquery.com/ui/1.13.1/jquery-ui.min.js"
    integrity="sha256-eTyxS0rkjpLEo16uXTS0uVCS4815lc40K2iVpWDvdSY=" crossorigin="anonymous">
</script>
<script>
    $(document).ready( function () {
    $('#datatable4').DataTable({bPaginate: false});
    $('.filter_submit').on('click',function(){

    })
    $('.outlay_date').each(function(index,datepicker){
        $(datepicker).datepicker({showButtonPanel: true,minDate: -30, maxDate: "+1M +10D",dateFormat:"yy-mm-dd"});
    });
toastr.options.timeOut = 2000;
            @if (Session::has('error'))
                toastr.error('{{ Session::get('error') }}');
            @elseif(Session::has('success'))
                toastr.success('{{ Session::get('success') }}');
            @endif
} );
</script>
@endsection
