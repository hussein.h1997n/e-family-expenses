<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class PrivilegesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $superAdmin = Role::create([
            'name' => 'super-admin',
        ]);
        $member = Role::create([
            'name' => 'member',
        ]);
        $edit_expense = Permission::create([
            'name' => 'edit_expense',
        ]);
        $add_expense = Permission::create([
            'name' => 'add_expense',
        ]);
        $delete_expense = Permission::create([
            'name' => 'delete_expense',
        ]);
        $show_expense = Permission::create([
            'name' => 'show_expense',
        ]);
        $member->givePermissionTo([
            'edit_expense',
            'add_expense',
            'delete_expense',
            'show_expense'
        ]);
        User::where('email', 'admin@gmail.com')->get()[0]->assignRole($superAdmin);
        User::where('email', 'john@gmail.com')->get()[0]->assignRole($member);
        User::where('email', 'belly@gmail.com')->get()[0]->assignRole($member);
        User::where('email', 'judy@gmail.com')->get()[0]->assignRole($member);
        User::where('email', 'julia@gmail.com')->get()[0]->assignRole($member);
    }
}
