<?php

namespace Database\Seeders;

use App\Models\Material;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class MaterialSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Material::create([
            'name' => 'Electricity Service',
            'isService' => true,
            'description' => 'pay money for electricity 24/24',
        ]);
        Material::create([
            'name' => 'Internet Service',
            'isService' => true,
            'description' => 'internet bill for getting 100GB monthly',
        ]);
        Material::create([
            'name' => 'Post Service',
            'isService' => true,
            'description' => 'to send and recieve packages and letters',
        ]);
        Material::create([
            'name' => 'Grocery Stuff',
            'isService' => false,
            'description' => 'meat fruit vegetable and daily stuff',
        ]);
        Material::create([
            'name' => 'New Furnace',
            'isService' => false,
            'description' => 'Down payment for a new oven',
        ]);




        Material::create([
            'name' => 'New Wheels',
            'isService' => false,
            'description' => 'Old wheels are worn out',
        ]);
        Material::create([
            'name' => 'parking tickets',
            'isService' => true,
            'description' => 'parking tickets',
        ]);





        Material::create([
            'name' => 'University installments',
            'isService' => true,
            'description' => 'money for the next semester with 7 subjects',
        ]);
        Material::create([
            'name' => 'New Laptop',
            'isService' => false,
            'description' => 'buying new laptop with good specifications',
        ]);
        Material::create([
            'name' => 'Books & Papers',
            'isService' => false,
            'description' => 'rent or buy books from the library',
        ]);





        Material::create([
            'name' => 'Bus Transportaion',
            'isService' => true,
            'description' => 'pay for getting bus  All over the city',
        ]);



        Material::create([
            'name' => 'Personal Insurance',
            'isService' => true,
            'description' => 'an amount of money you pay annually To be treated for free',
        ]);
    }
}
