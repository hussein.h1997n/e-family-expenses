<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;

use App\Models\OutlayType;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();

        \App\Models\User::factory()->create([
            'name' => 'admin',
            'email' => 'admin@gmail.com',
            'password' => '123123123a@'
        ]);
        \App\Models\User::factory()->create([
            'name' => 'john',
            'email' => 'john@gmail.com',
            'password' => '123123123a@'
        ]);
        \App\Models\User::factory()->create([
            'name' => 'belly',
            'email' => 'belly@gmail.com',
            'password' => '123123123a@'
        ]);
        \App\Models\User::factory()->create([
            'name' => 'judy',
            'email' => 'judy@gmail.com',
            'password' => '123123123a@'
        ]);
        \App\Models\User::factory()->create([
            'name' => 'julia',
            'email' => 'julia@gmail.com',
            'password' => '123123123a@'
        ]);
        $this->call(PrivilegesSeeder::class);
        $this->call(OutlayTypeSeeder::class);
        $this->call(MaterialSeeder::class);
        $this->call(OutlaySeeder::class);
    }
}
