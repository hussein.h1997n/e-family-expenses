<?php

namespace Database\Seeders;

use App\Models\OutlayType;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class OutlayTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        OutlayType::create([
            'name' => 'House',
            'description' => 'necessities you need to your house'
        ]);


        OutlayType::create([
            'name' => 'Car',
            'description' => 'necessities you need to your car'
        ]);



        OutlayType::create([
            'name' => 'Education',
            'description' => 'expenses for college and school'
        ]);


        OutlayType::create([
            'name' => 'Transportation',
            'description' => 'bus train and other transportation'
        ]);


        OutlayType::create([
            'name' => 'Health',
            'description' => 'expenses for college and school'
        ]);
    }
}
