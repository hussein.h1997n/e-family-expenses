<?php

namespace Database\Seeders;

use App\Models\Outlay;
use Carbon\Carbon;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class OutlaySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // house
        DB::insert('insert into outlays (material_id, outlaytype_id,user_id,price,description,date,created_at,updated_at ) values (?, ?,?,?,?,?,?,?)', [
            1, 1, 2, 40, "needed expenses",
            '2020-01-01', Carbon::now(), Carbon::now()
        ]);
        DB::insert('insert into outlays (material_id, outlaytype_id,user_id,price,description,date,created_at,updated_at ) values (?, ?,?,?,?,?,?,?)', [
            2, 1, 2, 60, "needed expenses",
            '2020-04-01', Carbon::now(), Carbon::now()
        ]);
        DB::insert('insert into outlays (material_id, outlaytype_id,user_id,price,description,date,created_at,updated_at ) values (?, ?,?,?,?,?,?,?)', [
            3, 1, 2, 35, "needed expenses",
            '2022-07-01', Carbon::now(), Carbon::now()
        ]);


        DB::insert('insert into outlays (material_id, outlaytype_id,user_id,price,description,date,created_at,updated_at ) values (?, ?,?,?,?,?,?,?)', [
            2, 1, 3, 53, "needed expenses",
            '2020-12-01', Carbon::now(), Carbon::now()
        ]);
        DB::insert('insert into outlays (material_id, outlaytype_id,user_id,price,description,date,created_at,updated_at ) values (?, ?,?,?,?,?,?,?)', [
            3, 1, 3, 25, "needed expenses",
            '2022-03-01', Carbon::now(), Carbon::now()
        ]);
        DB::insert('insert into outlays (material_id, outlaytype_id,user_id,price,description,date,created_at,updated_at ) values (?, ?,?,?,?,?,?,?)', [
            5, 1, 3, 120, "needed expenses",
            '2022-03-01', Carbon::now(), Carbon::now()
        ]);



        DB::insert('insert into outlays (material_id, outlaytype_id,user_id,price,description,date,created_at,updated_at ) values (?, ?,?,?,?,?,?,?)', [
            3, 1, 4, 25, "needed expenses",
            '2019-05-01', Carbon::now(), Carbon::now()
        ]);
        DB::insert('insert into outlays (material_id, outlaytype_id,user_id,price,description,date,created_at,updated_at ) values (?, ?,?,?,?,?,?,?)', [
            4, 1, 4, 48, "needed expenses",
            '2019-01-01', Carbon::now(), Carbon::now()
        ]);
        DB::insert('insert into outlays (material_id, outlaytype_id,user_id,price,description,date,created_at,updated_at ) values (?, ?,?,?,?,?,?,?)', [
            1, 1, 4, 34, "needed expenses",
            '2020-01-01', Carbon::now(), Carbon::now()
        ]);


        DB::insert('insert into outlays (material_id, outlaytype_id,user_id,price,description,date,created_at,updated_at ) values (?, ?,?,?,?,?,?,?)', [
            5, 1, 5, 125, "needed expenses",
            '2020-07-01', Carbon::now(), Carbon::now()
        ]);
        DB::insert('insert into outlays (material_id, outlaytype_id,user_id,price,description,date,created_at,updated_at ) values (?, ?,?,?,?,?,?,?)', [
            2, 1, 5, 38, "needed expenses",
            '2022-07-01', Carbon::now(), Carbon::now()
        ]);
        DB::insert('insert into outlays (material_id, outlaytype_id,user_id,price,description,date,created_at,updated_at ) values (?, ?,?,?,?,?,?,?)', [
            1, 1, 5, 22, "needed expenses",
            '2021-09-01', Carbon::now(), Carbon::now()
        ]);

        // car
        DB::insert('insert into outlays (material_id, outlaytype_id,user_id,price,description,date,created_at,updated_at ) values (?, ?,?,?,?,?,?,?)', [
            6, 2, 2, 80, "needed expenses",
            '2021-11-01', Carbon::now(), Carbon::now()
        ]);
        DB::insert('insert into outlays (material_id, outlaytype_id,user_id,price,description,date,created_at,updated_at ) values (?, ?,?,?,?,?,?,?)', [
            7, 2, 2, 30, "needed expenses",
            '2022-02-01', Carbon::now(), Carbon::now()
        ]);



        DB::insert('insert into outlays (material_id, outlaytype_id,user_id,price,description,date,created_at,updated_at ) values (?, ?,?,?,?,?,?,?)', [
            6, 2, 1, 120, "needed expenses",
            '2022-03-01', Carbon::now(), Carbon::now()
        ]);
        DB::insert('insert into outlays (material_id, outlaytype_id,user_id,price,description,date,created_at,updated_at ) values (?, ?,?,?,?,?,?,?)', [
            7, 2, 4, 10, "needed expenses",
            '2021-03-01', Carbon::now(), Carbon::now()
        ]);


        DB::insert('insert into outlays (material_id, outlaytype_id,user_id,price,description,date,created_at,updated_at ) values (?, ?,?,?,?,?,?,?)', [
            6, 2, 5, 94, "needed expenses",
            '2019-06-01', Carbon::now(), Carbon::now()
        ]);
        DB::insert('insert into outlays (material_id, outlaytype_id,user_id,price,description,date,created_at,updated_at ) values (?, ?,?,?,?,?,?,?)', [
            7, 2, 2, 30, "needed expenses",
            '2020-08-01', Carbon::now(), Carbon::now()
        ]);


        // Education
        DB::insert('insert into outlays (material_id, outlaytype_id,user_id,price,description,date,created_at,updated_at ) values (?, ?,?,?,?,?,?,?)', [
            8, 3, 2, 2000, "needed expenses",
            '2022-02-01', Carbon::now(), Carbon::now()
        ]);
        DB::insert('insert into outlays (material_id, outlaytype_id,user_id,price,description,date,created_at,updated_at ) values (?, ?,?,?,?,?,?,?)', [
            9, 3, 2, 1200, "needed expenses",
            '2022-05-01', Carbon::now(), Carbon::now()
        ]);



        DB::insert('insert into outlays (material_id, outlaytype_id,user_id,price,description,date,created_at,updated_at ) values (?, ?,?,?,?,?,?,?)', [
            8, 3, 4, 2000, "needed expenses",
            '2020-05-01', Carbon::now(), Carbon::now()
        ]);
        DB::insert('insert into outlays (material_id, outlaytype_id,user_id,price,description,date,created_at,updated_at ) values (?, ?,?,?,?,?,?,?)', [
            10, 3, 5, 40, "needed expenses",
            '2020-07-01', Carbon::now(), Carbon::now()
        ]);



        DB::insert('insert into outlays (material_id, outlaytype_id,user_id,price,description,date,created_at,updated_at ) values (?, ?,?,?,?,?,?,?)', [
            9, 3, 3, 800, "needed expenses",
            '2019-08-01', Carbon::now(), Carbon::now()
        ]);
        DB::insert('insert into outlays (material_id, outlaytype_id,user_id,price,description,date,created_at,updated_at ) values (?, ?,?,?,?,?,?,?)', [
            10, 3, 4, 49, "needed expenses",
            '2019-11-01', Carbon::now(), Carbon::now()
        ]);

        // Transporting

        DB::insert('insert into outlays (material_id, outlaytype_id,user_id,price,description,date,created_at,updated_at ) values (?, ?,?,?,?,?,?,?)', [
            11, 4, 3, 70, "needed expenses",
            '2020-03-01', Carbon::now(), Carbon::now()
        ]);
        DB::insert('insert into outlays (material_id, outlaytype_id,user_id,price,description,date,created_at,updated_at ) values (?, ?,?,?,?,?,?,?)', [
            11, 4, 5, 40, "needed expenses",
            '2020-01-01', Carbon::now(), Carbon::now()
        ]);
        DB::insert('insert into outlays (material_id, outlaytype_id,user_id,price,description,date,created_at,updated_at ) values (?, ?,?,?,?,?,?,?)', [
            11, 4, 2, 23, "needed expenses",
            '2022-01-01', Carbon::now(), Carbon::now()
        ]);
        // insurance
        DB::insert('insert into outlays (material_id, outlaytype_id,user_id,price,description,date,created_at,updated_at ) values (?, ?,?,?,?,?,?,?)', [
            12, 5, 2, 130, "needed expenses",
            '2021-02-01', Carbon::now(), Carbon::now()
        ]);

        DB::insert('insert into outlays (material_id, outlaytype_id,user_id,price,description,date,created_at,updated_at ) values (?, ?,?,?,?,?,?,?)', [
            12, 5, 4, 170, "needed expenses",
            '2021-04-01', Carbon::now(), Carbon::now()
        ]);

        DB::insert('insert into outlays (material_id, outlaytype_id,user_id,price,description,date,created_at,updated_at ) values (?, ?,?,?,?,?,?,?)', [
            12, 5, 3, 210, "needed expenses",
            '2020-01-01', Carbon::now(), Carbon::now()
        ]);





        //
        //

        // house
        DB::insert('insert into outlays (material_id, outlaytype_id,user_id,price,description,date,created_at,updated_at ) values (?, ?,?,?,?,?,?,?)', [
            1, 1, 2, 40, "needed expenses",
            '2020-12-01', Carbon::now(), Carbon::now()
        ]);
        DB::insert('insert into outlays (material_id, outlaytype_id,user_id,price,description,date,created_at,updated_at ) values (?, ?,?,?,?,?,?,?)', [
            2, 1, 2, 60, "needed expenses",
            '2020-11-01', Carbon::now(), Carbon::now()
        ]);
        DB::insert('insert into outlays (material_id, outlaytype_id,user_id,price,description,date,created_at,updated_at ) values (?, ?,?,?,?,?,?,?)', [
            3, 1, 2, 35, "needed expenses",
            '2022-10-01', Carbon::now(), Carbon::now()
        ]);


        DB::insert('insert into outlays (material_id, outlaytype_id,user_id,price,description,date,created_at,updated_at ) values (?, ?,?,?,?,?,?,?)', [
            2, 1, 3, 53, "needed expenses",
            '2020-09-01', Carbon::now(), Carbon::now()
        ]);
        DB::insert('insert into outlays (material_id, outlaytype_id,user_id,price,description,date,created_at,updated_at ) values (?, ?,?,?,?,?,?,?)', [
            3, 1, 3, 25, "needed expenses",
            '2022-08-01', Carbon::now(), Carbon::now()
        ]);
        DB::insert('insert into outlays (material_id, outlaytype_id,user_id,price,description,date,created_at,updated_at ) values (?, ?,?,?,?,?,?,?)', [
            5, 1, 3, 120, "needed expenses",
            '2022-07-01', Carbon::now(), Carbon::now()
        ]);



        DB::insert('insert into outlays (material_id, outlaytype_id,user_id,price,description,date,created_at,updated_at ) values (?, ?,?,?,?,?,?,?)', [
            3, 1, 4, 25, "needed expenses",
            '2019-06-01', Carbon::now(), Carbon::now()
        ]);
        DB::insert('insert into outlays (material_id, outlaytype_id,user_id,price,description,date,created_at,updated_at ) values (?, ?,?,?,?,?,?,?)', [
            4, 1, 4, 48, "needed expenses",
            '2019-05-01', Carbon::now(), Carbon::now()
        ]);
        DB::insert('insert into outlays (material_id, outlaytype_id,user_id,price,description,date,created_at,updated_at ) values (?, ?,?,?,?,?,?,?)', [
            1, 1, 4, 34, "needed expenses",
            '2020-04-01', Carbon::now(), Carbon::now()
        ]);


        DB::insert('insert into outlays (material_id, outlaytype_id,user_id,price,description,date,created_at,updated_at ) values (?, ?,?,?,?,?,?,?)', [
            5, 1, 5, 125, "needed expenses",
            '2020-03-01', Carbon::now(), Carbon::now()
        ]);
        DB::insert('insert into outlays (material_id, outlaytype_id,user_id,price,description,date,created_at,updated_at ) values (?, ?,?,?,?,?,?,?)', [
            2, 1, 5, 38, "needed expenses",
            '2022-02-01', Carbon::now(), Carbon::now()
        ]);
        DB::insert('insert into outlays (material_id, outlaytype_id,user_id,price,description,date,created_at,updated_at ) values (?, ?,?,?,?,?,?,?)', [
            1, 1, 5, 22, "needed expenses",
            '2021-01-01', Carbon::now(), Carbon::now()
        ]);

        // car
        DB::insert('insert into outlays (material_id, outlaytype_id,user_id,price,description,date,created_at,updated_at ) values (?, ?,?,?,?,?,?,?)', [
            6, 2, 2, 80, "needed expenses",
            '2021-11-01', Carbon::now(), Carbon::now()
        ]);
        DB::insert('insert into outlays (material_id, outlaytype_id,user_id,price,description,date,created_at,updated_at ) values (?, ?,?,?,?,?,?,?)', [
            7, 2, 2, 30, "needed expenses",
            '2022-12-01', Carbon::now(), Carbon::now()
        ]);



        DB::insert('insert into outlays (material_id, outlaytype_id,user_id,price,description,date,created_at,updated_at ) values (?, ?,?,?,?,?,?,?)', [
            6, 2, 1, 120, "needed expenses",
            '2022-03-01', Carbon::now(), Carbon::now()
        ]);
        DB::insert('insert into outlays (material_id, outlaytype_id,user_id,price,description,date,created_at,updated_at ) values (?, ?,?,?,?,?,?,?)', [
            7, 2, 4, 10, "needed expenses",
            '2021-04-01', Carbon::now(), Carbon::now()
        ]);


        DB::insert('insert into outlays (material_id, outlaytype_id,user_id,price,description,date,created_at,updated_at ) values (?, ?,?,?,?,?,?,?)', [
            6, 2, 5, 94, "needed expenses",
            '2019-06-01', Carbon::now(), Carbon::now()
        ]);
        DB::insert('insert into outlays (material_id, outlaytype_id,user_id,price,description,date,created_at,updated_at ) values (?, ?,?,?,?,?,?,?)', [
            7, 2, 2, 30, "needed expenses",
            '2020-02-01', Carbon::now(), Carbon::now()
        ]);


        // Education
        DB::insert('insert into outlays (material_id, outlaytype_id,user_id,price,description,date,created_at,updated_at ) values (?, ?,?,?,?,?,?,?)', [
            8, 3, 2, 2000, "needed expenses",
            '2022-03-01', Carbon::now(), Carbon::now()
        ]);
        DB::insert('insert into outlays (material_id, outlaytype_id,user_id,price,description,date,created_at,updated_at ) values (?, ?,?,?,?,?,?,?)', [
            9, 3, 2, 1200, "needed expenses",
            '2022-07-01', Carbon::now(), Carbon::now()
        ]);



        DB::insert('insert into outlays (material_id, outlaytype_id,user_id,price,description,date,created_at,updated_at ) values (?, ?,?,?,?,?,?,?)', [
            8, 3, 4, 2000, "needed expenses",
            '2020-02-01', Carbon::now(), Carbon::now()
        ]);
        DB::insert('insert into outlays (material_id, outlaytype_id,user_id,price,description,date,created_at,updated_at ) values (?, ?,?,?,?,?,?,?)', [
            10, 3, 5, 40, "needed expenses",
            '2020-03-01', Carbon::now(), Carbon::now()
        ]);



        DB::insert('insert into outlays (material_id, outlaytype_id,user_id,price,description,date,created_at,updated_at ) values (?, ?,?,?,?,?,?,?)', [
            9, 3, 3, 800, "needed expenses",
            '2019-06-01', Carbon::now(), Carbon::now()
        ]);
        DB::insert('insert into outlays (material_id, outlaytype_id,user_id,price,description,date,created_at,updated_at ) values (?, ?,?,?,?,?,?,?)', [
            10, 3, 4, 49, "needed expenses",
            '2019-12-01', Carbon::now(), Carbon::now()
        ]);

        // Transporting

        DB::insert('insert into outlays (material_id, outlaytype_id,user_id,price,description,date,created_at,updated_at ) values (?, ?,?,?,?,?,?,?)', [
            11, 4, 3, 70, "needed expenses",
            '2020-06-01', Carbon::now(), Carbon::now()
        ]);
        DB::insert('insert into outlays (material_id, outlaytype_id,user_id,price,description,date,created_at,updated_at ) values (?, ?,?,?,?,?,?,?)', [
            11, 4, 5, 40, "needed expenses",
            '2020-02-01', Carbon::now(), Carbon::now()
        ]);
        DB::insert('insert into outlays (material_id, outlaytype_id,user_id,price,description,date,created_at,updated_at ) values (?, ?,?,?,?,?,?,?)', [
            11, 4, 2, 23, "needed expenses",
            '2022-04-01', Carbon::now(), Carbon::now()
        ]);
        // insurance
        DB::insert('insert into outlays (material_id, outlaytype_id,user_id,price,description,date,created_at,updated_at ) values (?, ?,?,?,?,?,?,?)', [
            12, 5, 2, 130, "needed expenses",
            '2021-08-01', Carbon::now(), Carbon::now()
        ]);

        DB::insert('insert into outlays (material_id, outlaytype_id,user_id,price,description,date,created_at,updated_at ) values (?, ?,?,?,?,?,?,?)', [
            12, 5, 4, 170, "needed expenses",
            '2021-09-01', Carbon::now(), Carbon::now()
        ]);

        DB::insert('insert into outlays (material_id, outlaytype_id,user_id,price,description,date,created_at,updated_at ) values (?, ?,?,?,?,?,?,?)', [
            12, 5, 3, 210, "needed expenses",
            '2020-02-01', Carbon::now(), Carbon::now()
        ]);
    }
}
